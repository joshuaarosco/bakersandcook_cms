<?php

namespace App\Laravel\Middleware\Backend;
use App\Laravel\Requests\Backend\AuthRequest;
use App\Laravel\Models\User;
use Auth, Closure,Session;

class IsAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request ,Closure $next)
    {
        if(!Auth::check())
        {
            Session::flash('notification-status',"failed");
            Session::flash('notification-message',"Please login first to access the module.");
            return redirect()->route('auth.login');
        }
        return $next($request);
    }
}
