<?php

namespace App\Laravel\Middleware\Backend;
use App\Laravel\Requests\Backend\AuthRequest;
use App\Laravel\Models\User;

use Auth,Session,Closure;

class IsGuest 
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request ,Closure $next)
    {
        if(Auth::check())
        {
            return redirect()->route('frontend.blogs.retrieve');
        }
        return $next($request);
    }
}
