<?php

namespace App\Laravel\Services;
use App\Laravel\Models\SupportLogs;
use App\Laravel\Models\AuditTrail;
use App\Laravel\Models\Comments;
use App\Laravel\Models\Blogs;
use Auth;
class Helper{

	public static function date_format($timestamp, $format="M d, Y - h:i A"){
		$timestamp=strtotime($timestamp);

		return date($format,$timestamp);
	}

	public static function blog_format($timestamp, $format="d F Y - h:i A"){
		$timestamp=strtotime($timestamp);

		return date($format,$timestamp);
	}

	public static function date($timestamp, $format="M d, Y"){
		$timestamp=strtotime($timestamp);

		return date($format,$timestamp);
	}

	public static function day($timestamp, $format="d"){
		$timestamp=strtotime($timestamp);

		return date($format,$timestamp);
	}

	public static function month($timestamp, $format="m"){
		$timestamp=strtotime($timestamp);

		return date($format,$timestamp);
	}

	public static function year($timestamp, $format="Y"){
		$timestamp=strtotime($timestamp);

		return date($format,$timestamp);
	}

	public static function num_of_blogs(){
		return Blog::count();
	}

	public static function num_hr($timestamp,$format="H:i:s"){
		$timestamp = strtotime($timestamp);
		if(date("H",$timestamp)>12)
		{
			$m="pm";
		}
		else
		{
			$m="am";
		}
		echo date($format,$timestamp);
		
	}

	public static function audit_trail($action){
		return "this is your action: ".$action;
	}

	public static function support_logs($client_id,$client_name,$project_name,$type,$details,$status,$queueing_number,$date_time){

		$support = SupportLogs::create([
			'client_id' => $client_id,
			'client_name' => $client_name,
			'project_name' => $project_name,
			'type' => $type,
			'details' => $details,
			'status' => $status,
			'queueing_number' => $queueing_number,
			'date_time' => $date_time,
		]);
	}

	public static function audit($user_id,$user_name,$type,$action_type,$action){

		$audit = AuditTrail::create([
			'user_id' => $user_id,
			'user_name' => $user_name,
			'type' => $type,
			'action_type' => $action_type,
			'action' => $action,
		]);
	}

	public static function count_comment($id){
		$count = Comments::where('blog_id','=',$id)->count();
		return $count;
	}
}