<?php 

namespace App\Laravel\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PlankAbout extends Model{
 
 /**
  * Enable soft delete in table
  * @var boolean
  */
 protected $softDelete = false;
 
 /**
  * The database table used by the model.
  *
  * @var string
  */
 protected $table = 'tbl_plank_about';

 /**
  * The attributes that are mass assignable.
  *
  * @var array
  */
 protected $fillable = ['type','content','archive','deleted_at'];

 /**
  * The attributes excluded from the model's JSON form.
  *
  * @var array
  */
 protected $hidden = [];

 /**
  * The attributes that created within the model.
  *
  * @var array
  */
 protected $appends = [];
}