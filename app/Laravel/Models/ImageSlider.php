<?php 

namespace App\Laravel\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ImageSlider extends Model{
 
 /**
  * Enable soft delete in table
  * @var boolean
  */
 protected $softDelete = false;
 
 /**
  * The database table used by the model.
  *
  * @var string
  */
 protected $table = 'tbl_bakersandcook_menu_images';

 /**
  * The attributes that are mass assignable.
  *
  * @var array
  */
 protected $fillable = ['type','archive','deleted_at','directory','filename'];

 /**
  * The attributes excluded from the model's JSON form.
  *
  * @var array
  */
 protected $hidden = [];

 /**
  * The attributes that created within the model.
  *
  * @var array
  */
 protected $appends = [];
}