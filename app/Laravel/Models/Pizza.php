<?php 

namespace App\Laravel\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Pizza extends Model{
 
 /**
  * Enable soft delete in table
  * @var boolean
  */
 protected $softDelete = false;
 
 /**
  * The database table used by the model.
  *
  * @var string
  */
 protected $table = 'tbl_pizza';

 /**
  * The attributes that are mass assignable.
  *
  * @var array
  */
 protected $fillable = ['id','sequence_no','description','archive','deleted_at'];

 /**
  * The attributes excluded from the model's JSON form.
  *
  * @var array
  */
 protected $hidden = [];

 /**
  * The attributes that created within the model.
  *
  * @var array
  */
 protected $appends = [];
}