<?php 

namespace App\Laravel\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PlankHeader extends Model{
 
 /**
  * Enable soft delete in table
  * @var boolean
  */
 protected $softDelete = false;
 
 /**
  * The database table used by the model.
  *
  * @var string
  */
 protected $table = 'tbl_plank_header';

 /**
  * The attributes that are mass assignable.
  *
  * @var array
  */
 protected $fillable = [  
    'header',
    'sub_header',
    'sequence_no',
    'image_filename',
    'image_directory',
    'filename',
    'directory',
    'button_title',
    'button_link',
    'archive',
    'deleted_at'
    ];

 /**
  * The attributes excluded from the model's JSON form.
  *
  * @var array
  */
 protected $hidden = [];

 /**
  * The attributes that created within the model.
  *
  * @var array
  */
 protected $appends = [];
}