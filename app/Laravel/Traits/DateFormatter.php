<?php

namespace App\Laravel\Traits;
use App\Laravel\Model\Blog;

Trait DateFormatter{
	public function year_only($timestamp, $format="Y"){
		$timestamp = strtotime($timestamp);
		return date($format,$timestamp);
	}

	public function month_only($timestamp, $format="F Y"){
		$timestamp = strtotime($timestamp);
		return date($format,$timestamp);
	}

	public function date_db($timestamp, $db_driver){
		$timestamp = strtotime($timestamp);
		if($db_driver=="mysql")
		{
			$format="y-m-d";
		}
		else
		{
			$format="m-d-y";
		}
		return date($format,$timestamp);
	}
	public function num_hr($timestamp,$format="H:i:s"){
		$timestamp = strtotime($timestamp);
		if(date("H",$timestamp)>12)
		{
			$m="pm";
		}
		else
		{
			$m="am";
		}
		echo date($format,$timestamp);
		$total_sec = (date($format,$timestamp)*3600);
		$total_sec_min = $total_sec/60;
		$hrs=0;
		$mins=0;
		$secs=0;
		for($sec=1;$sec<=$total_sec;$sec++)
		{
			if($sec%60==0)
			{
				$secs++;
				if($secs%60==0)
				{
					$hrs++;

					$mins++;
				}
			}
		}
		echo "<br>The Total seconds is ".$total_sec." = ".$hrs.":".$mins.":".$secs;
	}

}