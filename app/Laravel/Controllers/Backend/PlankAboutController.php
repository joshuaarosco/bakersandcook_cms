<?php
namespace App\Http\Controllers;
namespace App\Laravel\Controllers\Backend;

use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\Authenticatesabouts;
use App\Laravel\Models\PlankAbout;
use Carbon\Carbon;
use Input,Session,Auth,Analytics,Period,File,Str,URL,Helper,Hash;

use Illuminate\Contracts\Auth\Guard;

class PlankAboutController extends Controller{

	/*protected $data;

	public function __construct () {
	  	$this->data = [];
	  	parent::__construct();
	  	array_merge($this->data, parent::get_data());
	}*/

	protected $data;
	public $restful=true;

	public function __construct(){
		$this->data = [];
	}

	public function create(){
        $content = Input::get('content');
        $type = Input::get('type');
        $about = PlankAbout::create([
          'content'=>$content,
          'type'=>$type,
          ]);
        if($about){
            Session::flash('notification-status','success');
            Session::flash('notification-message',"PlankAbout content is successfully created.");
            Helper::audit(Auth::user()->id,Auth::user()->name,Auth::user()->type,"create","PlankAbout content successfully created");
            return redirect('administrator/plank/about/list');
        }else{
            Session::flash('notification-status','warning');
            Session::flash('notification-message',"PlankAbout content was failed to create.");
            Helper::audit(Auth::user()->id,Auth::user()->name,Auth::user()->type,"create","PlankAbout content was failed to create.");
            return redirect()->back();
        }
	}

	public function restore($id){
        $find = PlankAbout::where('id','=',$id)->get();
        foreach($find as $index=>$info){
            $type=$info->type;
        }
        $this->data['count'] = PlankAbout::where('archive','=',null)->where('type','=',$type)->count();
        $count=$this->data['count'];
        if($count==0){
            $abouts = PlankAbout::find($id);
            $date=date('Y-m-d H:i:s');

            if($abouts){
                $abouts->updateOrCreate(array('id' => $id), array('archive' => null));
                if($abouts->save()){
                    Session::flash('notification-status',"success");
                    Session::flash('notification-message',"Success : PlankAbout successfully restore.");
                    Helper::audit(Auth::user()->id,Auth::user()->name,Auth::user()->type,"restore","PlankAbout content , was successfully restored");
                    return redirect()->back();
                }else{
                    Session::flash('notification-status',"failed");
                    Session::flash('notification-message',"Error : There's an error while communicating the database server. Please try again.");
                    Helper::audit(Auth::user()->id,Auth::user()->name,Auth::user()->type,"restore","PlankAbout content , was failed to restore");
                    return redirect()->back();
                }
            }
        }else{
            Session::flash('notification-status',"failed");
            Session::flash('notification-message',"Error : Faield to restore, ".ucfirst($type)." content is already exist.");
            Helper::audit(Auth::user()->id,Auth::user()->name,Auth::user()->type,"restore","PlankAbout content , was failed to restore");
            return redirect()->back();
        }
	}

	public function update($id){
        $abouts = PlankAbout::find($id);
        $content = Input::get('content');
        $about = $abouts->fill([
            'content' => $content,
            ]);

        if($about->save()){
            Session::flash('notification-status','success');
            Session::flash('notification-message',"Successfully updated.");
            Helper::audit(Auth::user()->id,Auth::user()->name,Auth::user()->type,"update","The PlankAbout content was successfully updated");
            return redirect('administrator/plank/about/list');
        }
        
	}

	public function delete($id){
        $abouts = PlankAbout::find($id);
        $date=date('Y-m-d H:i:s');

        if($abouts){
            $abouts->updateOrCreate(array('id' => $id), array('archive' => "1" ))
            ->updateOrCreate(array('id' => $id), array('deleted_at' => $date ));
            if($abouts->save()){
                Session::flash('notification-status',"success");
                Session::flash('notification-message',"Success : PlankAbout content successfully move to archives.");
                Helper::audit(Auth::user()->id,Auth::user()->name,Auth::user()->type,"delete","PlankAbout content , was successfully moved to archive");
                return redirect()->back();
            }else{
                Session::flash('notification-status',"failed");
                Session::flash('notification-message',"Error : There's an error while communicating the database server. Please try again.");
                Helper::audit(Auth::user()->id,Auth::user()->name,Auth::user()->type,"delete","PlankAbout content , was failed to move to archive");
                return redirect()->back();
            }
        }
        return redirect()->back();
        
	}
}