<?php
namespace App\Http\Controllers;
namespace App\Laravel\Controllers\Backend;

use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\Authenticatesblogs;
use App\Laravel\Models\Comments;
use App\Laravel\Models\Replies;
use Carbon\Carbon;
use Input,Session,Auth,Analytics,Period,File,Str,URL,Helper,Hash;

use Illuminate\Contracts\Auth\Guard;

class HomeController extends Controller{

	/*protected $data;

	public function __construct () {
	  	$this->data = [];
	  	parent::__construct();
	  	array_merge($this->data, parent::get_data());
	}*/

	protected $data;
	public $restful=true;

	public function __construct(){
		$this->data = [];
	}

    public function comment($id){
        $name = Input::get('name');
        $email = Input::get('email');
        $comment = Input::get('comment');

        $blogcomment = Comments::create([
            'blog_id' => $id,
            'name' => $name,
            'email' => $email,
            'comment' => $comment,
        ]);

        if($blogcomment){
            Session::flash('notification-status','success');
            Session::flash('notification-message',"Successfully sent.");
            Helper::audit("***",$name,"guest","comment","Sent a comment");
            return redirect()->back();
        }else{
            return redirect()->back();
        }
    }

    public function reply($blog_id,$comment_id){
        $name = Input::get('name');
        $email = Input::get('email');
        $comment = Input::get('comment');
        $blogreply = Replies::create([
            'blog_id' => $blog_id,
            'comment_id' => $comment_id,
            'name' => $name,
            'email' => $email,
            'comment' => $comment,
        ]);

        if($blogreply){
            Session::flash('notification-status','success');
            Session::flash('notification-message',"Successfully sent.");
            Helper::audit("***",$name,"guest","comment","Sent a reply to a comment");
            return redirect()->back();
        }else{
            return redirect()->back();
        }
    }
}