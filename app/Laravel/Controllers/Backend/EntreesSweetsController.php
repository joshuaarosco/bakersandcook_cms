<?php
namespace App\Http\Controllers;
namespace App\Laravel\Controllers\Backend;

use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\Authenticatesentrees_sweets;
use App\Laravel\Models\EntreesSweets;
use Carbon\Carbon;
use Session,Auth,Analytics,Period,File,Str,URL,Helper,Hash,Input;

use Illuminate\Contracts\Auth\Guard;

class EntreesSweetsController extends Controller{

	protected $data;
	public $restful=true;

	public function __construct(){
		$this->data = [];
	}

	public function create(){
		$food_name = Input::get('food_name');
		$type = Input::get('type');
        $entrees_sweets = EntreesSweets::create([
          'food_name'=> $food_name,
          'type'=> $type,
          ]);

        if($entrees_sweets){
            Session::flash('notification-status','success');
            Session::flash('notification-message',"Successfully created.");
            Helper::audit(Auth::user()->id,Auth::user()->name,Auth::user()->type,"add","Successfully added ");
            return redirect('administrator/plank/entrees_sweets/list');
        }
        return redirect()->back();
            
	}

	public function restore($id){
        $entrees_sweets = EntreesSweets::where('id','=',$id)->get();
        foreach($entrees_sweets as $index=>$info){
            $food_name = $info->food_name;
            $type = $info->type;
        }

        $entrees_sweets = EntreesSweets::find($id);
        $date=date('Y-m-d H:i:s');

        if($entrees_sweets){
            $entrees_sweets->updateOrCreate(array('id' => $id), array('archive' => null));
            if($entrees_sweets->save()){
                Session::flash('notification-status',"success");
                Session::flash('notification-message',"Success : Successfully restore.");
                Helper::audit(Auth::user()->id,Auth::user()->name,Auth::user()->type,"restore"," Successfully restored ".$type.": ".$food_name);
                return redirect()->back();
            }else{
                Session::flash('notification-status',"failed");
                Session::flash('notification-message',"Error : There's an error while communicating the database server. Please try again.");
                Helper::audit(Auth::user()->id,Auth::user()->name,Auth::user()->type,"restore","Failed to restore ".$type.": ".$food_name);
                return redirect()->back();
            }
        }
	}

	public function update($id){
        $entrees_sweets = EntreesSweets::find($id);
        $food_name = Input::get('food_name');
        $type = Input::get('type');

        $user = $entrees_sweets->fill([
            'food_name'=> $food_name,
            'type'=> $type,
            ]);

        if($user->save()){
            Session::flash('notification-status','success');
            Session::flash('notification-message',"Successfully updated.");
            Helper::audit(Auth::user()->id,Auth::user()->name,Auth::user()->type,"update","Successfully updated entrees & sweets:".$food_name);
            return redirect()->back();
        }
        return redirect()->back();
	}

	public function delete($id){
        $entrees_sweets = EntreesSweets::where('id','=',$id)->get();
        foreach($entrees_sweets as $index=>$info){
            $food_name = $info->food_name;
            $type = $info->type;
        }
        $entrees_sweets = EntreesSweets::find($id);
        $date=date('Y-m-d H:i:s');

        Session::flash('notification-status',"failed");
        Session::flash('notification-message',"User not found.");

        if($entrees_sweets){
            $entrees_sweets->updateOrCreate(array('id' => $id), array('archive' => "1" ))
            ->updateOrCreate(array('id' => $id), array('deleted_at' => $date ));
            if($entrees_sweets->save()){
                Session::flash('notification-status',"success");
                Session::flash('notification-message',"Success : Successfully move to archives.");
                Helper::audit(Auth::user()->id,Auth::user()->name,Auth::user()->type,"delete","Successfully deleted a ".$type.": ".$food_name);
                return redirect()->back();
            }else{
                Session::flash('notification-status',"failed");
                Session::flash('notification-message',"Error : There's an error while communicating the database server. Please try again.");
                Helper::audit(Auth::user()->id,Auth::user()->name,Auth::user()->type,"delete","Failed to delete a ".$type.": ".$food_name);
                return redirect()->back();
            }
        }
	}
}