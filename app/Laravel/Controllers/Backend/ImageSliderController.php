<?php
namespace App\Http\Controllers;
namespace App\Laravel\Controllers\Backend;

use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\Authenticatesimage_slider;
use App\Laravel\Models\ImageSlider;
use Carbon\Carbon;
use Session,Auth,Analytics,Period,File,Str,URL,Helper,Hash,Input;

use Illuminate\Contracts\Auth\Guard;

class ImageSliderController extends Controller{

	protected $data;
	public $restful=true;

	public function __construct(){
		$this->data = [];
	}

	public function create(){
		$type = Input::get('type');
		$file = Input::file('file');
        if(Input::hasFile('file')){
            $file = Input::file("file");
            $directory = "uploads/image/slider";
            $now = date("Ymd", strtotime(Carbon::now()));
            $ext = $file->getClientOriginalExtension();

            $path_directory = "{$directory}/{$now}";
            $resized_directory = "{$directory}/{$now}/resized";
            $thumb_directory = "{$directory}/{$now}/thumbnails";

            if (!File::exists($path_directory)) File::makeDirectory($path_directory, $mode = 0777, true, true);
            if (!File::exists($resized_directory)) File::makeDirectory($resized_directory, $mode = 0777, true, true);
            if (!File::exists($thumb_directory)) File::makeDirectory($thumb_directory, $mode = 0777, true, true);

            $filename = Str::lower(Str::random(10).date("mdYhs")).".".$ext;

            $file->move($path_directory, $filename); 

            $image_slider = ImageSlider::create([
                'type'=> $type,
                'filename'=> $filename,
                'directory'=>URL::to($path_directory),
                ]);

            if($image_slider){
                Session::flash('notification-status','success');
                Session::flash('notification-message',"Successfully created.");
                Helper::audit(Auth::user()->id,Auth::user()->name,Auth::user()->type,"create","Successfully created an Image Slider");
                return redirect('administrator/image/list');
            }
            return redirect()->back();
        }else{
            if($image_slider){
                Session::flash('notification-status','warning');
                Session::flash('notification-message',"Image is required.");
                Helper::audit(Auth::user()->id,Auth::user()->name,Auth::user()->type,"create","Failed to add Image ");
                return redirect()->back();
            }
            return redirect()->back();
        } 
	}

	public function restore($id){
        $image_slider = ImageSlider::where('id','=',$id)->get();
        $image_slider = ImageSlider::find($id);
        $date=date('Y-m-d H:i:s');

        if($image_slider){
            $image_slider->updateOrCreate(array('id' => $id), array('archive' => null));
            if($image_slider->save()){
                Session::flash('notification-status',"success");
                Session::flash('notification-message',"Success : Successfully restore.");
                Helper::audit(Auth::user()->id,Auth::user()->name,Auth::user()->type,"restore"," Successfully restored an Image Slider");
                return redirect()->back();
            }else{
                Session::flash('notification-status',"failed");
                Session::flash('notification-message',"Error : There's an error while communicating the database server. Please try again.");
                Helper::audit(Auth::user()->id,Auth::user()->name,Auth::user()->type,"restore","Failed to restore an Image Slider");
                return redirect()->back();
            }
        }
	}

	public function update($id){
        $image_slider = ImageSlider::find($id);
        $type = Input::get('type');
        $file = Input::file('file');
        if(Input::hasFile('file')){
            $file = Input::file("file");
            $directory = "uploads/image/slider";
            $now = date("Ymd", strtotime(Carbon::now()));
            $ext = $file->getClientOriginalExtension();

            $path_directory = "{$directory}/{$now}";
            $resized_directory = "{$directory}/{$now}/resized";
            $thumb_directory = "{$directory}/{$now}/thumbnails";

            if (!File::exists($path_directory)) File::makeDirectory($path_directory, $mode = 0777, true, true);
            if (!File::exists($resized_directory)) File::makeDirectory($resized_directory, $mode = 0777, true, true);
            if (!File::exists($thumb_directory)) File::makeDirectory($thumb_directory, $mode = 0777, true, true);

            $filename = Str::lower(Str::random(10).date("mdYhs")).".".$ext;

            $file->move($path_directory, $filename); 

            $image_slider = $image_slider->fill([
                'type'=> $type,
                'filename'=> $filename,
                'directory'=>URL::to($path_directory),
                ]);

            if($image_slider->save()){
                Session::flash('notification-status','success');
                Session::flash('notification-message',"Successfully updated.");
                Helper::audit(Auth::user()->id,Auth::user()->name,Auth::user()->type,"update","Successfully updated an Image Slider");
                return redirect('administrator/image/list');
            }
            return redirect()->back();
        }else{
             $image_slider = $image_slider->fill([
                'type'=> $type,
                ]);
            if($image_slider->save()){
                Session::flash('notification-status','success');
                Session::flash('notification-message',"Successfully updated.");
                Helper::audit(Auth::user()->id,Auth::user()->name,Auth::user()->type,"update","Failed to update Image ");
                return redirect('administrator/image/list');
            }
            return redirect()->back();
        }
	}

	public function delete($id){
        $image_slider = ImageSlider::where('id','=',$id)->get();
        $image_slider = ImageSlider::find($id);
        $date=date('Y-m-d H:i:s');

        Session::flash('notification-status',"failed");
        Session::flash('notification-message',"User not found.");

        if($image_slider){
            $image_slider->updateOrCreate(array('id' => $id), array('archive' => "1" ))
            ->updateOrCreate(array('id' => $id), array('deleted_at' => $date ));
            if($image_slider->save()){
                Session::flash('notification-status',"success");
                Session::flash('notification-message',"Success : Successfully move to archives.");
                Helper::audit(Auth::user()->id,Auth::user()->name,Auth::user()->type,"delete","Successfully deleted an Image Slider");
                return redirect()->back();
            }else{
                Session::flash('notification-status',"failed");
                Session::flash('notification-message',"Error : There's an error while communicating the database server. Please try again.");
                Helper::audit(Auth::user()->id,Auth::user()->name,Auth::user()->type,"delete","Failed to delete an Image Slider");
                return redirect()->back();
            }
        }
	}
}