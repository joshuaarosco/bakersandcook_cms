<?php
namespace App\Http\Controllers;
namespace App\Laravel\Controllers\Backend;

use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Laravel\Models\Users;
use Carbon\Carbon;
use Session,Auth,Analytics,Period,File,Str,URL,Helper,Hash,Input;

use Illuminate\Contracts\Auth\Guard;

class UserController extends Controller{

	/*protected $data;

	public function __construct () {
	  	$this->data = [];
	  	parent::__construct();
	  	array_merge($this->data, parent::get_data());
	}*/

	protected $data;
	public $restful=true;

	public function __construct(){
		$this->data = [];
	}

	public function create(){
		$name = Input::get('name');
		$phone = Input::get('phone');
		$address = Input::get('address');
		$email = Input::get('email');	
		$password = Input::get('password');
		$repeat_password = Input::get('repeat_password');
		$type = Input::get('type');
		$file = Input::file('file');

		if($password==$repeat_password){
			if(Input::hasFile('file')) {
				$file = Input::file("file");
				$directory = "uploads/users";
				$now = date("Ymd", strtotime(Carbon::now()));
				$ext = $file->getClientOriginalExtension();

				$path_directory = "{$directory}/{$now}";
				$resized_directory = "{$directory}/{$now}/resized";
				$thumb_directory = "{$directory}/{$now}/thumbnails";

				if (!File::exists($path_directory)) File::makeDirectory($path_directory, $mode = 0777, true, true);
				if (!File::exists($resized_directory)) File::makeDirectory($resized_directory, $mode = 0777, true, true);
				if (!File::exists($thumb_directory)) File::makeDirectory($thumb_directory, $mode = 0777, true, true);

				$filename = Str::lower(Str::random(10).date("mdYhs")).".".$ext;

				$file->move($path_directory, $filename); 
                /*Image::make("{$path_directory}/{$filename}")->save("{$resized_directory}/{$filename}",95);
                Image::make("{$path_directory}/{$filename}")->crop(250,250)->save("{$thumb_directory}/{$filename}",95);*/


                /*$new_user->directory = URL::to($path_directory);
                $new_user->filename = $filename;*/

                $user = Users::create([
                	'name'=> $name,
                	'type'=> $type,
                	'phone'=> $phone,
                	'filename'=> $filename,
                	'directory'=>URL::to($path_directory),
                	'address' => $address,
                	'email' => $email,
                	'password' => bcrypt($password),
                	]);

                if($user){
                	Session::flash('notification-status','success');
                	Session::flash('notification-message',"Successfully created.");
                	Helper::audit(Auth::user()->id,Auth::user()->name,Auth::user()->type,"create","Successfully created ".$name."'s account");
                	return redirect('administrator/user/list');
                }
                return redirect()->back();
            }else{
            	$user = Users::create([
            		'name'=> $name,
            		'type'=> $type,
            		'phone'=> $phone,
            		'address' => $address,
            		'email' => $email,
            		'password' => bcrypt($password),
            		]);

            	if($user){
            		Session::flash('notification-status','success');
            		Session::flash('notification-message',"Successfully created.");
            		Helper::audit(Auth::user()->id,Auth::user()->name,Auth::user()->type,"create","Successfully created ".$name."'s account");
            		return redirect('administrator/user/list');
            	}
            	return redirect()->back();
            }
        }else{
        	Session::flash('notification-status','warning');
        	Session::flash('notification-message',"Please confirm your password.");
        	Helper::audit(Auth::user()->id,Auth::user()->name,Auth::user()->type,"create","Failed to create ".$name."'s account");
        	return redirect()->back();
        }
	}

	public function restore($id){
        if($id!=Auth::user()->id){
            $users = Users::where('id','=',$id)->get();
            foreach($users as $index=>$info){
                $name = $info->name;
                $type = $info->type;
            }
            if(Auth::user()->type!="super admin" && $type=="super admin"){
                Session::flash('notification-status','failed');
                Session::flash('notification-message',"Your account is restricted to restore this account.");
                Helper::audit(Auth::user()->id,Auth::user()->name,Auth::user()->type,"restore",$name."'s account failed to restore");
                return redirect()->back();
            }else{
                $users = Users::find($id);
                $date=date('Y-m-d H:i:s');

                if($users){
                    $users->updateOrCreate(array('id' => $id), array('archive' => null));
                    if($users->save()){
                        Session::flash('notification-status',"success");
                        Session::flash('notification-message',"Success : User successfully restore.");
                        Helper::audit(Auth::user()->id,Auth::user()->name,Auth::user()->type,"restore",$name."'s account successfully restored");
                        return redirect()->back();
                    }else{
                        Session::flash('notification-status',"failed");
                        Session::flash('notification-message',"Error : There's an error while communicating the database server. Please try again.");
                        Helper::audit(Auth::user()->id,Auth::user()->name,Auth::user()->type,"restore",$name."'s account failed to restored");
                        return redirect()->back();
                    }
                }
                return redirect()->back();
            }
        }else{
            Session::flash('notification-status',"failed");
            Session::flash('notification-message',"Error : You cannot restore your own account.");
            Helper::audit(Auth::user()->id,Auth::user()->name,Auth::user()->type,"restore",Auth::user()->name."'s tries to restore his/her own account");
            return redirect()->back();
        }
	}

	public function update($id){
        $users = Users::find($id);
        $name = Input::get('name');
        $phone = Input::get('phone');
        $address = Input::get('address');
        $email = Input::get('email');   
        $password = Input::get('password');
        $repeat_password = Input::get('repeat_password');
        $type = Input::get('type');
        $file = Input::file('file');

        if($password==$repeat_password){
            if(Input::hasFile('file')) {
                $file = Input::file("file");
                $directory = "uploads/users";
                $now = date("Ymd", strtotime(Carbon::now()));
                $ext = $file->getClientOriginalExtension();

                $path_directory = "{$directory}/{$now}";
                $resized_directory = "{$directory}/{$now}/resized";
                $thumb_directory = "{$directory}/{$now}/thumbnails";

                if (!File::exists($path_directory)) File::makeDirectory($path_directory, $mode = 0777, true, true);
                if (!File::exists($resized_directory)) File::makeDirectory($resized_directory, $mode = 0777, true, true);
                if (!File::exists($thumb_directory)) File::makeDirectory($thumb_directory, $mode = 0777, true, true);

                $filename = Str::lower(Str::random(10).date("mdYhs")).".".$ext;

                $file->move($path_directory, $filename); 
                /*Image::make("{$path_directory}/{$filename}")->save("{$resized_directory}/{$filename}",95);
                Image::make("{$path_directory}/{$filename}")->crop(250,250)->save("{$thumb_directory}/{$filename}",95);*/


                /*$new_user->directory = URL::to($path_directory);
                $new_user->filename = $filename;*/

                $user = $users->fill([
                    'name'=> $name,
                    'type'=> $type,
                    'phone'=> $phone,
                    'filename'=> $filename,
                    'directory'=>URL::to($path_directory),
                    'address' => $address,
                    'email' => $email,
                    ]);

                if($user->save()){
                    Session::flash('notification-status','success');
                    Session::flash('notification-message',"Successfully updated.");
                    Helper::audit(Auth::user()->id,Auth::user()->name,Auth::user()->type,"update","Successfully updated ".$name."'s account");
                    return redirect('administrator/user/list');
                }
                return redirect()->back();
            }else{
                $user = $users->fill([
                    'name'=> $name,
                    'type'=> $type,
                    'phone'=> $phone,
                    'address' => $address,
                    'email' => $email,
                    ]);

                if($user->save()){
                    Session::flash('notification-status','success');
                    Session::flash('notification-message',"Successfully updated.");
                    Helper::audit(Auth::user()->id,Auth::user()->name,Auth::user()->type,"update","Successfully updated ".$name."'s account");
                    return redirect('administrator/user/list');
                }
                return redirect()->back();
            }
        }else{
            Session::flash('notification-status','warning');
            Session::flash('notification-message',"Please confirm your password.");
            Helper::audit(Auth::user()->id,Auth::user()->name,Auth::user()->type,"update","Failed to update ".$name."'s account");
            return redirect()->back();
        }
	}

	public function delete($id){
        if($id!=Auth::user()->id){
            $users = Users::where('id','=',$id)->get();
            foreach($users as $index=>$info){
                $name = $info->name;
                $type = $info->type;
            }
            if(Auth::user()->type!="super admin" && $type=="super admin"){
                Session::flash('notification-status','failed');
                Session::flash('notification-message',"Your account is restricted to delete this account.");
                Helper::audit(Auth::user()->id,Auth::user()->name,Auth::user()->type,"delete",$name."'s account failed to move to archive");
                return redirect()->back();
            }else{
                $users = Users::find($id);
                $date=date('Y-m-d H:i:s');

                Session::flash('notification-status',"failed");
                Session::flash('notification-message',"User not found.");

                if($users){
                    $users->updateOrCreate(array('id' => $id), array('archive' => "1" ))
                    ->updateOrCreate(array('id' => $id), array('deleted_at' => $date ));
                    if($users->save()){
                        Session::flash('notification-status',"success");
                        Session::flash('notification-message',"Success : User successfully move to archives.");
                        Helper::audit(Auth::user()->id,Auth::user()->name,Auth::user()->type,"delete",$name."'s account successfully moved to archive");
                        return redirect()->back();
                    }else{
                        Session::flash('notification-status',"failed");
                        Session::flash('notification-message',"Error : There's an error while communicating the database server. Please try again.");
                        Helper::audit(Auth::user()->id,Auth::user()->name,Auth::user()->type,"delete",$name."'s account failed to move to archive");
                        return redirect()->back();
                    }
                }
                return redirect()->back();
            }
        }else{
            Session::flash('notification-status',"failed");
            Session::flash('notification-message',"Error : You cannot delete your own account.");
            Helper::audit(Auth::user()->id,Auth::user()->name,Auth::user()->type,"delete",Auth::user()->name."'s tries to delete his/her own account");
            return redirect()->back();
        }
	}

    public function change_password($id){
        $old_pass=Input::get('op');
        $new_pass=Input::get('password');
        $con_pass=Input::get('confirmpassword');

        if($new_pass==$con_pass){
            if(Hash::check($old_pass,Auth::user()->password)){
                $users = Users::find($id);

                $users = $users->fill([
                    'password' => bcrypt($new_pass),
                ]);
                if($users->save()){
                    Session::flash('notification-status',"success");
                    Session::flash('notification-message',"Success : Password successfully updated.");
                    Helper::audit(Auth::user()->id,Auth::user()->name,Auth::user()->type,"change","Password successfully changed");
                    return redirect()->back();
                }else{
                    Session::flash('notification-status',"failed");
                    Session::flash('notification-message',"Error : There's an error while communicating the database server. Please try again.");
                }
            }else{
                Session::flash('notification-status',"warning");
                Session::flash('notification-message',"Oops : Please enter your current password.");
                return redirect()->back();
            }
        }else{
            Session::flash('notification-status',"warning");
            Session::flash('notification-message',"Oops : Please confirm your current password.");
            return redirect()->back();
        }
    }
}