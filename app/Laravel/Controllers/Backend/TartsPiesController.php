<?php
namespace App\Http\Controllers;
namespace App\Laravel\Controllers\Backend;

use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\Authenticatestarts_pies;
use App\Laravel\Models\TartsPies;
use Carbon\Carbon;
use Session,Auth,Analytics,Period,File,Str,URL,Helper,Hash,Input;

use Illuminate\Contracts\Auth\Guard;

class TartsPiesController extends Controller{

	/*protected $data;

	public function __construct () {
	  	$this->data = [];
	  	parent::__construct();
	  	array_merge($this->data, parent::get_data());
	}*/

	protected $data;
	public $restful=true;

	public function __construct(){
		$this->data = [];
	}

	public function create(){
		$food_name = Input::get('food_name');
		$type = Input::get('type');
        $tarts_pies = TartsPies::create([
          'food_name'=> $food_name,
          'type'=> $type,
          ]);

        if($tarts_pies){
            Session::flash('notification-status','success');
            Session::flash('notification-message',"Successfully added.");
            Helper::audit(Auth::user()->id,Auth::user()->name,Auth::user()->type,"add","Successfully added ");
            return redirect('administrator/tarts_pies/list');
        }
        return redirect()->back();
	}

	public function restore($id){
        $tarts_pies = TartsPies::where('id','=',$id)->get();
        foreach($tarts_pies as $index=>$info){
            $food_name = $info->food_name;
        }

        $tarts_pies = TartsPies::find($id);
        $date=date('Y-m-d H:i:s');

        if($tarts_pies){
            $tarts_pies->updateOrCreate(array('id' => $id), array('archive' => null));
            if($tarts_pies->save()){
                Session::flash('notification-status',"success");
                Session::flash('notification-message',"Success : Successfully restore.");
                Helper::audit(Auth::user()->id,Auth::user()->name,Auth::user()->type,"restore"," Successfully restored a Tarts & Pies: ".$food_name);
                return redirect()->back();
            }else{
                Session::flash('notification-status',"failed");
                Session::flash('notification-message',"Error : There's an error while communicating the database server. Please try again.");
                Helper::audit(Auth::user()->id,Auth::user()->name,Auth::user()->type,"restore","Failed to restore a Tarts & Pies: ".$food_name);
                return redirect()->back();
            }
        }
	}

	public function update($id){
        $tarts_pies = TartsPies::find($id);
        $food_name = Input::get('food_name');
        $type = Input::get('type');

        $user = $tarts_pies->fill([
            'food_name'=> $food_name,
            'type'=> $type,
            ]);

        if($user->save()){
            Session::flash('notification-status','success');
            Session::flash('notification-message',"Successfully updated.");
            Helper::audit(Auth::user()->id,Auth::user()->name,Auth::user()->type,"update","Successfully updated Tarts & Pies:".$food_name);
            return redirect()->back();
        }
        return redirect()->back();
	}

	public function delete($id){
        $tarts_pies = TartsPies::where('id','=',$id)->get();
        foreach($tarts_pies as $index=>$info){
            $food_name = $info->food_name;
        }
        $tarts_pies = TartsPies::find($id);
        $date=date('Y-m-d H:i:s');

        Session::flash('notification-status',"failed");
        Session::flash('notification-message',"User not found.");

        if($tarts_pies){
            $tarts_pies->updateOrCreate(array('id' => $id), array('archive' => "1" ))
            ->updateOrCreate(array('id' => $id), array('deleted_at' => $date ));
            if($tarts_pies->save()){
                Session::flash('notification-status',"success");
                Session::flash('notification-message',"Success : Successfully move to archives.");
                Helper::audit(Auth::user()->id,Auth::user()->name,Auth::user()->type,"delete","Successfully deleted a Tarts & Pies: ".$food_name);
                return redirect()->back();
            }else{
                Session::flash('notification-status',"failed");
                Session::flash('notification-message',"Error : There's an error while communicating the database server. Please try again.");
                Helper::audit(Auth::user()->id,Auth::user()->name,Auth::user()->type,"delete","Failed to delete a Tarts & Pies: ".$food_name);
                return redirect()->back();
            }
        }
	}
}