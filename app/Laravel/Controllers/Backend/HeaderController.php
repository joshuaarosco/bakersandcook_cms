<?php
namespace App\Http\Controllers;
namespace App\Laravel\Controllers\Backend;

use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\Authenticatesheader;
use App\Laravel\Models\Header;
use Carbon\Carbon;
use Session,Auth,Analytics,Period,File,Str,URL,Helper,Hash,Input;

use Illuminate\Contracts\Auth\Guard;

class HeaderController extends Controller{

	protected $data;
	public $restful=true;

	public function __construct(){
		$this->data = [];
	}

	public function create(){
		$sequence_no = Input::get('sequence_no');
        $header = Input::get('header');
        $sub_header = Input::get('sub_header');
        $button_title = Input::get('button_title');
        $button_link = Input::get('button_link');
        $image_file = Input::file('image_file');
		$file = Input::file('file');
        if(Input::hasFile('file')){
            $file = Input::file("file");
            $image_file = Input::file('image_file');
            $directory = "uploads/image/header";
            $now = date("Ymd", strtotime(Carbon::now()));
            $ext = $file->getClientOriginalExtension();

            $path_directory = "{$directory}/{$now}";
            $resized_directory = "{$directory}/{$now}/resized";
            $thumb_directory = "{$directory}/{$now}/thumbnails";

            if (!File::exists($path_directory)) File::makeDirectory($path_directory, $mode = 0777, true, true);
            if (!File::exists($resized_directory)) File::makeDirectory($resized_directory, $mode = 0777, true, true);
            if (!File::exists($thumb_directory)) File::makeDirectory($thumb_directory, $mode = 0777, true, true);

            $filename = Str::lower(Str::random(10).date("mdYhs")).".".$ext;

            $file->move($path_directory, $filename); 
            if(isset($image_file)){
                $exts = $image_file->getClientOriginalExtension();
                $image_filename = Str::lower(Str::random(10).date("mdYhs")).".".$exts;
                $image_file->move($path_directory, $image_filename); 
            }else{
                $image_filename="";
            }
            
            $header = Header::create([
                'sequence_no'=> $sequence_no,
                'header'=> $header,
                'sub_header'=> $sub_header,
                'button_title'=> $button_title,
                'button_link'=> $button_link,
                'image_filename'=> $image_filename,
                'filename'=> $filename,
                'directory'=>URL::to($path_directory),
            ]);

            if($header){
                Session::flash('notification-status','success');
                Session::flash('notification-message',"Successfully created.");
                Helper::audit(Auth::user()->id,Auth::user()->name,Auth::user()->type,"create","Successfully created a header");
                return redirect('administrator/header/list');
            }
            return redirect()->back();
        }else{
            if($header){
                Session::flash('notification-status','warning');
                Session::flash('notification-message',"Image is required.");
                Helper::audit(Auth::user()->id,Auth::user()->name,Auth::user()->type,"create","Failed to add Header ");
                return redirect()->back();
            }
            return redirect()->back();
        } 
	}

	public function restore($id){
        $header = Header::where('id','=',$id)->get();
        $header = Header::find($id);
        $date=date('Y-m-d H:i:s');

        if($header){
            $header->updateOrCreate(array('id' => $id), array('archive' => null));
            if($header->save()){
                Session::flash('notification-status',"success");
                Session::flash('notification-message',"Success : Successfully restore.");
                Helper::audit(Auth::user()->id,Auth::user()->name,Auth::user()->type,"restore"," Successfully restored an Image header");
                return redirect()->back();
            }else{
                Session::flash('notification-status',"failed");
                Session::flash('notification-message',"Error : There's an error while communicating the database server. Please try again.");
                Helper::audit(Auth::user()->id,Auth::user()->name,Auth::user()->type,"restore","Failed to restore an Image header");
                return redirect()->back();
            }
        }
	}

	public function update($id){
        $headers = Header::find($id);
        $sequence_no = Input::get('sequence_no');
        $header = Input::get('header');
        $sub_header = Input::get('sub_header');
        $button_title = Input::get('button_title');
        $button_link = Input::get('button_link');
        $image_file = Input::file('image_file');
        $file = Input::file('file');
        if(Input::hasFile('file')&&Input::hasFile('image_file')){
            $file = Input::file("file");
            $image_file = Input::file('image_file');
            $directory = "uploads/image/header";
            $now = date("Ymd", strtotime(Carbon::now()));
            $exts = $image_file->getClientOriginalExtension();
            $ext = $file->getClientOriginalExtension();

            $path_directory = "{$directory}/{$now}";
            $resized_directory = "{$directory}/{$now}/resized";
            $thumb_directory = "{$directory}/{$now}/thumbnails";

            if (!File::exists($path_directory)) File::makeDirectory($path_directory, $mode = 0777, true, true);
            if (!File::exists($resized_directory)) File::makeDirectory($resized_directory, $mode = 0777, true, true);
            if (!File::exists($thumb_directory)) File::makeDirectory($thumb_directory, $mode = 0777, true, true);

            $filename = Str::lower(Str::random(10).date("mdYhs")).".".$ext;
            $image_filename = Str::lower(Str::random(10).date("mdYhs")).".".$exts;

            $file->move($path_directory, $filename);
            $image_file->move($path_directory, $image_filename); 

            $headers = $headers->fill([
                'sequence_no'=> $sequence_no,
                'header'=> $header,
                'sub_header'=> $sub_header,
                'button_title'=> $button_title,
                'button_link'=> $button_link,
                'image_filename'=> $image_filename,
                'filename'=> $filename,
                'directory'=>URL::to($path_directory),
                ]);

            if($headers->save()){
                Session::flash('notification-status','success');
                Session::flash('notification-message',"Successfully updated.");
                Helper::audit(Auth::user()->id,Auth::user()->name,Auth::user()->type,"update","Successfully updated an Image header");
                return redirect('administrator/header/list');
            }
            return redirect()->back();
        }elseif(Input::hasFile('file')){
            $file = Input::file("file");
            $directory = "uploads/image/header";
            $now = date("Ymd", strtotime(Carbon::now()));
            $ext = $file->getClientOriginalExtension();

            $path_directory = "{$directory}/{$now}";
            $resized_directory = "{$directory}/{$now}/resized";
            $thumb_directory = "{$directory}/{$now}/thumbnails";

            if (!File::exists($path_directory)) File::makeDirectory($path_directory, $mode = 0777, true, true);
            if (!File::exists($resized_directory)) File::makeDirectory($resized_directory, $mode = 0777, true, true);
            if (!File::exists($thumb_directory)) File::makeDirectory($thumb_directory, $mode = 0777, true, true);

            $filename = Str::lower(Str::random(10).date("mdYhs")).".".$ext;

            $file->move($path_directory, $filename);
            
            $headers = $headers->fill([
                'sequence_no'=> $sequence_no,
                'header'=> $header,
                'sub_header'=> $sub_header,
                'button_title'=> $button_title,
                'button_link'=> $button_link,
                'filename'=> $filename,
                'directory'=>URL::to($path_directory),
                ]);
            if($headers->save()){
                Session::flash('notification-status','success');
                Session::flash('notification-message',"Successfully updated.");
                Helper::audit(Auth::user()->id,Auth::user()->name,Auth::user()->type,"update","Failed to update Image ");
                return redirect('administrator/header/list');
            }
            return redirect()->back();
        }elseif(Input::hasFile('image_file')){
            $image_file = Input::file('image_file');
            $directory = "uploads/image/header";
            $now = date("Ymd", strtotime(Carbon::now()));
            $exts = $image_file->getClientOriginalExtension();

            $path_directory = "{$directory}/{$now}";
            $resized_directory = "{$directory}/{$now}/resized";
            $thumb_directory = "{$directory}/{$now}/thumbnails";

            if (!File::exists($path_directory)) File::makeDirectory($path_directory, $mode = 0777, true, true);
            if (!File::exists($resized_directory)) File::makeDirectory($resized_directory, $mode = 0777, true, true);
            if (!File::exists($thumb_directory)) File::makeDirectory($thumb_directory, $mode = 0777, true, true);

            $image_filename = Str::lower(Str::random(10).date("mdYhs")).".".$exts;

            $image_file->move($path_directory, $image_filename); 
            
            $headers = $headers->fill([
                'sequence_no'=> $sequence_no,
                'header'=> $header,
                'sub_header'=> $sub_header,
                'button_title'=> $button_title,
                'button_link'=> $button_link,
                'image_filename'=> $image_filename,
                'directory'=>URL::to($path_directory),
                ]);
            if($headers->save()){
                Session::flash('notification-status','success');
                Session::flash('notification-message',"Successfully updated.");
                Helper::audit(Auth::user()->id,Auth::user()->name,Auth::user()->type,"update","Failed to update Image ");
                return redirect('administrator/header/list');
            }
            return redirect()->back();
        }else{
            $headers = $headers->fill([
                'sequence_no'=> $sequence_no,
                'header'=> $header,
                'sub_header'=> $sub_header,
                'button_title'=> $button_title,
                'button_link'=> $button_link,
                ]);
            if($headers->save()){
                Session::flash('notification-status','success');
                Session::flash('notification-message',"Successfully updated.");
                Helper::audit(Auth::user()->id,Auth::user()->name,Auth::user()->type,"update","Failed to update Image ");
                return redirect('administrator/header/list');
            }
            return redirect()->back();
        }
	}

	public function delete($id){
        $header = Header::where('id','=',$id)->get();
        $header = Header::find($id);
        $date=date('Y-m-d H:i:s');

        Session::flash('notification-status',"failed");
        Session::flash('notification-message',"User not found.");

        if($header){
            $header->updateOrCreate(array('id' => $id), array('archive' => "1" ))
            ->updateOrCreate(array('id' => $id), array('deleted_at' => $date ));
            if($header->save()){
                Session::flash('notification-status',"success");
                Session::flash('notification-message',"Success : Successfully move to archives.");
                Helper::audit(Auth::user()->id,Auth::user()->name,Auth::user()->type,"delete","Successfully deleted an Image header");
                return redirect()->back();
            }else{
                Session::flash('notification-status',"failed");
                Session::flash('notification-message',"Error : There's an error while communicating the database server. Please try again.");
                Helper::audit(Auth::user()->id,Auth::user()->name,Auth::user()->type,"delete","Failed to delete an Image header");
                return redirect()->back();
            }
        }
	}
}