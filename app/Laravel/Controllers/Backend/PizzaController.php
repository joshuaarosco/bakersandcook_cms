<?php
namespace App\Http\Controllers;
namespace App\Laravel\Controllers\Backend;

use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\Authenticatespizza;
use App\Laravel\Models\Pizza;
use Carbon\Carbon;
use Session,Auth,Analytics,Period,File,Str,URL,Helper,Hash,Input;

use Illuminate\Contracts\Auth\Guard;

class PizzaController extends Controller{

	/*protected $data;

	public function __construct () {
	  	$this->data = [];
	  	parent::__construct();
	  	array_merge($this->data, parent::get_data());
	}*/

	protected $data;
	public $restful=true;

	public function __construct(){
		$this->data = [];
	}

	public function create(){
		$description = Input::get('description');
		$sequence_no = Input::get('sequence_no');
        $pizza = Pizza::create([
          'sequence_no'=> $sequence_no,
          'description'=> $description,
          ]);

        if($pizza){
            Session::flash('notification-status','success');
            Session::flash('notification-message',"Successfully added.");
            Helper::audit(Auth::user()->id,Auth::user()->name,Auth::user()->sequence_no,"add","Successfully added ");
            return redirect('administrator/plank/pizza/list');
        }
        return redirect()->back();
	}

	public function restore($id){
        $pizza = Pizza::where('id','=',$id)->get();

        $pizza = Pizza::find($id);
        $date=date('Y-m-d H:i:s');

        if($pizza){
            $pizza->updateOrCreate(array('id' => $id), array('archive' => null));
            if($pizza->save()){
                Session::flash('notification-status',"success");
                Session::flash('notification-message',"Success : Successfully restore.");
                Helper::audit(Auth::user()->id,Auth::user()->name,Auth::user()->sequence_no,"restore"," Successfully restored a pizza description");
                return redirect()->back();
            }else{
                Session::flash('notification-status',"failed");
                Session::flash('notification-message',"Error : There's an error while communicating the database server. Please try again.");
                Helper::audit(Auth::user()->id,Auth::user()->name,Auth::user()->sequence_no,"restore","Failed to restore a pizza description");
                return redirect()->back();
            }
        }
	}

	public function update($id){
        $pizza = Pizza::find($id);
        $description = Input::get('description');
        $sequence_no = Input::get('sequence_no');

        $user = $pizza->fill([
            'sequence_no'=> $sequence_no,
            'description'=> $description,
        ]);

        if($user->save()){
            Session::flash('notification-status','success');
            Session::flash('notification-message',"Successfully updated.");
            Helper::audit(Auth::user()->id,Auth::user()->name,Auth::user()->sequence_no,"update","Successfully updated a pizza description");
            return redirect()->back();
        }
        return redirect()->back();
	}

	public function delete($id){
        $pizza = Pizza::where('id','=',$id)->get();
        $pizza = Pizza::find($id);
        $date=date('Y-m-d H:i:s');

        Session::flash('notification-status',"failed");
        Session::flash('notification-message',"User not found.");

        if($pizza){
            $pizza->updateOrCreate(array('id' => $id), array('archive' => "1" ))
            ->updateOrCreate(array('id' => $id), array('deleted_at' => $date ));
            if($pizza->save()){
                Session::flash('notification-status',"success");
                Session::flash('notification-message',"Success : Successfully move to archives.");
                Helper::audit(Auth::user()->id,Auth::user()->name,Auth::user()->sequence_no,"delete","Successfully deleted a pizza description");
                return redirect()->back();
            }else{
                Session::flash('notification-status',"failed");
                Session::flash('notification-message',"Error : There's an error while communicating the database server. Please try again.");
                Helper::audit(Auth::user()->id,Auth::user()->name,Auth::user()->sequence_no,"delete","Failed to delete a pizza description");
                return redirect()->back();
            }
        }
	}
}