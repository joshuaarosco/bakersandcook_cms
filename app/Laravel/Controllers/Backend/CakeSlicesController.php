<?php
namespace App\Http\Controllers;
namespace App\Laravel\Controllers\Backend;

use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\Authenticatescake_slices;
use App\Laravel\Models\CakeSlices;
use Carbon\Carbon;
use Session,Auth,Analytics,Period,File,Str,URL,Helper,Hash,Input;

use Illuminate\Contracts\Auth\Guard;

class CakeSlicesController extends Controller{

	/*protected $data;

	public function __construct () {
	  	$this->data = [];
	  	parent::__construct();
	  	array_merge($this->data, parent::get_data());
	}*/

	protected $data;
	public $restful=true;

	public function __construct(){
		$this->data = [];
	}

	public function create(){
		$food_name = Input::get('food_name');
		$type = Input::get('type');
        $cake_slices = CakeSlices::create([
          'food_name'=> $food_name,
          ]);

        if($cake_slices){
            Session::flash('notification-status','success');
            Session::flash('notification-message',"Successfully added.");
            Helper::audit(Auth::user()->id,Auth::user()->name,Auth::user()->type,"add","Successfully added ");
            return redirect('administrator/cakes_slices/list');
        }
        return redirect()->back();
	}

	public function restore($id){
        $cake_slices = CakeSlices::where('id','=',$id)->get();
        foreach($cake_slices as $index=>$info){
            $food_name = $info->food_name;
        }

        $cake_slices = CakeSlices::find($id);
        $date=date('Y-m-d H:i:s');

        if($cake_slices){
            $cake_slices->updateOrCreate(array('id' => $id), array('archive' => null));
            if($cake_slices->save()){
                Session::flash('notification-status',"success");
                Session::flash('notification-message',"Success : Successfully restore.");
                Helper::audit(Auth::user()->id,Auth::user()->name,Auth::user()->type,"restore"," Successfully restored a Cake & Slices: ".$food_name);
                return redirect()->back();
            }else{
                Session::flash('notification-status',"failed");
                Session::flash('notification-message',"Error : There's an error while communicating the database server. Please try again.");
                Helper::audit(Auth::user()->id,Auth::user()->name,Auth::user()->type,"restore","Failed to restore a Cake & Slices: ".$food_name);
                return redirect()->back();
            }
        }
	}

	public function update($id){
        $cake_slices = CakeSlices::find($id);
        $food_name = Input::get('food_name');
        $type = Input::get('type');

        $user = $cake_slices->fill([
            'food_name'=> $food_name,
            ]);

        if($user->save()){
            Session::flash('notification-status','success');
            Session::flash('notification-message',"Successfully updated.");
            Helper::audit(Auth::user()->id,Auth::user()->name,Auth::user()->type,"update","Successfully updated Cake & Slices:".$food_name);
            return redirect()->back();
        }
        return redirect()->back();
	}

	public function delete($id){
        $cake_slices = CakeSlices::where('id','=',$id)->get();
        foreach($cake_slices as $index=>$info){
            $food_name = $info->food_name;
        }
        $cake_slices = CakeSlices::find($id);
        $date=date('Y-m-d H:i:s');

        Session::flash('notification-status',"failed");
        Session::flash('notification-message',"User not found.");

        if($cake_slices){
            $cake_slices->updateOrCreate(array('id' => $id), array('archive' => "1" ))
            ->updateOrCreate(array('id' => $id), array('deleted_at' => $date ));
            if($cake_slices->save()){
                Session::flash('notification-status',"success");
                Session::flash('notification-message',"Success : Successfully move to archives.");
                Helper::audit(Auth::user()->id,Auth::user()->name,Auth::user()->type,"delete","Successfully deleted a Cake & Slices: ".$food_name);
                return redirect()->back();
            }else{
                Session::flash('notification-status',"failed");
                Session::flash('notification-message',"Error : There's an error while communicating the database server. Please try again.");
                Helper::audit(Auth::user()->id,Auth::user()->name,Auth::user()->type,"delete","Failed to delete a Cakes & Slices: ".$food_name);
                return redirect()->back();
            }
        }
	}
}