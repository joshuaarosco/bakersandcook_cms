<?php
namespace App\Http\Controllers;
namespace App\Laravel\Controllers\Backend;

use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\Authenticatesto_start;
use App\Laravel\Models\ToStart;
use Carbon\Carbon;
use Session,Auth,Analytics,Period,File,Str,URL,Helper,Hash,Input;

use Illuminate\Contracts\Auth\Guard;

class ToStartController extends Controller{

	protected $data;
	public $restful=true;

	public function __construct(){
		$this->data = [];
	}

	public function create(){
		$food_name = Input::get('food_name');
		$type = Input::get('type');
        $to_start = ToStart::create([
          'food_name'=> $food_name,
          'type'=> $type,
          ]);

        if($to_start){
            Session::flash('notification-status','success');
            Session::flash('notification-message',"Successfully created.");
            Helper::audit(Auth::user()->id,Auth::user()->name,Auth::user()->type,"add","Successfully added ");
            return redirect('administrator/plank/to_start/list');
        }
        return redirect()->back();
            
	}

	public function restore($id){
        $to_start = ToStart::where('id','=',$id)->get();
        foreach($to_start as $index=>$info){
            $food_name = $info->food_name;
            $type = $info->type;
        }

        $to_start = ToStart::find($id);
        $date=date('Y-m-d H:i:s');

        if($to_start){
            $to_start->updateOrCreate(array('id' => $id), array('archive' => null));
            if($to_start->save()){
                Session::flash('notification-status',"success");
                Session::flash('notification-message',"Success : Successfully restore.");
                Helper::audit(Auth::user()->id,Auth::user()->name,Auth::user()->type,"restore"," Successfully restored : ".$food_name);
                return redirect()->back();
            }else{
                Session::flash('notification-status',"failed");
                Session::flash('notification-message',"Error : There's an error while communicating the database server. Please try again.");
                Helper::audit(Auth::user()->id,Auth::user()->name,Auth::user()->type,"restore","Failed to restore : ".$food_name);
                return redirect()->back();
            }
        }
	}

	public function update($id){
        $to_start = ToStart::find($id);
        $food_name = Input::get('food_name');
        $type = Input::get('type');

        $user = $to_start->fill([
            'food_name'=> $food_name,
            'type'=> $type,
            ]);

        if($user->save()){
            Session::flash('notification-status','success');
            Session::flash('notification-message',"Successfully updated.");
            Helper::audit(Auth::user()->id,Auth::user()->name,Auth::user()->type,"update","Successfully updated bread & pastry:".$food_name);
            return redirect()->back();
        }
        return redirect()->back();
	}

	public function delete($id){
        $to_start = ToStart::where('id','=',$id)->get();
        foreach($to_start as $index=>$info){
            $food_name = $info->food_name;
            $type = $info->type;
        }
        $to_start = ToStart::find($id);
        $date=date('Y-m-d H:i:s');

        Session::flash('notification-status',"failed");
        Session::flash('notification-message',"User not found.");

        if($to_start){
            $to_start->updateOrCreate(array('id' => $id), array('archive' => "1" ))
            ->updateOrCreate(array('id' => $id), array('deleted_at' => $date ));
            if($to_start->save()){
                Session::flash('notification-status',"success");
                Session::flash('notification-message',"Success : Successfully move to archives.");
                Helper::audit(Auth::user()->id,Auth::user()->name,Auth::user()->type,"delete","Successfully deleted : ".$food_name);
                return redirect()->back();
            }else{
                Session::flash('notification-status',"failed");
                Session::flash('notification-message',"Error : There's an error while communicating the database server. Please try again.");
                Helper::audit(Auth::user()->id,Auth::user()->name,Auth::user()->type,"delete","Failed to delete : ".$food_name);
                return redirect()->back();
            }
        }
	}
}