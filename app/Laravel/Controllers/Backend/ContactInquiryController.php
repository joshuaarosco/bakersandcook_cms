<?php
namespace App\Http\Controllers;
namespace App\Laravel\Controllers\Backend;

use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\Authenticatesblogs;
use App\Laravel\Models\Inquiries;
use Carbon\Carbon;
use Input,Session,Auth,Analytics,Period,File,Str,URL,Helper,Hash;

use Illuminate\Contracts\Auth\Guard;

class ContactInquiryController extends Controller{

	/*protected $data;

	public function __construct () {
	  	$this->data = [];
	  	parent::__construct();
	  	array_merge($this->data, parent::get_data());
	}*/

	protected $data;
	public $restful=true;

	public function __construct(){
		$this->data = [];
	}

    public function contact(){
        $name = Input::get('name');
        $email = Input::get('email');
        $message = Input::get('message');
        $type = Input::get('type');
        
        $contact = Inquiries::create([
            'name' => $name,
            'type' => $type,
            'email' => $email,
            'message' => $message,
        ]);

        if($contact){
            Session::flash('notification-status','success');
            Session::flash('notification-message',"Successfully sent.");
            Helper::audit("***",$name,"guest","contact","Sent a contact inquiry");
            if($type=="plank"){
                return redirect('plank#contact');
            }else{
                return redirect('home#contact');
            }
        }else{
            Session::flash('notification-status','failed');
            Session::flash('notification-message',"Not sent.");
            if($type=="plank"){
                return redirect('plank#contact');
            }else{
                return redirect('home#contact');
            }
        }
    }

    public function restore($id){
        $inquiries = Inquiries::where('id','=',$id)->get();
        $inquiries = Inquiries::find($id);
        $date=date('Y-m-d H:i:s');

        if($inquiries){
            $inquiries->updateOrCreate(array('id' => $id), array('archive' => null));
            if($inquiries->save()){
                Session::flash('notification-status',"success");
                Session::flash('notification-message',"Success : inquiry successfully restore.");
                Helper::audit(Auth::user()->id,Auth::user()->name,Auth::user()->type,"restore","The inquiry was successfully restored");
                return redirect()->back();
            }else{
                Session::flash('notification-status',"failed");
                Session::flash('notification-message',"Error : There's an error while communicating the database server. Please try again.");
                Helper::audit(Auth::user()->id,Auth::user()->name,Auth::user()->type,"restore","The inquiry was failed to restore");
                return redirect()->back();
            }
        }
    }

    public function delete($id){
        $inquiries = Inquiries::where('id','=',$id)->get();
        $inquiries = Inquiries::find($id);
        $date=date('Y-m-d H:i:s');

        Session::flash('notification-status',"failed");
        Session::flash('notification-message',"inquiry not found.");

        if($inquiries){
            $inquiries->updateOrCreate(array('id' => $id), array('archive' => "1" ))
            ->updateOrCreate(array('id' => $id), array('deleted_at' => $date ));
            if($inquiries->save()){
                Session::flash('notification-status',"success");
                Session::flash('notification-message',"Success : Inquiry successfully move to archives.");
                Helper::audit(Auth::user()->id,Auth::user()->name,Auth::user()->type,"delete","The inquiry was successfully moved to archive");
                return redirect()->back();
            }else{
                Session::flash('notification-status',"failed");
                Session::flash('notification-message',"Error : There's an error while communicating the database server. Please try again.");
                Helper::audit(Auth::user()->id,Auth::user()->name,Auth::user()->type,"delete","The inquiry was failed to move to archive");
                return redirect()->back();
            }
        }
        return redirect()->back();
        
    }
}