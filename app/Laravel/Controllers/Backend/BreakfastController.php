<?php
namespace App\Http\Controllers;
namespace App\Laravel\Controllers\Backend;

use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\Authenticatesbreakfast;
use App\Laravel\Models\Breakfast;
use Carbon\Carbon;
use Session,Auth,Analytics,Period,File,Str,URL,Helper,Hash,Input;

use Illuminate\Contracts\Auth\Guard;

class BreakfastController extends Controller{

	/*protected $data;

	public function __construct () {
	  	$this->data = [];
	  	parent::__construct();
	  	array_merge($this->data, parent::get_data());
	}*/

	protected $data;
	public $restful=true;

	public function __construct(){
		$this->data = [];
	}

	public function create(){
		$food_name = Input::get('food_name');
		$type = Input::get('type');
        $breakfast = Breakfast::create([
          'food_name'=> $food_name,
          'type'=> $type,
          ]);

        if($breakfast){
            Session::flash('notification-status','success');
            Session::flash('notification-message',"Successfully created.");
            Helper::audit(Auth::user()->id,Auth::user()->name,Auth::user()->type,"add","Successfully added ");
            return redirect('administrator/breakfast/list');
        }
        return redirect()->back();
            
	}

	public function restore($id){
        $breakfast = Breakfast::where('id','=',$id)->get();
        foreach($breakfast as $index=>$info){
            $food_name = $info->food_name;
            $type = $info->type;
        }

        $breakfast = Breakfast::find($id);
        $date=date('Y-m-d H:i:s');

        if($breakfast){
            $breakfast->updateOrCreate(array('id' => $id), array('archive' => null));
            if($breakfast->save()){
                Session::flash('notification-status',"success");
                Session::flash('notification-message',"Success : Successfully restore.");
                Helper::audit(Auth::user()->id,Auth::user()->name,Auth::user()->type,"restore"," Successfully restored a ".$type.": ".$food_name);
                return redirect()->back();
            }else{
                Session::flash('notification-status',"failed");
                Session::flash('notification-message',"Error : There's an error while communicating the database server. Please try again.");
                Helper::audit(Auth::user()->id,Auth::user()->name,Auth::user()->type,"restore","Failed to restore a ".$type.": ".$food_name);
                return redirect()->back();
            }
        }
	}

	public function update($id){
        $breakfast = Breakfast::find($id);
        $food_name = Input::get('food_name');
        $type = Input::get('type');

        $user = $breakfast->fill([
            'food_name'=> $food_name,
            'type'=> $type,
            ]);

        if($user->save()){
            Session::flash('notification-status','success');
            Session::flash('notification-message',"Successfully updated.");
            Helper::audit(Auth::user()->id,Auth::user()->name,Auth::user()->type,"update","Successfully updated bread & pastry:".$food_name);
            return redirect()->back();
        }
        return redirect()->back();
	}

	public function delete($id){
        $breakfast = Breakfast::where('id','=',$id)->get();
        foreach($breakfast as $index=>$info){
            $food_name = $info->food_name;
            $type = $info->type;
        }
        $breakfast = Breakfast::find($id);
        $date=date('Y-m-d H:i:s');

        Session::flash('notification-status',"failed");
        Session::flash('notification-message',"User not found.");

        if($breakfast){
            $breakfast->updateOrCreate(array('id' => $id), array('archive' => "1" ))
            ->updateOrCreate(array('id' => $id), array('deleted_at' => $date ));
            if($breakfast->save()){
                Session::flash('notification-status',"success");
                Session::flash('notification-message',"Success : Successfully move to archives.");
                Helper::audit(Auth::user()->id,Auth::user()->name,Auth::user()->type,"delete","Successfully deleted a ".$type.": ".$food_name);
                return redirect()->back();
            }else{
                Session::flash('notification-status',"failed");
                Session::flash('notification-message',"Error : There's an error while communicating the database server. Please try again.");
                Helper::audit(Auth::user()->id,Auth::user()->name,Auth::user()->type,"delete","Failed to delete a ".$type.": ".$food_name);
                return redirect()->back();
            }
        }
	}
}