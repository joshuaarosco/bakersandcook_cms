<?php
namespace App\Http\Controllers;
namespace App\Laravel\Controllers\Backend;

use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\Authenticatesitems;
use App\Laravel\Models\Items;
use Carbon\Carbon;
use Session,Auth,Analytics,Period,File,Str,URL,Helper,Hash,Input;

use Illuminate\Contracts\Auth\Guard;

class ItemsController extends Controller{

	/*protected $data;

	public function __construct () {
	  	$this->data = [];
	  	parent::__construct();
	  	array_merge($this->data, parent::get_data());
	}*/

	protected $data;
	public $restful=true;

	public function __construct(){
		$this->data = [];
	}

	public function create(){
		$item_name = Input::get('item_name');
		$type = Input::get('type');
        $items = Items::create([
          'item_name'=> $item_name,
          ]);

        if($items){
            Session::flash('notification-status','success');
            Session::flash('notification-message',"Successfully added.");
            Helper::audit(Auth::user()->id,Auth::user()->name,Auth::user()->type,"add","Successfully added ");
            return redirect('administrator/items/list');
        }
        return redirect()->back();
	}

	public function restore($id){
        $items = Items::where('id','=',$id)->get();
        foreach($items as $index=>$info){
            $item_name = $info->item_name;
        }

        $items = Items::find($id);
        $date=date('Y-m-d H:i:s');

        if($items){
            $items->updateOrCreate(array('id' => $id), array('archive' => null));
            if($items->save()){
                Session::flash('notification-status',"success");
                Session::flash('notification-message',"Success : Successfully restore.");
                Helper::audit(Auth::user()->id,Auth::user()->name,Auth::user()->type,"restore"," Successfully restored an item: ".$item_name);
                return redirect()->back();
            }else{
                Session::flash('notification-status',"failed");
                Session::flash('notification-message',"Error : There's an error while communicating the database server. Please try again.");
                Helper::audit(Auth::user()->id,Auth::user()->name,Auth::user()->type,"restore","Failed to restore an item: ".$item_name);
                return redirect()->back();
            }
        }
	}

	public function update($id){
        $items = Items::find($id);
        $item_name = Input::get('item_name');
        $type = Input::get('type');

        $user = $items->fill([
            'item_name'=> $item_name,
            ]);

        if($user->save()){
            Session::flash('notification-status','success');
            Session::flash('notification-message',"Successfully updated.");
            Helper::audit(Auth::user()->id,Auth::user()->name,Auth::user()->type,"update","Successfully updated an item:".$item_name);
            return redirect()->back();
        }
        return redirect()->back();
	}

	public function delete($id){
        $items = Items::where('id','=',$id)->get();
        foreach($items as $index=>$info){
            $item_name = $info->item_name;
        }
        $items = Items::find($id);
        $date=date('Y-m-d H:i:s');

        Session::flash('notification-status',"failed");
        Session::flash('notification-message',"User not found.");

        if($items){
            $items->updateOrCreate(array('id' => $id), array('archive' => "1" ))
            ->updateOrCreate(array('id' => $id), array('deleted_at' => $date ));
            if($items->save()){
                Session::flash('notification-status',"success");
                Session::flash('notification-message',"Success : Successfully move to archives.");
                Helper::audit(Auth::user()->id,Auth::user()->name,Auth::user()->type,"delete","Successfully deleted an item: ".$item_name);
                return redirect()->back();
            }else{
                Session::flash('notification-status',"failed");
                Session::flash('notification-message',"Error : There's an error while communicating the database server. Please try again.");
                Helper::audit(Auth::user()->id,Auth::user()->name,Auth::user()->type,"delete","Failed to delete an item: ".$item_name);
                return redirect()->back();
            }
        }
	}
}