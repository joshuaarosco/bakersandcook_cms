<?php
namespace App\Http\Controllers;
namespace App\Laravel\Controllers\Backend;

use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Laravel\Models\Users;
use Carbon\Carbon;
use Input,Session,Auth,Analytics,Period,Helper;

use Illuminate\Contracts\Auth\Guard;

class LoginController extends Controller{

	/*protected $data;

	public function __construct () {
	  	$this->data = [];
	  	parent::__construct();
	  	array_merge($this->data, parent::get_data());
	}*/

	protected $data;
	public $restful=true;

	public function __construct(){
		$this->data = [];
	}

	//this function is used to view the landing page of the website
	public function login(){
		$email = Input::get('email');
    	$password = Input::get('password');

    	if(Auth::attempt(['email' => $email, 'password' => $password])){
    		if(Auth::user()->archive == "1"){
    			Auth::logout();
    			Session::flash('notification-status','failed');
		    	Session::flash('notification-message',"Invalid email account. Please try again.");
		    	Helper::audit("***","***","***","login","Failed to login.");
		    	return redirect()->back();
    		}else{
    			Session::flash('notification-status','info');
    			Session::flash('notification-message',"Successfully Login.");
    			Helper::audit(Auth::user()->id,Auth::user()->name,Auth::user()->type,"login","Successfully login");
    			return redirect('/administrator');
    		}
    	}

    	Session::flash('notification-status','failed');
    	Session::flash('notification-message',"Invalid email account. Please try again.");
    	Helper::audit("***","***","***","login","Failed to login.");
    	return redirect()->back();
	}

	public function destroy(){
		if(Auth::check()){
			$type = Auth::user()->type;
			Helper::audit(Auth::user()->id,Auth::user()->name,Auth::user()->type,"logout","Successfully logout.");
			Auth::logout();
			Session::flash('notification-status','success');
			Session::flash('notification-message',"Successfully logged out.");
			return redirect('administrator/login');
		}
		return redirect()->back();
	}
}