<?php
namespace App\Http\Controllers;
namespace App\Laravel\Controllers\Frontend;

use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\Authenticatesentrees_sweets;
use App\Laravel\Models\EntreesSweets;
use App\Laravel\Models\AuditTrail;
use Carbon\Carbon;
use Input,Session,Auth,Analytics,Period;

use Illuminate\Contracts\Auth\Guard;

class EntreesSweetsController extends Controller{

	protected $data;
	public $restful=true;

	public function __construct(){
		$this->data = [];
	}

	public function index(){
		if(!Auth::check()){
			return redirect('');
		}else{
			$this->data['entrees_sweets'] = EntreesSweets::where('archive','=',null)->get();
			return view('backend._content.entrees_sweets.list',$this->data);
		}
	}

	public function archives(){
		if(!Auth::check()){
			return redirect('');
		}else{
			$this->data['entrees_sweets'] = EntreesSweets::where('archive','=','1')->get();
			return view('backend._content.entrees_sweets.archives',$this->data);
		}
	}

	public function create(){
		if(!Auth::check()){
			return redirect('');
		}else{
			return view('backend._content.entrees_sweets.create');
		}
	}
}