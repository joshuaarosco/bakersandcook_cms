<?php
namespace App\Http\Controllers;
namespace App\Laravel\Controllers\Frontend;

use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesPlankHeaders;
use App\Laravel\Models\PlankHeader;
use App\Laravel\Models\AuditTrail;
use Carbon\Carbon;
use Input,Session,Auth,Analytics,Period;

use Illuminate\Contracts\Auth\Guard;

class PlankHeaderController extends Controller{

	protected $data;
	public $restful=true;

	public function __construct(){
		$this->data = [];
	}

	public function index(){
		if(!Auth::check()){
			return redirect('');
		}else{
			$this->data['headers'] = PlankHeader::where('archive','=',null)->get();
			return view('backend._content.plank_header.list',$this->data);
		}
	}
	
	public function view($id){
		if(!Auth::check()){
			return redirect('');
		}else{
			$headers = PlankHeader::where('id','=',$id)->get();
			$count = PlankHeader::count();
			$type = PlankHeader::all(); 

			if($headers->count()!=0){
				return view('backend._content.plank_header.view')
				->with('headers',$headers)
				->with('count',$count)
				->with('type',$type)
				->with('id',$id);
			}else{
				return redirect()->back();
			}
		}
		
	}

	public function archives(){
		if(!Auth::check()){
			return redirect('');
		}else{
			$headers = PlankHeader::where('archive','=','1')->get();
			return view('backend._content.plank_header.archives')
			->with('headers',$headers);
		}
	}

	public function create(){
		if(!Auth::check()){
			return redirect('');
		}else{
			$headers = PlankHeader::where('archive','=',null)->get();
			$this->data['count'] = $headers->count();
			return view('backend._content.plank_header.create',$this->data);
		}
	}
}