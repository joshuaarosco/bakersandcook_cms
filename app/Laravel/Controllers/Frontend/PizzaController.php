<?php
namespace App\Http\Controllers;
namespace App\Laravel\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Laravel\Models\Pizza;
use App\Laravel\Models\AuditTrail;
use Carbon\Carbon;
use Input,Session,Auth,Analytics,Period;

use Illuminate\Contracts\Auth\Guard;

class PizzaController extends Controller{

	/*protected $data;

	public function __construct () {
	  	$this->data = [];
	  	parent::__construct();
	  	array_merge($this->data, parent::get_data());
	}*/

	protected $data;
	public $restful=true;

	public function __construct(){
		$this->data = [];
	}

	public function index(){
		if(!Auth::check()){
			return redirect('');
		}else{
			$this->data['count'] = Pizza::where('archive','=',null)->count();
			$this->data['pizza'] = Pizza::where('archive','=',null)->get();
			return view('backend._content.pizza.list',$this->data);
		}
	}

	public function archives(){
		if(!Auth::check()){
			return redirect('');
		}else{
			$this->data['count'] = Pizza::where('archive','=','1')->count();
			$this->data['pizza'] = Pizza::where('archive','=','1')->get();
			return view('backend._content.pizza.archives',$this->data);
		}
	}

	public function create(){
		if(!Auth::check()){
			return redirect('');
		}else{
			$this->data['count'] = Pizza::where('archive','=',null)->count();
			return view('backend._content.pizza.create',$this->data);
		}
	}
}