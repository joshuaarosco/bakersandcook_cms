<?php
namespace App\Http\Controllers;
namespace App\Laravel\Controllers\Frontend;

use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\Authenticatesbreakfast;
use App\Laravel\Models\Breakfast;
use App\Laravel\Models\AuditTrail;
use Carbon\Carbon;
use Input,Session,Auth,Analytics,Period;

use Illuminate\Contracts\Auth\Guard;

class BreakfastController extends Controller{

	/*protected $data;

	public function __construct () {
	  	$this->data = [];
	  	parent::__construct();
	  	array_merge($this->data, parent::get_data());
	}*/

	protected $data;
	public $restful=true;

	public function __construct(){
		$this->data = [];
	}

	public function index(){
		if(!Auth::check()){
			return redirect('');
		}else{
			$this->data['breakfast'] = Breakfast::where('archive','=',null)->get();
			return view('backend._content.breakfast.list',$this->data);
		}
	}
	
	public function view($id){
		if(!Auth::check()){
			return redirect('');
		}else{
			$breakfast = Breakfast::where('id','=',$id)->get();;

			if($breakfast->count()!=0){
				return view('backend._content.breakfast.view')
				->with('breakfast',$breakfast);
			}else{
				return redirect()->back();
			}
		}
		
	}

	public function archives(){
		if(!Auth::check()){
			return redirect('');
		}else{
			$this->data['breakfast'] = Breakfast::where('archive','=','1')->get();
			return view('backend._content.breakfast.archives',$this->data);
		}
	}

	public function create(){
		/*if(!Auth::check()){
			return redirect('');
		}else{*/
			return view('backend._content.breakfast.create');
		//}
	}
}