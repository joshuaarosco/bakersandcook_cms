<?php
namespace App\Http\Controllers;
namespace App\Laravel\Controllers\Frontend;

use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Laravel\Models\BreadPastries;
use App\Laravel\Models\CakeSlices;
use App\Laravel\Models\TartsPies;
use App\Laravel\Models\Breakfast;
use App\Laravel\Models\Pizza;
use App\Laravel\Models\Items;
use App\Laravel\Models\EntreesSweets;
use App\Laravel\Models\ImageSlider;
use App\Laravel\Models\PlankImageSlider;
use App\Laravel\Models\Header;
use App\Laravel\Models\PlankHeader;
use App\Laravel\Models\About;
use App\Laravel\Models\PLankAbout;
use App\Laravel\Models\ToStart;
use Carbon\Carbon;
use Input,Session,Auth,Analytics,Period,Helper,DB;

use Illuminate\Contracts\Auth\Guard;

class HomeController extends Controller{

	/*protected $data;

	public function __construct () {
	  	$this->data = [];
	  	parent::__construct();
	  	array_merge($this->data, parent::get_data());
	}*/

	protected $data;
	public $restful=true;

	public function __construct(){
		$this->data = [];
	}

	//this function is used to view the landing page of the website
	public function index(){
		if(Auth::check()){
			return redirect('administrator');
		}else{
			$this->data['bread_pastries'] = BreadPastries::where('archive','=',null)->get();
			$this->data['cakes_slices'] = CakeSlices::where('archive','=',null)->get();
			$this->data['tarts_pies'] = TartsPies::where('archive','=',null)->get();
			$this->data['breakfast'] = Breakfast::where('archive','=',null)->get()->toArray();
			$this->data['breakfasts'] = Breakfast::where('archive','=',null)->get();

			$this->data['salad'] = Breakfast::where('type','salads')->whereRaw(DB::raw("archive IS NULL"))->count();
			$this->data['items'] = Items::where('archive','=',null)->get();
			$this->data['image_slider'] = ImageSlider::where('archive','=',null)->get();
			$this->data['header'] = Header::where('archive','=',null)->get();
			$this->data['abouts'] = About::where('archive','=',null)->get();
			return view('frontend.index',$this->data);
		}
	}

	public function plank(){
		if(Auth::check()){
			return redirect('administrator');
		}else{
			$this->data['image_slider'] = PlankImageSlider::where('archive','=',null)->get();
			$this->data['entrees_sweets'] = EntreesSweets::where('archive','=',null)->get();
			$this->data['pizza'] = Pizza::where('archive','=',null)->orderBy('sequence_no')->get()->toArray();
			$this->data['header'] = PlankHeader::where('archive','=',null)->get();
			$this->data['abouts'] = PlankAbout::where('archive','=',null)->get();
			$this->data['start'] = ToStart::where('archive','=',null)->get();
			return view('frontend.plank',$this->data);
		}
	}

	public function error(){
		return view('error_404');
	}
}