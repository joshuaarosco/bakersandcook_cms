<?php
namespace App\Http\Controllers;
namespace App\Laravel\Controllers\Frontend;

use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\Authenticatesbread_pastries;
use App\Laravel\Models\bread_pastries;
use App\Laravel\Models\BreadPastries;
use App\Laravel\Models\AuditTrail;
use Carbon\Carbon;
use Input,Session,Auth,Analytics,Period;

use Illuminate\Contracts\Auth\Guard;

class BreadPastriesController extends Controller{

	/*protected $data;

	public function __construct () {
	  	$this->data = [];
	  	parent::__construct();
	  	array_merge($this->data, parent::get_data());
	}*/

	protected $data;
	public $restful=true;

	public function __construct(){
		$this->data = [];
	}

	public function index(){
		if(!Auth::check()){
			return redirect('');
		}else{
			$this->data['bread_pastries'] = BreadPastries::where('archive','=',null)->get();
			return view('backend._content.bread_pastries.list',$this->data);
		}
	}
	
	public function view($id){
		if(!Auth::check()){
			return redirect('');
		}else{
			$bread_pastries = BreadPastries::where('id','=',$id)->get();;

			if($bread_pastries->count()!=0){
				return view('backend._content.bread_pastries.view')
				->with('bread_pastries',$bread_pastries);
			}else{
				return redirect()->back();
			}
		}
		
	}

	public function archives(){
		if(!Auth::check()){
			return redirect('');
		}else{
			$this->data['bread_pastries'] = BreadPastries::where('archive','=','1')->get();
			return view('backend._content.bread_pastries.archives',$this->data);
		}
	}

	public function create(){
		/*if(!Auth::check()){
			return redirect('');
		}else{*/
			return view('backend._content.bread_pastries.create');
		//}
	}
}