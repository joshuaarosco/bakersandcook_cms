<?php
namespace App\Http\Controllers;
namespace App\Laravel\Controllers\Frontend;

use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Laravel\Models\AuditTrail;
use Carbon\Carbon;
use Input,Session,Auth,Analytics,Period;

use Illuminate\Contracts\Auth\Guard;

class AuditController extends Controller{

	/*protected $data;

	public function __construct () {
	  	$this->data = [];
	  	parent::__construct();
	  	array_merge($this->data, parent::get_data());
	}*/

	protected $data;
	public $restful=true;

	public function __construct(){
		$this->data = [];
	}

	public function index(){
		if(!Auth::check()){
			return redirect('');
		}else{
			$audits = AuditTrail::orderBy('created_at','DESC')->get();
			return view('backend._content.audit.list')
			->with('audits',$audits);
		}
	}

	/*public function archives(){
		if(!Auth::check()){
			return redirect('');
		}else{
			$audits = AuditTrail::where('archive','=','1')->get();
			return view('backend._content.audit.archives')
			->with('audits',$audits);
		}
	}*/
}