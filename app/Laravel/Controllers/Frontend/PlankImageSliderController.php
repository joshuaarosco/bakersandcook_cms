<?php
namespace App\Http\Controllers;
namespace App\Laravel\Controllers\Frontend;

use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\Authenticatesimage_slider;
use App\Laravel\Models\PlankImageSlider;
use App\Laravel\Models\AuditTrail;
use Carbon\Carbon;
use Input,Session,Auth,Analytics,Period;

use Illuminate\Contracts\Auth\Guard;

class PlankImageSliderController extends Controller{

	protected $data;
	public $restful=true;

	public function __construct(){
		$this->data = [];
		$this->data['types']=[ '' => "Choose type", 'start' => "Start", 'pizza' => "Pizza", 'entrees-sweets' => "Entrees Sweets"];
	}

	public function index(){
		if(!Auth::check()){
			return redirect('');
		}else{
			$this->data['image_slider'] = PlankImageSlider::where('archive','=',null)->get();
			return view('backend._content.plank_image_slider.list',$this->data);
		}
	}

	public function archives(){
		if(!Auth::check()){
			return redirect('');
		}else{
			$this->data['image_slider'] = PlankImageSlider::where('archive','=','1')->get();
			return view('backend._content.plank_image_slider.archives',$this->data);
		}
	}

	public function view($id){
		if(!Auth::check()){
			return redirect('');
		}else{
			$this->data['image_slider'] = PlankImageSlider::where('id','=',$id)->get();
			if($this->data['image_slider']->count()!=0){
				return view('backend._content.plank_image_slider.view',$this->data);
			}else{
				return redirect()->back();
			}
		}
		
	}

	public function create(){
		if(!Auth::check()){
			return redirect('');
		}else{
			$this->data['start'] =  PlankImageSlider::where('archive','=',null)->where('type','=','start')->count();
			$this->data['pizza'] =  PlankImageSlider::where('archive','=',null)->where('type','=','pizza')->count();
			return view('backend._content.plank_image_slider.create',$this->data);
		}
	}
}