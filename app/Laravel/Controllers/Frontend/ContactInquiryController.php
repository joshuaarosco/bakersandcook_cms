<?php
namespace App\Http\Controllers;
namespace App\Laravel\Controllers\Frontend;

use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\Authenticatesusers;
use App\Laravel\Models\Inquiries;
use App\Laravel\Models\AuditTrail;
use Carbon\Carbon;
use Input,Session,Auth,Analytics,Period;

use Illuminate\Contracts\Auth\Guard;

class ContactInquiryController extends Controller{

	/*protected $data;

	public function __construct () {
	  	$this->data = [];
	  	parent::__construct();
	  	array_merge($this->data, parent::get_data());
	}*/

	protected $data;
	public $restful=true;

	public function __construct(){
		$this->data = [];
	}

	public function index(){
		if(!Auth::check()){
			return redirect('');
		}else{
			$inquiries = Inquiries::where('archive','=',null)->get();
			return view('backend._content.inquiry.list')
			->with('inquiries',$inquiries);
		}
	}

	public function archives(){
		if(!Auth::check()){
			return redirect('');
		}else{
			$inquiries = Inquiries::where('archive','=','1')->get();
			return view('backend._content.inquiry.archives')
			->with('inquiries',$inquiries);
		}
	}
}