<?php
namespace App\Http\Controllers;
namespace App\Laravel\Controllers\Frontend;

use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Laravel\Models\WebContact;
use Carbon\Carbon;
use Input,Session,Auth,Analytics,Period,Helper;

use Illuminate\Contracts\Auth\Guard;

class LoginController extends Controller{

	/*protected $data;

	public function __construct () {
	  	$this->data = [];
	  	parent::__construct();
	  	array_merge($this->data, parent::get_data());
	}*/

	protected $data;
	public $restful=true;

	public function __construct(){
		$this->data = [];
	}

	//this function is used to view the landing page of the website
	public function index(){
		if(Auth::check()){
			return redirect('administrator');
		}else{
			return view('frontend.login');
		}
	}
}