<?php
namespace App\Http\Controllers;
namespace App\Laravel\Controllers\Frontend;

use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\Authenticatesitems;
use App\Laravel\Models\Items;
use App\Laravel\Models\AuditTrail;
use Carbon\Carbon;
use Input,Session,Auth,Analytics,Period;

use Illuminate\Contracts\Auth\Guard;

class ItemsController extends Controller{

	/*protected $data;

	public function __construct () {
	  	$this->data = [];
	  	parent::__construct();
	  	array_merge($this->data, parent::get_data());
	}*/

	protected $data;
	public $restful=true;

	public function __construct(){
		$this->data = [];
	}

	public function index(){
		if(!Auth::check()){
			return redirect('');
		}else{
			$this->data['items'] = Items::where('archive','=',null)->get();
			return view('backend._content.items.list',$this->data);
		}
	}

	public function archives(){
		if(!Auth::check()){
			return redirect('');
		}else{
			$this->data['items'] = Items::where('archive','=','1')->get();
			return view('backend._content.items.archives',$this->data);
		}
	}

	public function create(){
		return view('backend._content.items.create');
	}
}