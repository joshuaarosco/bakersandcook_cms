<?php
namespace App\Http\Controllers;
namespace App\Laravel\Controllers\Frontend;

use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Laravel\Models\Users;
use App\Laravel\Models\AuditTrail;
use Carbon\Carbon;
use Input,Session,Auth,Analytics,Period;

use Illuminate\Contracts\Auth\Guard;

class UserController extends Controller{

	/*protected $data;

	public function __construct () {
	  	$this->data = [];
	  	parent::__construct();
	  	array_merge($this->data, parent::get_data());
	}*/

	protected $data;
	public $restful=true;

	public function __construct(){
		$this->data = [];
	}

	public function index(){
		if(!Auth::check()){
			return redirect('');
		}else{
			$users = Users::where('archive','=',null)->get();
			return view('backend._content.user.list')
			->with('users',$users);
		}
	}
	
	public function view($id){
		if(!Auth::check()){
			return redirect('');
		}else{
			$users = Users::where('id','=',$id)->get();
			$count = Users::count();
			$type = Users::all(); 
			$audit = AuditTrail::where('user_id','=',$id)
			->orderBy('created_at','DESC')
			->get();

			if($users->count()!=0){
				return view('backend._content.user.view')
				->with('users',$users)
				->with('count',$count)
				->with('type',$type)
				->with('audit',$audit)
				->with('id',$id);
			}else{
				return redirect()->back();
			}
		}
		
	}

	public function archives(){
		if(!Auth::check()){
			return redirect('');
		}else{
			$users = Users::where('archive','=','1')->get();
			return view('backend._content.user.archives')
			->with('users',$users);
		}
	}

	public function create(){
		/*if(!Auth::check()){
			return redirect('');
		}else{*/
			return view('backend._content.user.create');
		//}
	}
}