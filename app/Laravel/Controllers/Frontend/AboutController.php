<?php
namespace App\Http\Controllers;
namespace App\Laravel\Controllers\Frontend;

use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\Authenticatesabouts;
use App\Laravel\Models\About;
use App\Laravel\Models\AuditTrail;
use Carbon\Carbon;
use Input,Session,Auth,Analytics,Period;

use Illuminate\Contracts\Auth\Guard;

class AboutController extends Controller{

	/*protected $data;

	public function __construct () {
	  	$this->data = [];
	  	parent::__construct();
	  	array_merge($this->data, parent::get_data());
	}*/

	protected $data;
	public $restful=true;

	public function __construct(){
		$this->data = [];
	}

	public function index(){
		if(!Auth::check()){
			return redirect('');
		}else{
			$this->data['abouts'] = About::where('archive','=',null)->get();
			$this->data['count'] = About::where('archive','=',null)->count();
			return view('backend._content.about.list',$this->data);
		}
	}

	public function archives(){
		if(!Auth::check()){
			return redirect('');
		}else{
			$this->data['abouts'] = About::where('archive','=','1')->get();
			return view('backend._content.about.archives',$this->data);
		}
	}

	public function create(){
		if(!Auth::check()){
			return redirect('');
		}else{
			$this->data['abouts'] = About::where('archive','=',null)->get();
			$this->data['count'] = About::where('archive','=',null)->count();
			$this->data['count_image'] = $this->data['abouts']->count();
			return view('backend._content.about.create',$this->data);
		}
	}
}