<?php
namespace App\Http\Controllers;
namespace App\Laravel\Controllers\Frontend;

use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\Authenticatestarts_pies;
use App\Laravel\Models\TartsPies;
use App\Laravel\Models\AuditTrail;
use Carbon\Carbon;
use Input,Session,Auth,Analytics,Period;

use Illuminate\Contracts\Auth\Guard;

class TartsPiesController extends Controller{

	/*protected $data;

	public function __construct () {
	  	$this->data = [];
	  	parent::__construct();
	  	array_merge($this->data, parent::get_data());
	}*/

	protected $data;
	public $restful=true;

	public function __construct(){
		$this->data = [];
	}

	public function index(){
		if(!Auth::check()){
			return redirect('');
		}else{
			$this->data['tarts_pies'] = TartsPies::where('archive','=',null)->get();
			return view('backend._content.tarts_pies.list',$this->data);
		}
	}

	public function archives(){
		if(!Auth::check()){
			return redirect('');
		}else{
			$this->data['tarts_pies'] = TartsPies::where('archive','=','1')->get();
			return view('backend._content.tarts_pies.archives',$this->data);
		}
	}

	public function create(){
		return view('backend._content.tarts_pies.create');
	}
}