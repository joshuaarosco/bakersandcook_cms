<?php
namespace App\Http\Controllers;
namespace App\Laravel\Controllers\Frontend;

use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\Authenticatesimage_slider;
use App\Laravel\Models\ImageSlider;
use App\Laravel\Models\AuditTrail;
use Carbon\Carbon;
use Input,Session,Auth,Analytics,Period;

use Illuminate\Contracts\Auth\Guard;

class ImageSliderController extends Controller{

	protected $data;
	public $restful=true;

	public function __construct(){
		$this->data = [];
		$this->data['types']=[ '' => "Choose type", 'bread-pastries' => "Bread Pastries", 'cakes-slices' => "Cakes Slices",'tarts-pies' => "Tarts Pies",'breakfast' => "Breakfast",'items'=>"Items"];
	}

	public function index(){
		if(!Auth::check()){
			return redirect('');
		}else{
			$this->data['image_slider'] = ImageSlider::where('archive','=',null)->get();
			return view('backend._content.image_slider.list',$this->data);
		}
	}

	public function archives(){
		if(!Auth::check()){
			return redirect('');
		}else{
			$this->data['image_slider'] = ImageSlider::where('archive','=','1')->get();
			return view('backend._content.image_slider.archives',$this->data);
		}
	}

	public function view($id){
		if(!Auth::check()){
			return redirect('');
		}else{
			$this->data['image_slider'] = ImageSlider::where('id','=',$id)->get();
			if($this->data['image_slider']->count()!=0){
				return view('backend._content.image_slider.view',$this->data);
			}else{
				return redirect()->back();
			}
		}
		
	}

	public function create(){
		if(!Auth::check()){
			return redirect('');
		}else{
			$this->data['bread_pastries'] =  ImageSlider::where('archive','=',null)->where('type','=','bread-pastries')->count();
			$this->data['cakes_slices'] =  ImageSlider::where('archive','=',null)->where('type','=','cakes-slices')->count();
			$this->data['tarts_pies'] =  ImageSlider::where('archive','=',null)->where('type','=','tarts-pies')->count();
			$this->data['breakfast'] =  ImageSlider::where('archive','=',null)->where('type','=','breakfast')->count();
			$this->data['items'] =  ImageSlider::where('archive','=',null)->where('type','=','items')->count();
			return view('backend._content.image_slider.create',$this->data);
		}
	}
}