<?php

$this->group(['namespace'=> "Frontend",'middleware' => "web",'as' => "frontend."],function(){
	$this->group(['prefix' => "",'as' => ""],function(){
		$this->get('',['as'=>"index",'uses' => "HomeController@index"]);
		$this->get('home',['as'=>"index",'uses' => "HomeController@index"]);
		$this->get('plank',['as'=>"index",'uses' => "HomeController@plank"]);
		$this->get('404',['as'=>"error",'uses' => "HomeController@error"]);
		$this->group(['prefix' => "administrator",'as' => ""],function(){
			$this->get('',['as'=>"index",'uses' => "AdminController@index"]);
			$this->get('login',['as'=>"login",'uses' => "LoginController@index"]);
			$this->group(['prefix' => "user",'as' => ""],function(){
				$this->get('list',['as'=>"list",'uses' => "UserController@index"]);
				$this->get('create',['as'=>"create",'uses' => "UserController@create"]);
				$this->get('view/{id}',['as'=>"view",'uses' => "UserController@view"]);
				$this->get('archives',['as'=>"view",'uses' => "UserController@archives"]);
			});
			$this->group(['prefix' => "header",'as' => ""],function(){
				$this->get('list',['as'=>"list",'uses' => "HeaderController@index"]);
				$this->get('create',['as'=>"create",'uses' => "HeaderController@create"]);
				$this->get('view/{id}',['as'=>"view",'uses' => "HeaderController@view"]);
				$this->get('archives',['as'=>"view",'uses' => "HeaderController@archives"]);
			});
			$this->group(['prefix' => "about",'as' => ""],function(){
				$this->get('list',['as'=>"list",'uses' => "AboutController@index"]);
				$this->get('create',['as'=>"create",'uses' => "AboutController@create"]);
				$this->get('archives',['as'=>"view",'uses' => "AboutController@archives"]);
			});
			$this->group(['prefix' => "bread_pastries",'as' => ""],function(){
				$this->get('list',['as'=>"list",'uses' => "BreadPastriesController@index"]);
				$this->get('create',['as'=>"create",'uses' => "BreadPastriesController@create"]);
				$this->get('archives',['as'=>"view",'uses' => "BreadPastriesController@archives"]);
			});
			$this->group(['prefix' => "cakes_slices",'as' => ""],function(){
				$this->get('list',['as'=>"list",'uses' => "CakeSlicesController@index"]);
				$this->get('create',['as'=>"create",'uses' => "CakeSlicesController@create"]);
				$this->get('archives',['as'=>"view",'uses' => "CakeSlicesController@archives"]);
			});
			$this->group(['prefix' => "tarts_pies",'as' => ""],function(){
				$this->get('list',['as'=>"list",'uses' => "TartsPiesController@index"]);
				$this->get('create',['as'=>"create",'uses' => "TartsPiesController@create"]);
				$this->get('archives',['as'=>"view",'uses' => "TartsPiesController@archives"]);
			});
			$this->group(['prefix' => "breakfast",'as' => ""],function(){
				$this->get('list',['as'=>"list",'uses' => "BreakfastController@index"]);
				$this->get('create',['as'=>"create",'uses' => "BreakfastController@create"]);
				$this->get('archives',['as'=>"view",'uses' => "BreakfastController@archives"]);
			});
			$this->group(['prefix' => "items",'as' => ""],function(){
				$this->get('list',['as'=>"list",'uses' => "ItemsController@index"]);
				$this->get('create',['as'=>"create",'uses' => "ItemsController@create"]);
				$this->get('archives',['as'=>"view",'uses' => "ItemsController@archives"]);
			});
			$this->group(['prefix' => "inquiry",'as' => ""],function(){
				$this->get('list',['as'=>"list",'uses' => "ContactInquiryController@index"]);
				$this->get('create',['as'=>"create",'uses' => "ContactInquiryController@create"]);
				$this->get('archives',['as'=>"view",'uses' => "ContactInquiryController@archives"]);
			});
			$this->group(['prefix' => "image",'as' => ""],function(){
				$this->get('list',['as'=>"list",'uses' => "ImageSliderController@index"]);
				$this->get('create',['as'=>"create",'uses' => "ImageSliderController@create"]);
				$this->get('view/{id}',['as'=>"view",'uses' => "ImageSliderController@view"]);
				$this->get('archives',['as'=>"view",'uses' => "ImageSliderController@archives"]);
			});
			$this->group(['prefix' => "header",'as' => ""],function(){
				$this->get('list',['as'=>"list",'uses' => "HeaderController@index"]);
				$this->get('create',['as'=>"create",'uses' => "HeaderController@create"]);
				$this->get('view/{id}',['as'=>"view",'uses' => "HeaderController@view"]);
				$this->get('archives',['as'=>"view",'uses' => "HeaderController@archives"]);
			});
			$this->group(['prefix' => "plank",'as' => ""],function(){
				$this->group(['prefix' => "to_start",'as' => ""],function(){
					$this->get('list',['as'=>"list",'uses' => "ToStartController@index"]);
					$this->get('create',['as'=>"create",'uses' => "ToStartController@create"]);
					$this->get('archives',['as'=>"view",'uses' => "ToStartController@archives"]);
				});
				$this->group(['prefix' => "pizza",'as' => ""],function(){
					$this->get('list',['as'=>"list",'uses' => "PizzaController@index"]);
					$this->get('create',['as'=>"create",'uses' => "PizzaController@create"]);
					$this->get('archives',['as'=>"view",'uses' => "PizzaController@archives"]);
				});
				$this->group(['prefix' => "entrees_sweets",'as' => ""],function(){
					$this->get('list',['as'=>"list",'uses' => "EntreesSweetsController@index"]);
					$this->get('create',['as'=>"create",'uses' => "EntreesSweetsController@create"]);
					$this->get('archives',['as'=>"view",'uses' => "EntreesSweetsController@archives"]);
				});
				$this->group(['prefix' => "image",'as' => ""],function(){
					$this->get('list',['as'=>"list",'uses' => "PlankImageSliderController@index"]);
					$this->get('create',['as'=>"create",'uses' => "PlankImageSliderController@create"]);
					$this->get('view/{id}',['as'=>"view",'uses' => "PlankImageSliderController@view"]);
					$this->get('archives',['as'=>"view",'uses' => "PlankImageSliderController@archives"]);
				});
				$this->group(['prefix' => "header",'as' => ""],function(){
					$this->get('list',['as'=>"list",'uses' => "PlankHeaderController@index"]);
					$this->get('create',['as'=>"create",'uses' => "PlankHeaderController@create"]);
					$this->get('view/{id}',['as'=>"view",'uses' => "PlankHeaderController@view"]);
					$this->get('archives',['as'=>"view",'uses' => "PlankHeaderController@archives"]);
				});
				$this->group(['prefix' => "about",'as' => ""],function(){
				$this->get('list',['as'=>"list",'uses' => "PlankAboutController@index"]);
				$this->get('create',['as'=>"create",'uses' => "PlankAboutController@create"]);
				$this->get('archives',['as'=>"view",'uses' => "PlankAboutController@archives"]);
			});
			});
			$this->group(['prefix' => "audit",'as' => ""],function(){
				$this->get('list',['as'=>"list",'uses' => "AuditController@index"]);
			});
		});
	});
});