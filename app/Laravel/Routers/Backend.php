<?php

$this->group(['namespace'=> "Backend",'middleware' => "web",'as' => "backend."],function(){
	$this->group(['prefix' => "",'as' => ""],function(){
		$this->post('inquiry/sent',['as'=>"inquiry",'uses' => "ContactInquiryController@contact"]);
		$this->post('donate',['as'=>"inquiry",'uses' => "DonateController@donate"]);
		$this->group(['prefix' => "administrator",'as' => ""],function(){
			$this->post('login',['as'=>"login",'uses' => "LoginController@login"]);
			$this->any('logout',['as'=>"logout",'uses' => "LoginController@destroy"]);
			$this->group(['prefix' => "user",'as' => ""],function(){
				$this->post('create',['as'=>"create",'uses' => "UserController@create"]);
				$this->any('delete/{id}',['as'=>"delete",'uses' => "UserController@delete"]);
				$this->any('restore/{id}',['as'=>"restore",'uses' => "UserController@restore"]);
				$this->post('update/{id}',['as'=>"update",'uses' => "UserController@update"]);
				$this->post('change/pass/{id}',['as'=>"pass",'uses' => "UserController@change_password"]);
			});
			$this->group(['prefix' => "header",'as' => ""],function(){
				$this->post('create',['as'=>"create",'uses' => "HeaderController@create"]);
				$this->any('delete/{id}',['as'=>"delete",'uses' => "HeaderController@delete"]);
				$this->any('restore/{id}',['as'=>"restore",'uses' => "HeaderController@restore"]);
				$this->post('update/{id}',['as'=>"update",'uses' => "HeaderController@update"]);
				$this->post('change/pass/{id}',['as'=>"pass",'uses' => "HeaderController@change_password"]);
			});
			$this->group(['prefix' => "about",'as' => ""],function(){
				$this->post('add',['as'=>"add",'uses' => "AboutController@create"]);
				$this->any('delete/{id}',['as'=>"create",'uses' => "AboutController@delete"]);
				$this->any('restore/{id}',['as'=>"restore",'uses' => "AboutController@restore"]);
				$this->post('update/{id}',['as'=>"update",'uses' => "AboutController@update"]);
			});
			$this->group(['prefix' => "bread_pastries",'as' => ""],function(){
				$this->post('add',['as'=>"add",'uses' => "BreadPastriesController@create"]);
				$this->any('delete/{id}',['as'=>"create",'uses' => "BreadPastriesController@delete"]);
				$this->any('restore/{id}',['as'=>"restore",'uses' => "BreadPastriesController@restore"]);
				$this->post('update/{id}',['as'=>"update",'uses' => "BreadPastriesController@update"]);
			});
			$this->group(['prefix' => "cakes_slices",'as' => ""],function(){
				$this->post('add',['as'=>"add",'uses' => "CakeSlicesController@create"]);
				$this->any('delete/{id}',['as'=>"create",'uses' => "CakeSlicesController@delete"]);
				$this->any('restore/{id}',['as'=>"restore",'uses' => "CakeSlicesController@restore"]);
				$this->post('update/{id}',['as'=>"update",'uses' => "CakeSlicesController@update"]);
			});
			$this->group(['prefix' => "tarts_pies",'as' => ""],function(){
				$this->post('add',['as'=>"add",'uses' => "TartsPiesController@create"]);
				$this->any('delete/{id}',['as'=>"create",'uses' => "TartsPiesController@delete"]);
				$this->any('restore/{id}',['as'=>"restore",'uses' => "TartsPiesController@restore"]);
				$this->post('update/{id}',['as'=>"update",'uses' => "TartsPiesController@update"]);
			});
			$this->group(['prefix' => "breakfast",'as' => ""],function(){
				$this->post('add',['as'=>"add",'uses' => "BreakfastController@create"]);
				$this->any('delete/{id}',['as'=>"create",'uses' => "BreakfastController@delete"]);
				$this->any('restore/{id}',['as'=>"restore",'uses' => "BreakfastController@restore"]);
				$this->post('update/{id}',['as'=>"update",'uses' => "BreakfastController@update"]);
			});
			$this->group(['prefix' => "items",'as' => ""],function(){
				$this->post('add',['as'=>"add",'uses' => "ItemsController@create"]);
				$this->any('delete/{id}',['as'=>"create",'uses' => "ItemsController@delete"]);
				$this->any('restore/{id}',['as'=>"restore",'uses' => "ItemsController@restore"]);
				$this->post('update/{id}',['as'=>"update",'uses' => "ItemsController@update"]);
			});
			$this->group(['prefix' => "inquiry",'as' => ""],function(){
				$this->any('delete/{id}',['as'=>"create",'uses' => "ContactInquiryController@delete"]);
				$this->any('restore/{id}',['as'=>"restore",'uses' => "ContactInquiryController@restore"]);
			});
			$this->group(['prefix' => "image",'as' => ""],function(){
				$this->post('create',['as'=>"add",'uses' => "ImageSliderController@create"]);
				$this->any('delete/{id}',['as'=>"create",'uses' => "ImageSliderController@delete"]);
				$this->any('restore/{id}',['as'=>"restore",'uses' => "ImageSliderController@restore"]);
				$this->post('update/{id}',['as'=>"update",'uses' => "ImageSliderController@update"]);
			});
			$this->group(['prefix' => "plank",'as' => ""],function(){
				$this->group(['prefix' => "to_start",'as' => ""],function(){
					$this->post('add',['as'=>"add",'uses' => "ToStartController@create"]);
					$this->any('delete/{id}',['as'=>"delete",'uses' => "ToStartController@delete"]);
					$this->any('restore/{id}',['as'=>"restore",'uses' => "ToStartController@restore"]);
					$this->post('update/{id}',['as'=>"update",'uses' => "ToStartController@update"]);
				});
				$this->group(['prefix' => "pizza",'as' => ""],function(){
					$this->post('add',['as'=>"add",'uses' => "PizzaController@create"]);
					$this->any('delete/{id}',['as'=>"delete",'uses' => "PizzaController@delete"]);
					$this->any('restore/{id}',['as'=>"restore",'uses' => "PizzaController@restore"]);
					$this->post('update/{id}',['as'=>"update",'uses' => "PizzaController@update"]);
				});
				$this->group(['prefix' => "entrees_sweets",'as' => ""],function(){
					$this->post('add',['as'=>"add",'uses' => "EntreesSweetsController@create"]);
					$this->any('delete/{id}',['as'=>"delete",'uses' => "EntreesSweetsController@delete"]);
					$this->any('restore/{id}',['as'=>"restore",'uses' => "EntreesSweetsController@restore"]);
					$this->post('update/{id}',['as'=>"update",'uses' => "EntreesSweetsController@update"]);
				});
				$this->group(['prefix' => "image",'as' => ""],function(){
					$this->post('create',['as'=>"add",'uses' => "PlankImageSliderController@create"]);
					$this->any('delete/{id}',['as'=>"create",'uses' => "PlankImageSliderController@delete"]);
					$this->any('restore/{id}',['as'=>"restore",'uses' => "PlankImageSliderController@restore"]);
					$this->post('update/{id}',['as'=>"update",'uses' => "PlankImageSliderController@update"]);
				});
				$this->group(['prefix' => "header",'as' => ""],function(){
					$this->post('create',['as'=>"create",'uses' => "PlankHeaderController@create"]);
					$this->any('delete/{id}',['as'=>"delete",'uses' => "PlankHeaderController@delete"]);
					$this->any('restore/{id}',['as'=>"restore",'uses' => "PlankHeaderController@restore"]);
					$this->post('update/{id}',['as'=>"update",'uses' => "PlankHeaderController@update"]);
					$this->post('change/pass/{id}',['as'=>"pass",'uses' => "PlankHeaderController@change_password"]);
				});
				$this->group(['prefix' => "about",'as' => ""],function(){
				$this->post('add',['as'=>"add",'uses' => "PlankAboutController@create"]);
				$this->any('delete/{id}',['as'=>"create",'uses' => "PlankAboutController@delete"]);
				$this->any('restore/{id}',['as'=>"restore",'uses' => "PlankAboutController@restore"]);
				$this->post('update/{id}',['as'=>"update",'uses' => "PlankAboutController@update"]);
			});
			});
		});
	});
});