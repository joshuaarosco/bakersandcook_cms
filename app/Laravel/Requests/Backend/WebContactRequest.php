<?php 

namespace App\Laravel\Requests\Backend;

use Session,Auth;
use App\Laravel\Requests\RequestManager;

class WebContactRequest extends RequestManager{

    public function rules(){

        return [
            'tl'=>'required',
            'tp'=>'required',
            'cp'=>'required',
            'wl'=>'required',
            'email'=>'required',
        ];
    }

    public function messages(){
        return [
            'tl.required'=>"Please enter the web Tagline.",
            'tp.required'=>"Please enter Web Telephone number.",
            'cp.required'=>"Please enter Web Cellphone number.",
            'wl.required'=>"Please enter Web links.",
            'email.required'=>"Please enter your web Email.",
        ];
    }
}