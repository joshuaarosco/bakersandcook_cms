<?php 

namespace App\Laravel\Requests\Backend;

use Session,Auth;
use App\Laravel\Requests\RequestManager;

class CreateUserRequest extends RequestManager{

    public function rules(){

        return [
            'name'=>'required',
            'company_name'=>'required',
            'company_address'=>'required',
            'phone'=>'required',
            'email'=>'required|confirmed',
            'password'=>'required|confirmed'
        ];
    }

    public function messages(){
        return [
            'name.required'=>"Please enter your name.",
            'email.required'=>"Please enter your email.",
            'password.required'=>"Please enter your password.",
            'phone.required'=>"Please enter your phone number.",
            'company_name.required'=>"Please enter your company name.",
            'company_address.required'=>"Please enter your company address.",
            'email.confirmed'=>"Please confirm your email.",
            'password.confirmed'=>"Please confirm your password."
        ];
    }
}