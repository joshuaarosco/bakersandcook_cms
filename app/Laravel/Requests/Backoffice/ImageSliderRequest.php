<?php 

namespace App\Laravel\Requests\Backoffice;

use Session,Auth;
use App\Laravel\Requests\RequestManager;

class ImageSliderRequest extends RequestManager{

	public function rules(){

		$id = $this->segment(4)?:0;

		$rules = [
			// 'header' => "required",
			// 'subheader' => "required",
			'display_order' => "integer",
			'file' => "required|image",
		];

		if($id){
			$rules['file'] = "image";
		}

		return $rules;
	}

	public function messages(){
		return [
			'required' => "This item is required.",
		];
	}
}