<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TblBakersandcookHeader extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('tbl_bakersandcook_header', function (Blueprint $table) {
            $table->increments('id');   
            $table->text('sequence_no')->nullable();
            $table->text('header')->nullable();
            $table->text('sub_header')->nullable();
            $table->text('button_title')->nullable();
            $table->text('button_link')->nullable();
            $table->string('image_filename')->nullable();
            $table->string('directory')->nullable();
            $table->string('filename')->nullable();
            $table->string('archive')->nullable();
            $table->datetime('deleted_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
