<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
	<link href="{{ asset('assets/css/icons/icomoon/styles.css')}}" rel="stylesheet" type="text/css">
	<link href="{{ asset('assets/css/bootstrap.css')}}" rel="stylesheet" type="text/css">
	<link href="{{ asset('assets/css/core.css')}}" rel="stylesheet" type="text/css">
	<link href="{{ asset('assets/css/components.css')}}" rel="stylesheet" type="text/css">
	<link href="{{ asset('assets/css/colors.css')}}" rel="stylesheet" type="text/css">
	<!-- /global stylesheets -->

	<!-- Core JS files -->
	<script type="text/javascript" src="{{ asset('assets/js/plugins/loaders/pace.min.js')}}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/core/libraries/jquery.min.js')}}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/core/libraries/bootstrap.min.js')}}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/plugins/loaders/blockui.min.js')}}"></script>
	<!-- /core JS files -->

	<!-- Theme JS files -->
	<script type="text/javascript" src="{{ asset('assets/js/plugins/notifications/pnotify.min.js')}}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/plugins/notifications/noty.min.js')}}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/plugins/notifications/jgrowl.min.js')}}"></script>

	<script type="text/javascript" src="{{ asset('assets/js/core/app.js')}}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/pages/components_notifications_other.js')}}"></script>