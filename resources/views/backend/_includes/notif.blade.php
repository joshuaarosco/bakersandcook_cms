@if(Session::get('notification-status')=="warning")
    <div class="alert bg-warning alert-styled-left">
        <button type="button" class="close" data-dismiss="alert"><span>&times;</span><span class="sr-only">Close</span></button>
        <span class="text-semibold">Warning!</span> 
        {{Session::get('notification-message')}}
    </div>
@endif
@if(Session::get('notification-status')=="failed")
    <div class="alert bg-danger alert-styled-left">
        <button type="button" class="close" data-dismiss="alert"><span>&times;</span><span class="sr-only">Close</span></button>
        <span class="text-semibold">Oh snap!</span> {{Session::get('notification-message')}}
    </div>
@endif
@if(Session::get('notification-status')=="success")
    <div class="alert bg-success alert-styled-left">
        <button type="button" class="close" data-dismiss="alert"><span>&times;</span><span class="sr-only">Close</span></button>
        <span class="text-semibold">Well done!</span> 
        {{Session::get('notification-message')}}
    </div>
@endif