<div class="sidebar-category sidebar-category-visible">
	<div class="category-content no-padding">
		<ul class="navigation navigation-main navigation-accordion">

			<!-- Main -->
			<li class="navigation-header"><span>Main</span> <i class="icon-menu" title="Main pages"></i></li>
			<li><a href="{{url('administrator')}}"><i class="icon-home4"></i> <span>Dashboard</span></a></li>
			<li>
				<a href="#"><i class="icon-user"></i> <span>Employee</span></a>
				<ul>
					<li><a href="{{url('administrator/user/list')}}"><i class="icon-users"></i> Record Data</a></li>
					<li><a href="{{url('administrator/user/create')}}"><i class="icon-user-plus"></i> Create New</a></li>
					<li><a href="{{url('administrator/user/archives')}}"><i class="icon-archive"></i>Archive</a></li>
				</ul>
			</li>
			<li class="navigation-header"><span>Baker & Cook Web Content</span> <i class="icon-menu6" title="Baker & Cook Web Content"></i></li>
			<li>
				<a href="#"><i class="icon-grid6"></i> <span>Header</span></a>
				<ul>
					<li><a href="{{url('administrator/header/list')}}"><i class="icon-grid3"></i> Record Data</a></li>
					<li><a href="{{url('administrator/header/create')}}"><i class="icon-stack-plus"></i> Create New</a></li>
					<li><a href="{{url('administrator/header/archives')}}"><i class="icon-archive"></i>Archive</a></li>
				</ul>
			</li>
			<li>
				<a href="#"><i class="icon-info22"></i> <span>About</span></a>
				<ul>
					<li><a href="{{url('administrator/about/list')}}"><i class="icon-stack-text"></i> Record Data</a></li>
					<li><a href="{{url('administrator/about/create')}}"><i class="icon-stack-plus"></i> Create New</a></li>
					<li><a href="{{url('administrator/about/archives')}}"><i class="icon-archive"></i>Archive</a></li>
				</ul>
			</li>
			<li>
				<a href="#"><i class="icon-images2"></i> <span>Image Slider</span></a>
				<ul>
					<li><a href="{{url('administrator/image/list')}}"><i class="icon-stack-text"></i> Record Data</a></li>
					<li><a href="{{url('administrator/image/create')}}"><i class="icon-stack-plus"></i> Create New</a></li>
					<li><a href="{{url('administrator/image/archives')}}"><i class="icon-archive"></i>Archive</a></li>
				</ul>
			</li>
			<li>
				<a href="#"><i class="icon-tree6"></i> <span>Menu</span></a>
				<ul>
					<li><a href="#"><i class="icon-stack-text"></i> Bread & Pastries</a>
						<ul>
							<li><a href="{{url('administrator/bread_pastries/list')}}"><i class="icon-stack-text"></i> Record Data</a></li>
							<li><a href="{{url('administrator/bread_pastries/create')}}"><i class="icon-stack-plus"></i> Create New</a></li>
							<li><a href="{{url('administrator/bread_pastries/archives')}}"><i class="icon-archive"></i>Archive</a></li>
						</ul>
					</li>
					<li><a href="#"><i class="icon-stack-text"></i> Cake & Slices</a>
						<ul>
							<li><a href="{{url('administrator/cakes_slices/list')}}"><i class="icon-stack-text"></i> Record Data</a></li>
							<li><a href="{{url('administrator/cakes_slices/create')}}"><i class="icon-stack-plus"></i> Create New</a></li>
							<li><a href="{{url('administrator/cakes_slices/archives')}}"><i class="icon-archive"></i>Archive</a></li>
						</ul>
					</li>
					<li><a href="#"><i class="icon-stack-text"></i> Tarts & Pies</a>
						<ul>
							<li><a href="{{url('administrator/tarts_pies/list')}}"><i class="icon-stack-text"></i> Record Data</a></li>
							<li><a href="{{url('administrator/tarts_pies/create')}}"><i class="icon-stack-plus"></i> Create New</a></li>
							<li><a href="{{url('administrator/tarts_pies/archives')}}"><i class="icon-archive"></i>Archive</a></li>
						</ul>
					</li>
					<li><a href="#"><i class="icon-stack-text"></i> Breakfast</a>
						<ul>
							<li><a href="{{url('administrator/breakfast/list')}}"><i class="icon-stack-text"></i> Record Data</a></li>
							<li><a href="{{url('administrator/breakfast/create')}}"><i class="icon-stack-plus"></i> Create New</a></li>
							<li><a href="{{url('administrator/breakfast/archives')}}"><i class="icon-archive"></i>Archive</a></li>
						</ul>
					</li>
					<li><a href="#"><i class="icon-stack-text"></i> Items</a>
						<ul>
							<li><a href="{{url('administrator/items/list')}}"><i class="icon-stack-text"></i> Record Data</a></li>
							<li><a href="{{url('administrator/items/create')}}"><i class="icon-stack-plus"></i> Create New</a></li>
							<li><a href="{{url('administrator/items/archives')}}"><i class="icon-archive"></i>Archive</a></li>
						</ul>
					</li>
				</ul>
			</li>
			<li class="navigation-header"><span>Plank Web Content</span> <i class="icon-menu6" title="Baker & Cook Web Content"></i></li>
			<li>
				<a href="#"><i class="icon-grid6"></i> <span>Header</span></a>
				<ul>
					<li><a href="{{url('administrator/plank/header/list')}}"><i class="icon-grid3"></i> Record Data</a></li>
					<li><a href="{{url('administrator/plank/header/create')}}"><i class="icon-stack-plus"></i> Create New</a></li>
					<li><a href="{{url('administrator/plank/header/archives')}}"><i class="icon-archive"></i>Archive</a></li>
				</ul>
			</li>
			<li>
				<a href="#"><i class="icon-info22"></i> <span>About</span></a>
				<ul>
					<li><a href="{{url('administrator/plank/about/list')}}"><i class="icon-stack-text"></i> Record Data</a></li>
					<li><a href="{{url('administrator/plank/about/create')}}"><i class="icon-stack-plus"></i> Create New</a></li>
					<li><a href="{{url('administrator/plank/about/archives')}}"><i class="icon-archive"></i>Archive</a></li>
				</ul>
			</li>
			<li>
				<a href="#"><i class="icon-images2"></i> <span>Image Slider</span></a>
				<ul>
					<li><a href="{{url('administrator/plank/image/list')}}"><i class="icon-stack-text"></i> Record Data</a></li>
					<li><a href="{{url('administrator/plank/image/create')}}"><i class="icon-stack-plus"></i> Create New</a></li>
					<li><a href="{{url('administrator/plank/image/archives')}}"><i class="icon-archive"></i>Archive</a></li>
				</ul>
			</li>
			<li>
				<a href="#"><i class="icon-tree6"></i> <span>Menu</span></a>
				<ul>
					<li><a href="#"><i class="icon-stack-text"></i> To Start</a>
						<ul>
							<li><a href="{{url('administrator/plank/to_start/list')}}"><i class="icon-stack-text"></i> Record Data</a></li>
							<li><a href="{{url('administrator/plank/to_start/create')}}"><i class="icon-stack-plus"></i> Create New</a></li>
							<li><a href="{{url('administrator/plank/to_start/archives')}}"><i class="icon-archive"></i>Archive</a></li>
						</ul>
					</li>
					<li><a href="#"><i class="icon-stack-text"></i> Pizza</a>
						<ul>
							<li><a href="{{url('administrator/plank/pizza/list')}}"><i class="icon-stack-text"></i> Record Data</a></li>
							<li><a href="{{url('administrator/plank/pizza/create')}}"><i class="icon-stack-plus"></i> Create New</a></li>
							<li><a href="{{url('administrator/plank/pizza/archives')}}"><i class="icon-archive"></i>Archive</a></li>
						</ul>
					</li>
					<li><a href="#"><i class="icon-stack-text"></i> Entrees & Sweets</a>
						<ul>
							<li><a href="{{url('administrator/plank/entrees_sweets/list')}}"><i class="icon-stack-text"></i> Record Data</a></li>
							<li><a href="{{url('administrator/plank/entrees_sweets/create')}}"><i class="icon-stack-plus"></i> Create New</a></li>
							<li><a href="{{url('administrator/plank/entrees_sweets/archives')}}"><i class="icon-archive"></i>Archive</a></li>
						</ul>
					</li>
				</ul>
			</li>
			<li class="navigation-header"><span>Inquiries </span> <i class="icon-paperplane" title="Inquiries & Donations "></i></li>
			<li>
				<a href="#"><i class="icon-touch"></i> <span>Inquiries</span></a>
				<ul>
					<li><a href="{{url('administrator/inquiry/list')}}"><i class="icon-stack-text"></i> Record Data</a></li>
					<li><a href="{{url('administrator/inquiry/archives')}}"><i class="icon-archive"></i>Archive</a></li>
				</ul>
			</li>
			<li class="navigation-header"><span>Audit Trail </span> <i class="icon-footprint" title="Audit Trail "></i></li>
			<li>
				<a href="{{url('administrator/audit/list')}}"><i class="icon-pencil7"></i> <span>Audit Trail</span></a>
			</li>
			
			

			
			<!-- /page kits -->

		</ul>
	</div>
</div>