<div class="sidebar-user">
	<div class="category-content">
		<div class="media">
			<a href="#" class="media-left"><img src="{{asset(Auth::user()->directory.'/'.Auth::user()->filename)}}" class="img-circle img-sm" alt=""></a>
			<div class="media-body">
				<span class="media-heading text-semibold">{{Auth::user()->name}}</span>
				<div class="text-size-mini text-muted">
					<i class="icon-pin text-size-small"></i> &nbsp;{{Auth::user()->address}}
				</div>
			</div>

			<div class="media-right media-middle">
				<ul class="icons-list">
					<li>
						<a href="#"><i class="icon-user"></i></a>
					</li>
				</ul>
			</div>
		</div>
	</div>
</div>