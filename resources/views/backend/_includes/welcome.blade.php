@if(Session::get('notification-status')=="info")
	<div id="jGrowl" class="top-right jGrowl">
		<div class="jGrowl-notification"></div>
		<div class="jGrowl-notification alert ui-state-highlight ui-corner-all alert-info alert-styled-left alert-arrow-left alert-component" style="display: block;">
		<button class="jGrowl-close">×</button>
		<div class="jGrowl-header">It's nice to be back</div>
		<div class="jGrowl-message">Welcome {{Auth::user()->name}} !</div></div>
	</div>
@endif