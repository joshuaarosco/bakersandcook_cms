	<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
	<link href="{{ asset('assets/css/icons/icomoon/styles.css')}}" rel="stylesheet" type="text/css">
	<link href="{{ asset('assets/css/icons/fontawesome/styles.min.css')}}" rel="stylesheet" type="text/css">
	<link href="{{ asset('assets/css/bootstrap.css')}}" rel="stylesheet" type="text/css">
	<link href="{{ asset('assets/css/core.css')}}" rel="stylesheet" type="text/css">
	<link href="{{ asset('assets/css/components.css')}}" rel="stylesheet" type="text/css">
	<link href="{{ asset('assets/css/colors.css')}}" rel="stylesheet" type="text/css">
	<!-- /global stylesheets -->

	<!-- Core JS files -->
	<script type="text/javascript" src="{{ asset('assets/js/plugins/loaders/pace.min.js')}}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/core/libraries/jquery.min.js')}}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/core/libraries/bootstrap.min.js')}}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/plugins/loaders/blockui.min.js')}}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/plugins/notifications/jgrowl.min.js')}}"></script>
	<!-- /core JS files -->

	<!-- Theme JS files -->
	<script type="text/javascript" src="{{ asset('assets/js/plugins/visualization/d3/d3.min.js')}}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/plugins/visualization/d3/d3_tooltip.js')}}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/plugins/forms/styling/switchery.min.js')}}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/plugins/forms/styling/uniform.min.js')}}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/plugins/forms/selects/bootstrap_multiselect.js')}}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/plugins/ui/moment/moment.min.js')}}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/plugins/pickers/daterangepicker.js')}}"></script>
	
	<link href="{{ asset('backoffice/summernote/summernote.css')}}" rel="stylesheet">	
	<script src="{{ asset('backoffice/summernote/summernote.min.js')}}"></script>	
	<script src="{{asset('backoffice/summernote/summernote-cleaner.js')}}"></script>
	<script type="text/javascript">		
		$(function(){			
			$(".summernote").summernote({ 
				height : 300,
				toolbar: [
				['cleaner',['cleaner']],
				['style', ['style']],
				['font', ['bold', 'italic', 'underline', 'clear', 'superscript', 'subscript']],
				['fontname', ['fontname']],
				['color', ['color']],
				['fontsize', ['fontsize']],
				['para', ['ul', 'ol', 'paragraph']],
				['table',['table']],
				['insert',['link','picture','video']],
				['view',['fullscreen','codeview','help']]
				],
				fontNames:["Arial","Arial Black","Comic Sans MS","Courier New","Helvetica Neue","Helvetica","Impact","Lucida Grande","Tahoma","Times New Roman","Verdana","Roboto"],

				onImageUpload: function(files, editor, welEditable) {
					sendFile(files[0], editor, welEditable);
				},cleaner:{
                 notTime: 2400, // Time to display Notifications.
                 action: 'both', // both|button|paste 'button' only cleans via toolbar button, 'paste' only clean when pasting content, both does both options.
                 newline: '<br>', // Summernote's default is to use '<p><br></p>'
                 notStyle: 'position:absolute;top:0;left:0;right:0', // Position of Notification
                 icon: '<i class="note-icon">Reset</i>',
                 keepHtml: false, // Remove all Html formats
                 keepClasses: false, // Remove Classes
                 badTags: ['style', 'script', 'applet', 'embed', 'noframes', 'noscript', 'html'], // Remove full tags with contents
                 badAttributes: ['style', 'start'] // Remove attributes from remaining tags
             }
         });	

			function sendFile(file, editor, welEditable) {
				data = new FormData();
				data.append("file", file);
				data.append("api_token","{{env('APP_KEY')}}");
				$.ajax({
					data: data,
					type: "POST",
					cache: false,
					contentType: false,
					processData: false,
					success: function(data) {
						if(data.status == true){
							$('.summernote').summernote('insertImage', data.image);
						}
					}
				});
			}		
		});	
	</script>

	<script type="text/javascript" src="{{ asset('assets/js/core/app.js')}}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/pages/dashboard.js')}}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/plugins/forms/validation/validate.min.js')}}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/plugins/forms/selects/bootstrap_multiselect.js')}}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/plugins/forms/inputs/touchspin.min.js')}}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/plugins/forms/selects/select2.min.js')}}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/plugins/forms/styling/switch.min.js')}}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/plugins/forms/styling/switchery.min.js')}}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/plugins/forms/styling/uniform.min.js')}}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/pages/form_validation.js')}}"></script>

	<!-- Core JS files -->
	<link rel="apple-touch-icon" sizes="57x57" href="{{asset('frontend/images/logo.png')}}">