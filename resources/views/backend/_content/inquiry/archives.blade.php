@extends('backend._template.main')
@section('style')
	@include('backend._includes.table-data-style')
@stop
@section('content')
<div class="page-header page-header-default">
	<div class="page-header-content">
		<div class="page-title">
			<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Home</span> - Inquiry list</h4>
		</div>
	</div>

	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href="{{url('')}}"><i class="icon-home2 position-left"></i> Home</a></li>
			<li class="active">Inquiry list</li>
		</ul>
	</div>
</div>
<!-- /page header -->


<!-- Content area -->
<div class="content">

	@include('backend._includes.notif')
	<!-- Main charts -->
	<div class="panel panel-flat">
		<div class="panel-heading">
		<h5 class="panel-title">Inquiry list</h5>
			<div class="heading-elements">
				<ul class="icons-list">
					<li><a data-action="collapse"></a></li>
					<!-- <li><a data-action="reload"></a></li>
					<li><a data-action="close"></a></li> -->
				</ul>
			</div>
		</div>

		<div class="panel-body">
		</div>

		<table class="table datatable-button-html5-columns">
			<thead>
				<tr>
					<th><center>Name</center></th>
					<th><center>Inquiry Sent at</center></th>
					<th><center>Email</center></th>
					<th><center>Message</center></th>
					<th><center>Deleted at</center></th>
					<th><center>Actions</center></th>
				</tr>
			</thead>
			<tbody>
				@foreach($inquiries as $index=>$info)
				<tr>
					<td><center>{{Str::limit($info->name,$limit='10',$end='...')}}</center></td>
					<td><center>{{ucfirst($info->type)}}</center></td>
					<td><center>{{Str::limit($info->email,$limit='10',$end='...')}}</center></td>
					<td><center>{{Str::limit($info->message,$limit='30',$end='...')}}</center></td>
					<td><center>{{Helper::date_format($info->deleted_at)}}</center></td>
					<td><center>
						<a data-toggle="modal" data-target="#modal_{{$info->id}}" class="btn border-info text-info btn-flat btn-icon btn-rounded"><i class="icon-eye"></i></a>&nbsp; &nbsp;
						<a href="{{url('administrator/inquiry/restore/'.$info->id)}}" onclick="return confirm('Are you sure you want to restore this inquiry?');" data-toggle="tooltip" data-placement="top" title="Restore" class="btn border-success text-success btn-flat btn-icon btn-rounded"><i class="icon-sync fa-spin"></i></a></center>
					</td>
				</tr>
				<div id="modal_{{$info->id}}" class="modal fade">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header bg-info">
								<button type="button" class="close" data-dismiss="modal">&times;</button>
								<h6 class="modal-title">Inquiry Details</h6>
							</div>

							<div class="modal-body">
								<label>Name: </label><strong class="text-semibold">{{$info->name}}</strong><br/><br/>
								<label>Email: </label><strong class="text-semibold">{{$info->email}}</strong><br/><br/>
								<label>Message: </label><br/>
								<h6 class="text-semibold">{{$info->message}}</h6>
							</div>
							<hr>
							<div class="modal-footer">
								<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
							</div>
						</div>
					</div>
				</div>
				@endforeach
			</tbody>
		</table>
	</div>
	<!-- /main charts -->


	<!-- Dashboard content -->
	
	<!-- /dashboard content -->


	<!-- Footer -->
	@include('backend._includes.footer')
	<!-- /footer -->

</div>
@stop
