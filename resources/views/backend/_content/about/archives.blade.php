@extends('backend._template.main')
@section('style')
	@include('backend._includes.table-data-style')
@stop
@section('content')
<div class="page-header page-header-default">
	<div class="page-header-content">
		<div class="page-title">
			<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Home</span> - About list</h4>
		</div>
	</div>

	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href="{{url('')}}"><i class="icon-home2 position-left"></i> Home</a></li>
			<li class="active">About list</li>
		</ul>
	</div>
</div>
<!-- /page header -->


<!-- Content area -->
<div class="content">

	@include('backend._includes.notif')
	<!-- Main charts -->
	<div class="panel panel-flat">
		<div class="panel-heading">
		<h5 class="panel-title">About list</h5>
			<div class="heading-elements">
				<ul class="icons-list">
					<li><a data-action="collapse"></a></li>
					<!-- <li><a data-action="reload"></a></li>
					<li><a data-action="close"></a></li> -->
				</ul>
			</div>
		</div>

		<div class="panel-body">
		</div>

		<table class="table datatable-button-html5-columns">
			<thead>
				<tr>
					<th>Content</th>
					<th>Deleted at</th>
					<th><center>Actions</center></th>
				</tr>
			</thead>
			<tbody>
				@foreach($abouts as $index=>$info)
				<tr>
					<td>{{Str::limit($info->content,$limit="50",$end="...")}}</td>
					<td>{{Helper::date_format($info->deleted_at)}}</td>
					<td><center>
						<a data-toggle="modal" data-target="#modal_{{$info->id}}" class="btn border-info text-info btn-flat btn-icon btn-rounded"><i class="icon-eye"></i></a></a>&nbsp; &nbsp;
						<a href="{{url('administrator/about/restore/'.$info->id)}}" onclick="return confirm('Are you sure you want to restore this?');" data-toggle="tooltip" data-placement="top" title="Restore" class="btn border-success text-success btn-flat btn-icon btn-rounded"><i class="icon-sync fa-spin"></i></a></center>
					</td>
				</tr>
				<div id="modal_{{$info->id}}" class="modal fade">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header bg-info">
								<button type="button" class="close" data-dismiss="modal">&times;</button>
								<h6 class="modal-title">About Us Content</h6>
							</div>

							<div class="modal-body">
								<legend>Content</legend>
								<div class="form-group">
									<h6 class="text-semibold"><textarea class="form-control" cols="5" rows="10" name="content" placeholder="Content">{{$info->content}}</textarea></h6>
									<input type="hidden" name="sequence_no" value="0">
								</div>
							</div>
							<hr>
							<div class="modal-footer">
								<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
							</div>
						</div>
					</div>
				</div>
				@endforeach
			</tbody>
		</table>
	</div>
	<!-- /main charts -->


	<!-- Dashboard content -->
	
	<!-- /dashboard content -->


	<!-- Footer -->
	@include('backend._includes.footer')
	<!-- /footer -->

</div>
@stop
