@extends('backend._template.main')
@section('style')
	@include('backend._includes.styles')
@stop
@section('content')
<div class="page-header page-header-default">
	<div class="page-header-content">
		<div class="page-title">
			<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Home</span> - Create Header</h4>
		</div>
	</div>

	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href="{{url('')}}"><i class="icon-home2 position-left"></i> Home</a></li>
			<li class="active">Create Header</li>
		</ul>
	</div>
</div>
<!-- /page header -->


<!-- Content area -->
<div class="content">

	@include('backend._includes.notif')
	<!-- Main charts -->
	<div class="panel panel-flat">
		<div class="panel-heading">
			<h5 class="panel-title">Create Header</h5>
			<div class="heading-elements">
				<ul class="icons-list">
					<li><a data-action="collapse"></a></li>
					<!-- <li><a data-action="reload"></a></li>
					<li><a data-action="close"></a></li> -->
				</ul>
			</div>
		</div>

		<div class="panel-body">
			<p class="content-group-lg"></p>
			<form class="form-horizontal form-validate-jquery" action="" method="POST" enctype="multipart/form-data">
				<input type="hidden" name="_token" value="{{csrf_token()}}">
				<fieldset class="content-group">
					<legend class="text-bold">Header Details</legend>

					<!-- Basic text input -->
					<div class="form-group">
						<label class="control-label col-lg-3">Sequence number <span class="text-danger">*</span></label>
						<div class="col-lg-9">
							<select name="sequence_no" class="form-control" cols="5" rows="5" required="required"> 
							@for($x=$count+1;$x>=1;$x--)
								<option value="{{$x}}" >{{$x}}</option>
							@endfor
							</select>
						</div>
					</div>
					<!-- /basic text input -->

					<!-- Basic text input -->
					<div class="form-group">
						<label class="control-label col-lg-3">Header Title <span class="text-danger">(optional)</span></label>
						<div class="col-lg-9">
							<input type="text" name="header" class="form-control" placeholder="Header Title">
						</div>
					</div>
					<!-- /basic text input -->

					<!-- Basic text input -->
					<div class="form-group">
						<label class="control-label col-lg-3">Sub Header Title <span class="text-danger">(optional)</span></label>
						<div class="col-lg-9">
							<input type="text" name="sub_header" class="form-control" placeholder="Sub Header Title">
						</div>
					</div>
					<!-- /basic text input -->

					<div class="form-group">
						<label class="control-label col-lg-3">Title Image <span class="text-danger">(optional)</span></label>
						<div class="col-lg-9">
							<input type="file" name="image_file" class="form-control">
						</div>
					</div>
					<!-- /repeat password -->

					<!-- Basic text input -->
					<div class="form-group">
						<label class="control-label col-lg-3">Button Title <span class="text-danger">(optional)</span></label>
						<div class="col-lg-9">
							<input type="text" name="button_title" class="form-control" placeholder="Button Title">
						</div>
					</div>
					<!-- /basic text input -->

					<!-- Basic text input -->
					<div class="form-group">
						<label class="control-label col-lg-3">Button link <span class="text-danger">(optional)</span></label>
						<div class="col-lg-9">
							<input type="text" name="button_link" class="form-control" placeholder="Button Link">
						</div>
					</div>
					<!-- /basic text input -->

					<div class="form-group">
						<label class="control-label col-lg-3">Header Background Image <span class="text-danger">*</span></label>
						<div class="col-lg-9">
							<input type="file" name="file" class="form-control" placeholder="Repeat Password" required="required">
						</div>
					</div>
					<!-- /repeat password -->

				</fieldset>

				<div class="text-right">
					<button type="reset" class="btn btn-default" id="reset">Reset <i class="icon-reload-alt position-right"></i></button>
					<button type="submit" class="btn btn-primary">Submit <i class="icon-arrow-right14 position-right"></i></button>
				</div>
			</form>
		</div>
	</div>
	<!-- /main charts -->


	<!-- Dashboard content -->
	
	<!-- /dashboard content -->


	<!-- Footer -->
	@include('backend._includes.footer')
	<!-- /footer -->

</div>
@stop