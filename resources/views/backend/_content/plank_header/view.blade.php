@extends('backend._template.main')
@section('style')
	@include('backend._includes.editable-content-style')
@stop
@section('content')
<div class="page-header page-header-default">
	<div class="page-header-content">
		<div class="page-title">
			<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Home</span> - View Header</h4>
		</div>
	</div>

	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href="{{url('')}}"><i class="icon-home2 position-left"></i> Home</a></li>
			<li class="active">View Header</li>
		</ul>
	</div>
</div>
<!-- /page header -->


<!-- Content area -->
<div class="content">

	@include('backend._includes.notif')
	<!-- Main charts -->
	<div class="panel panel-flat">
		<div class="panel-heading">
			<h5 class="panel-title">View Header</h5>
			<div class="heading-elements">
				<ul class="icons-list">
					<li><a data-action="collapse"></a></li>
					<li><a data-action="reload"></a></li>
					<li><a data-action="close"></a></li>
				</ul>
			</div>
		</div>

		@foreach($headers as $index=>$info)
		<div class="panel-body">
			<form class="form-horizontal form-validate-jquery" action="{{url('administrator/plank/header/update/'.$info->id)}}" method="POST" enctype="multipart/form-data">
				<input type="hidden" name="_token" value="{{csrf_token()}}">
				<fieldset class="content-group">
					<legend class="text-bold">Header</legend>

					<div class="row">
						<div class="col-lg-4 col-sm-6">
							
						</div>
						<div class="col-lg-4 col-sm-6">
							<legend>Header Title Image</legend>
							<div class="thumbnail">
								<div class="thumb">
									<img src="{{asset($info->directory.'/'.$info->image_filename)}}" alt="">
									<div class="caption-overflow">
										<span>
											<a href="{{asset($info->directory.'/'.$info->image_filename)}}" data-popup="lightbox" rel="gallery" class="btn border-white text-white btn-flat btn-icon btn-rounded"><i class="icon-plus3"></i></a>
										</span>
									</div>
								</div>
							</div>
						</div>
						<div class="col-lg-4 col-sm-6">
						<legend>Header Background Image</legend>
							<div class="thumbnail">
								<div class="thumb">
									<img src="{{asset($info->directory.'/'.$info->filename)}}" alt="">
									<div class="caption-overflow">
										<span>
											<a href="{{asset($info->directory.'/'.$info->filename)}}" data-popup="lightbox" rel="gallery" class="btn border-white text-white btn-flat btn-icon btn-rounded"><i class="icon-plus3"></i></a>
										</span>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="form-group">
							<div class="col-lg-3">
								<label class="control-label col-lg-12">Sequence number<span class="text-danger">(optional)</span></label>
							</div>
							<div class="col-lg-9">
								<select name="sequence_no" class="form-control" required="required">
									<option value="{{$info->sequence_no}}">{{$info->sequence_no}}</option>
									@for($x=1;$x<=$count+1;$x++)
									@if($x==$info->sequence_no)
									@else
									<option value="{{$x}}">{{$x}}</option>
									@endif
									@endfor
								</select>
							</div>
						</div>
						<div class="form-group">
							<div class="col-lg-3">
								<label class="control-label col-lg-12">Header Title<span class="text-danger">(optional)</span></label>
							</div>
							<div class="col-lg-9">
								<input type="text" cols="5" rows="5" name="header" class="form-control" value="{{$info->header}}" placeholder="Header">
							</div>
						</div>
						<div class="form-group">
							<div class="col-lg-3">
								<label class="control-label col-lg-12">Sub Header Title<span class="text-danger">(optional)</span></label>
							</div>
							<div class="col-lg-9">
								<input type="text" cols="5" rows="5" name="sub_header" class="form-control" value="{{$info->sub_header}}" placeholder="Sub Header">
							</div>
						</div>
						<div class="form-group">
							<div class="col-lg-3">
								<label class="control-label col-lg-12">Change Header Title Image<span class="text-danger">(optional)</span></label>
							</div>
							<div class="col-lg-9">
								<input type="file" name="image_file" class="form-control" value="{{$info->filename}}">
							</div>
						</div>
						<div class="form-group">
							<div class="col-lg-3">
								<label class="control-label col-lg-12">Button Title<span class="text-danger">(optional)</span></label>
							</div>
							<div class="col-lg-9">
								<input type="text" cols="5" rows="5" name="button_title" class="form-control" value="{{$info->button_title}}" placeholder="Button Title">
							</div>
						</div>
						<div class="form-group">
							<div class="col-lg-3">
								<label class="control-label col-lg-12">Button Link<span class="text-danger">(optional)</span></label>
							</div>
							<div class="col-lg-9">
								<input type="text" cols="5" rows="5" name="button_link" class="form-control" value="{{$info->button_link}}" placeholder="Button Link" >
							</div>
						</div>
						<div class="form-group">
							<div class="col-lg-3">
								<label class="control-label col-lg-12">Change Header Background Image<span class="text-danger">(optional)</span></label>
							</div>
							<div class="col-lg-9">
								<input type="file" name="file" class="form-control" value="{{$info->filename}}">
							</div>
						</div>
					</div>

				</fieldset>

				<div class="text-right">
					<button type="reset" class="btn btn-default" id="reset">Reset <i class="icon-reload-alt position-right"></i></button>
					<button type="submit" class="btn btn-primary">Update <i class="icon-arrow-right14 position-right"></i></button>
				</div>
			</form>
		</div>
		@endforeach
	</div>
	<!-- /main charts -->


	<!-- Dashboard content -->
	
	<!-- /dashboard content -->


	<!-- Footer -->
	@include('backend._includes.footer')
	<!-- /footer -->

</div>
@stop