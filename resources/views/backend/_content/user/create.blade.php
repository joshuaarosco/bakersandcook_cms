@extends('backend._template.main')
@section('style')
	@include('backend._includes.styles')
@stop
@section('content')
<div class="page-header page-header-default">
	<div class="page-header-content">
		<div class="page-title">
			<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Home</span> - Create User</h4>
		</div>
	</div>

	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href="{{url('')}}"><i class="icon-home2 position-left"></i> Home</a></li>
			<li class="active">Create User</li>
		</ul>
	</div>
</div>
<!-- /page header -->


<!-- Content area -->
<div class="content">

	@include('backend._includes.notif')
	<!-- Main charts -->
	<div class="panel panel-flat">
		<div class="panel-heading">
			<h5 class="panel-title">Create User</h5>
			<div class="heading-elements">
				<ul class="icons-list">
					<li><a data-action="collapse"></a></li>
					<!-- <li><a data-action="reload"></a></li>
					<li><a data-action="close"></a></li> -->
				</ul>
			</div>
		</div>

		<div class="panel-body">
			<p class="content-group-lg"></p>
			<form class="form-horizontal form-validate-jquery" action="" method="POST" enctype="multipart/form-data">
				<input type="hidden" name="_token" value="{{csrf_token()}}">
				<fieldset class="content-group">
					<legend class="text-bold">User Information</legend>

					<!-- Basic text input -->
					<div class="form-group">
						<label class="control-label col-lg-3">Name <span class="text-danger">*</span></label>
						<div class="col-lg-9">
							<input type="text" name="name" class="form-control" required="required" placeholder="Name">
						</div>
					</div>
					<!-- /basic text input -->

					<!-- Basic text input -->
					<div class="form-group">
						<label class="control-label col-lg-3">Phone <span class="text-danger">*</span></label>
						<div class="col-lg-9">
							<input type="text" name="phone" class="form-control" required="required" placeholder="Phone">
						</div>
					</div>
					<!-- /basic text input -->

					<!-- Basic text input -->
					<div class="form-group">
						<label class="control-label col-lg-3">Address <span class="text-danger">*</span></label>
						<div class="col-lg-9">
							<input type="text" name="address" class="form-control" required="required" placeholder="Address">
						</div>
					</div>
					<!-- /basic text input -->

					<!-- Email field -->
					<div class="form-group">
						<label class="control-label col-lg-3">Email <span class="text-danger">*</span></label>
						<div class="col-lg-9">
							<input type="email" name="email" class="form-control" id="email" required="required" placeholder="Enter email address">
						</div>
					</div>
					<!-- /email field -->

					<!-- Password field -->
					<div class="form-group">
						<label class="control-label col-lg-3">Password <span class="text-danger">*</span></label>
						<div class="col-lg-9">
							<input type="password" name="password" id="password" class="form-control" required="required" placeholder="Password">
						</div>
					</div>
					<!-- /password field -->


					<!-- Repeat password -->
					<div class="form-group">
						<label class="control-label col-lg-3">Repeat password <span class="text-danger">*</span></label>
						<div class="col-lg-9">
							<input type="password" name="repeat_password" class="form-control" required="required" placeholder="Repeat Password">
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-lg-3">Type <span class="text-danger"></span></label>
						<div class="col-lg-9">
							<select class="form-control" name="type">
								<option value="admin">Admin</option>
								<option value="super admin">Super Admin</option>
							</select>
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-lg-3">Profile Pic <span class="text-danger">(optional)</span></label>
						<div class="col-lg-9">
							<input type="file" name="file" class="form-control" placeholder="Repeat Password">
						</div>
					</div>
					<!-- /repeat password -->

				</fieldset>

				<div class="text-right">
					<button type="reset" class="btn btn-default" id="reset">Reset <i class="icon-reload-alt position-right"></i></button>
					<button type="submit" class="btn btn-primary">Submit <i class="icon-arrow-right14 position-right"></i></button>
				</div>
			</form>
		</div>
	</div>
	<!-- /main charts -->


	<!-- Dashboard content -->
	
	<!-- /dashboard content -->


	<!-- Footer -->
	@include('backend._includes.footer')
	<!-- /footer -->

</div>
@stop