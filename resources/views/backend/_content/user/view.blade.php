@extends('backend._template.main')
@section('style')
	@include('backend._includes.styles')
@stop
@section('content')
<div class="content-wrapper">

	<!-- Page header -->
	<div class="page-header">

		<!-- Header content -->
		<div class="page-header-content">
			<div class="page-title">
				<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">View user</span> - Profile</h4>

				<ul class="breadcrumb position-right">
					<li><a href="{{url('')}}">Home</a></li>
					<li class="active">Profile</li>
				</ul>
			</div>
		</div>
		<!-- /header content -->


		<!-- Toolbar -->
		<div class="navbar navbar-default navbar-component navbar-xs">
			<ul class="nav navbar-nav visible-xs-block">
				<li class="full-width text-center"><a data-toggle="collapse" data-target="#navbar-filter"><i class="icon-menu7"></i></a></li>
			</ul>

			<div class="navbar-collapse collapse" id="navbar-filter">
				<ul class="nav navbar-nav">
					<li class="active"><a href="#activity" data-toggle="tab"><i class="icon-menu7 position-left"></i> Activity</a></li>
					@if($id==Auth::user()->id||Auth::user()->type=="super admin")
					<li><a href="#settings" data-toggle="tab"><i class="icon-cog3 position-left"></i> Settings</a></li>
					@endif
				</ul>
			</div>
		</div>
		<!-- /toolbar -->

	</div>
	<!-- /page header -->


	<!-- Content area -->
	<div class="content">

		@include('backend._includes.notif')
		<!-- Main charts -->
		<!-- User profile -->
		<div class="row">
			<div class="col-lg-9">
				<div class="tabbable">
					<div class="tab-content">
						<div class="tab-pane fade in active" id="activity">
							<div class="timeline timeline-left content-group">
								<div class="timeline-container">

						@foreach($audit as $index=>$info)

									<!-- Sales stats -->
									<div class="timeline-row">
										<div class="timeline-icon">
										@if($info->action_type=="login")
											<div class="bg-primary-600">
											<i class="icon-enter"></i>
											</div>
										@elseif($info->action_type=="logout")
											<div class="bg-warning">
											<i class="icon-exit"></i>
											</div>
										@elseif($info->action_type=="create")
											<div class="bg-success">
											<i class="icon-check"></i>
											</div>
										@elseif($info->action_type=="delete")
											<div class="bg-danger">
											<i class="icon-trash"></i>
											</div>
										@elseif($info->action_type=="update")
											<div class="bg-teal">
											<i class="icon-pencil5"></i>
											</div>
										@elseif($info->action_type=="restore")
											<div class="bg-green">
											<i class="icon-sync"></i>
											</div>
										@else
											<div class="bg-info-400">
											<i class="icon-flag3"></i>
											</div>
										@endif
										</div>

										<div class="panel panel-flat timeline-content">
											<div class="panel-heading">
												<h6 class="panel-title">Audit Trail</h6>
												<div class="heading-elements">
													<span class="heading-text"><i class="icon-history position-left text-success"></i> Action occur {{Helper::date_format($info->updated_at)}}</span>

													<ul class="icons-list">
														<li><a data-action="reload"></a></li>
													</ul>
												</div>
											</div>

											<div class="panel-body">
												<h4>{{$info->action}}</h4>
											</div>
										</div>
									</div>
									<!-- /sales stats -->


									<!-- Blog post -->
									<!-- <div class="timeline-row">
										<div class="timeline-icon">
											<img src="assets/images/demo/users/face12.jpg" alt="">
										</div>

										<div class="panel panel-flat timeline-content">
											<div class="panel-heading">
												<h6 class="panel-title">Himalayan sunset</h6>
												<div class="heading-elements">
													<span class="heading-text"><i class="icon-checkmark-circle position-left text-success"></i> 49 minutes ago</span>
													<ul class="icons-list">
														<li class="dropdown">
															<a href="#" class="dropdown-toggle" data-toggle="dropdown">
																<i class="icon-arrow-down12"></i>
															</a>

															<ul class="dropdown-menu dropdown-menu-right">
																<li><a href="#"><i class="icon-user-lock"></i> Hide user posts</a></li>
																<li><a href="#"><i class="icon-user-block"></i> Block user</a></li>
																<li><a href="#"><i class="icon-user-minus"></i> Unfollow user</a></li>
																<li class="divider"></li>
																<li><a href="#"><i class="icon-embed"></i> Embed post</a></li>
																<li><a href="#"><i class="icon-blocked"></i> Report this post</a></li>
															</ul>
														</li>
													</ul>
												</div>
											</div>

											<div class="panel-body">
												<a href="#" class="display-block content-group">
													<img src="assets/images/demo/cover3.jpg" class="img-responsive content-group" alt="">
												</a>

												<h6 class="content-group">
													<i class="icon-comment-discussion position-left"></i>
													Comment from <a href="#">Jason Ansley</a>:
												</h6>

												<blockquote>
													<p>When suspiciously goodness labrador understood rethought yawned grew piously endearingly inarticulate oh goodness jeez trout distinct hence cobra despite taped laughed the much audacious less inside tiger groaned darn stuffily metaphoric unihibitedly inside cobra.</p>
													<footer>Jason, <cite title="Source Title">10:39 am</cite></footer>
												</blockquote>
											</div>

											<div class="panel-footer panel-footer-transparent">
												<div class="heading-elements">
													<ul class="list-inline list-inline-condensed heading-text">
														<li><a href="#" class="text-default"><i class="icon-eye4 position-left"></i> 438</a></li>
														<li><a href="#" class="text-default"><i class="icon-comment-discussion position-left"></i> 71</a></li>
													</ul>

													<span class="heading-btn pull-right">
														<a href="#" class="btn btn-link">Read post <i class="icon-arrow-right14 position-right"></i></a>
													</span>
												</div>
											</div>
										</div>
									</div> -->
									<!-- /blog post -->
								
						@endforeach
								</div>
							</div>
						</div>
						<div class="tab-pane fade" id="settings">
						@foreach($users as $index=>$info)
							<!-- Profile info -->
							<div class="panel panel-flat">
								<div class="panel-heading">
									<h6 class="panel-title">Profile information</h6>
									<div class="heading-elements">
										<ul class="icons-list">
											<li><a data-action="collapse"></a></li>
											<li><a data-action="reload"></a></li>
											<!-- <li><a data-action="close"></a></li> -->
										</ul>
									</div>
								</div>

								<div class="panel-body">
									<form action="{{url('administrator/user/update/'.$info->id)}}" method="POST" enctype="multipart/form-data">
										<input type="hidden" name="_token" value="{{csrf_token()}}">
										<div class="form-group">
											<div class="row">
												<div class="col-md-12">
													<label>Name</label>
													<input type="text" name="name" value="{{ucfirst($info->name)}}" class="form-control" required>
												</div>
											</div>
										</div>
										<div class="form-group">
											<div class="row">
												<div class="col-md-6">
													<label>Phone</label>
													<input type="text" name="phone" value="{{$info->phone}}" class="form-control" required>
												</div>
												<div class="col-md-6">
													<label>Email</label>
													<input type="email" name="email" value="{{$info->email}}" class="form-control" required>
												</div>
											</div>
										</div>

										<div class="form-group">
											<div class="row">
												<div class="col-md-12">
													<label>Address</label>
													<input type="text" name="address" value="{{$info->address}}" class="form-control" required>
												</div>
											</div>
										</div>

										@if(Auth::user()->type=="super admin")
										<div class="form-group">
											<div class="row">
												<div class="col-md-6">
													<label>Type</label>
													<select class="select" name="type">
														<option value="{{$info->type}}">{{ucfirst($info->type)}}</option> 
														@if($info->type=="admin")
														<option value="super admin">Super admin</option>
														@else
														<option value="admin">Admin</option>
														@endif
													</select>
												</div>
											</div>
										</div>
										@endif

										@if($id==Auth::user()->id)
										<div class="form-group">
											<div class="row">
												<div class="col-md-12">
													<label class="display-block">Update profile image</label>
													<input type="file" name="file" class="file-styled">
													<span class="help-block">Accepted formats: gif, png, jpg. Max file size 2Mb</span>
												</div>
											</div>
										</div>
										@endif

										<div class="text-right">
											<button type="submit" class="btn btn-primary">Save <i class="icon-arrow-right14 position-right"></i></button>
										</div>
									</form>
								</div>
							</div>
							<!-- /profile info -->


							<!-- Account settings -->
							@if($id==Auth::user()->id)
							<div class="panel panel-flat">
								<div class="panel-heading">
									<h6 class="panel-title">Account settings</h6>
									<div class="heading-elements">
										<ul class="icons-list">
											<li><a data-action="collapse"></a></li>
											<li><a data-action="reload"></a></li>
											<!-- <li><a data-action="close"></a></li> -->
										</ul>
									</div>
								</div>

								<div class="panel-body">
									<form action="{{url('administrator/user/change/pass/'.Auth::user()->id)}}" method="POST">
										<input type="hidden" name="_token" value="{{csrf_token()}}">
										<div class="form-group">
											<div class="row">
												<div class="col-md-12">
													<label>Current password</label>
													<input type="password" value="" name="op" placeholder="Password" class="form-control" required>
												</div>
											</div>
										</div>

										<div class="form-group">
											<div class="row">
												<div class="col-md-6">
													<label>New password</label>
													<input type="password" name="password" placeholder="Enter new password" class="form-control" required>
												</div>

												<div class="col-md-6">
													<label>Repeat password</label>
													<input type="password" name="confirmpassword" placeholder="Repeat new password" class="form-control" required>
												</div>
											</div>
										</div>
										<div class="text-right">
											<button type="submit" class="btn btn-primary">Save <i class="icon-arrow-right14 position-right"></i></button>
										</div>
									</form>
								</div>
							</div>
							@endif
							<!-- /account settings -->
						@endforeach
						</div>
					</div>
				</div>
			</div>

			<div class="col-lg-3">
			@foreach($users as $index=>$info)
				<!-- User thumbnail -->
				<div class="thumbnail">
					<div class="thumb thumb-rounded thumb-slide">
						<img src="{{asset($info->directory.'/'.$info->filename)}}" alt="">
						<div class="caption">
							<span>
								<h4>{{ucfirst($info->type)}}</h4>
								<!-- <a href="#" class="btn bg-success-400 btn-icon btn-xs" data-popup="lightbox"><i class="icon-plus2"></i></a>
								<a href="#" class="btn bg-success-400 btn-icon btn-xs"><i class="icon-link"></i></a> -->
							</span>
							
						</div>
					</div>

					<div class="caption text-center">
						<h6 class="text-semibold no-margin">{{$info->name}}<small class="display-block">{{ucfirst($info->type)}}</small></h6>
						<ul class="icons-list mt-15">
							<li><a href="#" data-popup="tooltip" title="{{$info->phone}}"><i class="icon-phone"></i></a></li>
							<li><a href="#" data-popup="tooltip" title="{{$info->email}}"><i class="icon-envelope"></i></a></li>
							<li><a href="#" data-popup="tooltip" title="{{$info->address}}"><i class="icon-pin"></i></a></li>
						</ul>
					</div>
				</div>
				<!-- /user thumbnail -->

				<!-- Share your thoughts -->
				@if($id==Auth::user()->id)
				<!-- <div class="panel panel-flat">
					<div class="panel-heading">
						<h6 class="panel-title">Share your thoughts</h6>
						<div class="heading-elements">
							<ul class="icons-list">
								<li><a data-action="close"></a></li>
							</ul>
						</div>
					</div>

					<div class="panel-body">
						<form action="#">
							<div class="form-group">
								<textarea name="enter-message" class="form-control mb-15" rows="3" cols="1" placeholder="What's on your mind?"></textarea>
							</div>

							<div class="row">
								<div class="col-xs-6">
									<ul class="icons-list icons-list-extended mt-10">
										<li><a href="#" data-popup="tooltip" title="Add photo" data-container="body"><i class="icon-images2"></i></a></li>
										<li><a href="#" data-popup="tooltip" title="Add video" data-container="body"><i class="icon-film2"></i></a></li>
										<li><a href="#" data-popup="tooltip" title="Add event" data-container="body"><i class="icon-calendar2"></i></a></li>
									</ul>
								</div>

								<div class="col-xs-6 text-right">
									<button type="button" class="btn btn-primary btn-labeled btn-labeled-right">Share <b><i class="icon-circle-right2"></i></b></button>
								</div>
							</div>
						</form>
					</div>
				</div> -->
				@endif
				<!-- /share your thoughts -->
			@endforeach
				<!-- Connections -->
				<div class="panel panel-flat">
					<div class="panel-heading">
						<h6 class="panel-title">Employees</h6>
						<div class="heading-elements">
							<span class="badge badge-success heading-text">+{{number_format($count)}}</span>
						</div>
					</div>

					<ul class="media-list media-list-linked pb-5">
					@foreach($type as $index=>$info)
						@if($info->type == "super admin")
						<li class="media-header">Super Admin</li>

						<li class="media">
							<a href="#" class="media-link">
								<div class="media-left"><img src="{{asset($info->directory.'/'.$info->filename)}}" class="img-circle" alt=""></div>
								<div class="media-body">
									<span class="media-heading text-semibold">{{$info->name}}</span>
									<span class="media-annotation">{{$info->email}}</span>
								</div>
								<!-- <div class="media-right media-middle">
									<span class="status-mark bg-success"></span>
								</div> -->
							</a>
						</li>
						@else
						<li class="media-header">Admin</li>

						<li class="media">
							<a href="#" class="media-link">
								<div class="media-left"><img src="{{asset($info->directory.'/'.$info->filename)}}" class="img-circle" alt=""></div>
								<div class="media-body">
									<span class="media-heading text-semibold">{{$info->name}}</span>
									<span class="media-annotation">{{$info->email}}</span>
								</div>
								<!-- <div class="media-right media-middle">
									<span class="status-mark bg-success"></span>
								</div> -->
							</a>
						</li>
						@endif
					@endforeach
					</ul>
				</div>
				<!-- /connections -->
			</div>
		</div>


		<!-- Footer -->
		@include('backend._includes.footer')
		<!-- /footer -->

	</div>
</div>
@stop