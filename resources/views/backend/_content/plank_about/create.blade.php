@extends('backend._template.main')
@section('style')
	@include('backend._includes.styles')
@stop
@section('content')
<div class="page-header page-header-default">
	<div class="page-header-content">
		<div class="page-title">
			<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Home</span> - About</h4>
		</div>
	</div>

	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href="{{url('')}}"><i class="icon-home2 position-left"></i> Home</a></li>
			<li class="active">About</li>
		</ul>
	</div>
</div>
<!-- /page header -->


<!-- Content area -->
<div class="content">

	@include('backend._includes.notif')
	<!-- Main charts -->
	<div class="panel panel-flat">
		<div class="panel-heading">
		<h5 class="panel-title">About</h5>
			<div class="heading-elements">
				<ul class="icons-list">
					<li><a data-action="collapse"></a></li>
					<li><a data-action="reload"></a></li>
					<li><a data-action="close"></a></li>
				</ul>
			</div>
		</div>

		<div class="panel-body">
			<p class="content-group-lg"></p>
			<form class="form-horizontal form-validate-jquery" action="add" method="POST" enctype="multipart/form-data">
				<input type="hidden" name="_token" value="{{csrf_token()}}">
				<fieldset class="content-group">
					@if($count==0)
					<legend class="text-bold">Please Enter the Discover Plank Sourdough Pizza Content</legend>
					<div class="form-group">
						<label class="control-label col-lg-2">Content<span class="text-danger">*</span></label>
						<div class="col-lg-10">
							<textarea cols="5" rows="5" name="content" class="form-control" required="required" placeholder="Content"></textarea>
						</div>
					</div>
					<input type="hidden" value="discover" name="type">
					@elseif($count_1==0)
					<legend class="text-bold">Please Enter the Our Food Content</legend>
					<div class="form-group">
						<label class="control-label col-lg-2">Content<span class="text-danger">*</span></label>
						<div class="col-lg-10">
							<textarea cols="5" rows="5" name="content" class="form-control" required="required" placeholder="Content"></textarea>
						</div>
					</div>
					<input type="hidden" value="our food" name="type">
					@else
					<legend class="text-bold">About us content is successfully set up</legend>
					@endif

				</fieldset>
				@if($count==0||$count_1==0)
				<div class="text-right">
					<button type="reset" class="btn btn-default" id="reset">Reset <i class="icon-reload-alt position-right"></i></button>
					<button type="submit" class="btn btn-primary">Create <i class="icon-arrow-right14 position-right"></i></button>
				</div>
				@endif
			</form>
		</div>
	</div>
	<!-- /main charts -->


	<!-- Dashboard content -->
	
	<!-- /dashboard content -->


	<!-- Footer -->
	@include('backend._includes.footer')
	<!-- /footer -->

</div>
@stop