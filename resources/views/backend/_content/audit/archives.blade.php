@extends('backend._template.main')
@section('style')
	@include('backend._includes.table-data-style')
@stop
@section('content')
<div class="page-header page-header-default">
	<div class="page-header-content">
		<div class="page-title">
			<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Home</span> - Audit list</h4>
		</div>
	</div>

	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href="{{url('')}}"><i class="icon-home2 position-left"></i> Home</a></li>
			<li class="active">Audit list</li>
		</ul>
	</div>
</div>
<!-- /page header -->


<!-- Content area -->
<div class="content">

	@include('backend._includes.notif')
	<!-- Main charts -->
	<div class="panel panel-flat">
		<div class="panel-heading">
		<h5 class="panel-title">Audit list</h5>
			<div class="heading-elements">
				<ul class="icons-list">
					<li><a data-action="collapse"></a></li>
					<!-- <li><a data-action="reload"></a></li>
					<li><a data-action="close"></a></li> -->
				</ul>
			</div>
		</div>

		<div class="panel-body">
		</div>

		<table class="table datatable-button-html5-columns">
			<thead>
				<tr>
					<th><center>User ID</center></th>
					<th><center>Name</center></th>
					<th><center>Type</center></th>
					<th><center>Action Type</center></th>
					<th><center>Action Taken</center></th>
					<th><center>Action Occur</center></th>
					<th><center>Actions</center></th>
				</tr>
			</thead>
			<tbody>
				@foreach($audits as $index=>$info)
				<tr>	
					<td><center>{{$info->user_id}}</center></td>
					<td><center>{{substr($info->user_name,0,20)}}</center></td>
					<td><center>{{ucfirst($info->type)}}</center></td>
					<td><center>{{ucfirst($info->action_type)}}</center></td>
					<td><center>{{substr($info->action,0,20)}}</center></td>
					<td><center>{{Helper::date_format($info->created_at)}}</center></td>
					<td><center>
						<a data-toggle="modal" data-target="#modal_{{$info->id}}" class="btn border-info text-info btn-flat btn-icon btn-rounded"><i class="icon-eye"></i></a>&nbsp; &nbsp;
						<a href="{{url('administrator/audit/restore/'.$info->id)}}" onclick="return confirm('Are you sure you want to restore this audit ?');" data-toggle="tooltip" data-placement="top" title="Restore" class="btn border-success text-success btn-flat btn-icon btn-rounded"><i class="icon-sync fa-spin"></i></a></center>
					</td>
				</tr>
				<div id="modal_{{$info->id}}" class="modal fade">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header bg-info">
								<button type="button" class="close" data-dismiss="modal">&times;</button>
								<h6 class="modal-title">Action</h6>
							</div>

							<div class="modal-body">
								<label>Name: </label><strong class="text-semibold">{{$info->user_name}}</strong><br/><br/>
								<label>Type: </label><strong class="text-semibold">{{$info->type}}</strong><br/><br/>
								<label>Action Type: </label><strong class="text-semibold">{{$info->action_type}}</strong><br/><br/>
								<label>Action: </label><br/>
								<h6 class="text-semibold">{{$info->action}}</h6>
							</div>
							<hr>
							<div class="modal-footer">
								<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
							</div>
						</div>
					</div>
				</div>
				@endforeach
			</tbody>
		</table>
	</div>
	<!-- /main charts -->


	<!-- Dashboard content -->
	
	<!-- /dashboard content -->


	<!-- Footer -->
	@include('backend._includes.footer')
	<!-- /footer -->

</div>
@stop
