@extends('backend._template.main')
@section('style')
	@include('backend._includes.table-data-style')
@stop
@section('content')
<div class="page-header page-header-default">
	<div class="page-header-content">
		<div class="page-title">
			<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Home</span> - Entrees & Sweets list</h4>
		</div>
	</div>

	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href="{{url('')}}"><i class="icon-home2 position-left"></i> Home</a></li>
			<li class="active">Entrees & Sweets list</li>
		</ul>
	</div>
</div>
<!-- /page header -->


<!-- Content area -->
<div class="content">

	@include('backend._includes.notif')
	<!-- Main charts -->
	<div class="panel panel-flat">
		<div class="panel-heading">
		<h5 class="panel-title">Entrees & Sweets list</h5>
			<div class="heading-elements">
				<ul class="icons-list">
					<li><a data-action="collapse"></a></li>
					<!-- <li><a data-action="reload"></a></li>
					<li><a data-action="close"></a></li> -->
				</ul>
			</div>
		</div>

		<div class="panel-body">
		</div>

		<table class="table datatable-button-html5-columns">
			<thead>
				<tr>
					<th>Entrees & Sweets Name</th>
					<th>Type</th>
					<th>Created at</th>
					<th>Updated at</th>
					<th>Actions</th>
				</tr>
			</thead>
			<tbody>
				@foreach($entrees_sweets as $index=>$info)
				<tr>
					<td>{{Str::limit(strip_tags($info->food_name),$limit=30)}}</td>
					<td>{{ucfirst($info->type)}}</td>
					<td>{{Helper::date_format($info->created_at)}}</td>
					<td>{{Helper::date_format($info->updated_at)}}</td>
					<td><center>
						<a data-toggle="modal" title="View and edit" data-target="#modal_{{$info->id}}" class="btn border-info text-info btn-flat btn-icon btn-rounded"><i class="icon-eye"></i></a></a>&nbsp; &nbsp;
						<a href="{{url('administrator/plank/entrees_sweets/delete/'.$info->id)}}" onclick="return confirm('Are you sure you want to delete this?');" data-toggle="tooltip" data-placement="top" title="Delete" data-popup="lightbox" rel="gallery" class="btn border-danger text-danger btn-flat btn-icon btn-rounded"><i class="icon-trash"></i></a></center>
					</td>
				</tr>
				<div id="modal_{{$info->id}}" class="modal fade">
					<div class="modal-dialog">
						<form class="form-horizontal form-validate-jquery" action="update/{{$info->id}}" method="POST" enctype="multipart/form-data">
						<input type="hidden" name="_token" value="{{csrf_token()}}">
						<div class="modal-content">
							<div class="modal-header bg-info">
								<button type="button" class="close" data-dismiss="modal">&times;</button>
								<h6 class="modal-title">Entrees & Sweets</h6>
							</div>

							<div class="modal-body">
								<div class="form-group">
									<label><strong>Entrees & Sweets Name</strong></label>
									<textarea type="text" name="food_name" value="" placeholder="Bread & Pastry Name" required="required" class="form-control">{!!$info->food_name!!}</textarea>
								</div>
								<div class="form-group">
									<label><strong>Type</strong></label>
									<select name="type" value="{{$info->type}}"  required="required" class="form-control">
										<option value="{{$info->type}}">{{ucfirst($info->type)}}</option>
										@if($info->type=="entrees")
										<option value="sweets">Sweets</option>
										@elseif($info->type=="sweets")
										<option value="entrees">Entrees</option>
										@endif
									</select>
								</div>
							</div>
							<hr>
							<div class="modal-footer">
								<button type="submit" class="btn btn-info">Update</button>
								<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
							</div>
						</div>
						</form>
					</div>
				</div>
				@endforeach
			</tbody>
		</table>
	</div>
	<!-- /main charts -->


	<!-- Dashboard content -->
	
	<!-- /dashboard content -->


	<!-- Footer -->
	@include('backend._includes.footer')
	<!-- /footer -->

</div>
@stop
