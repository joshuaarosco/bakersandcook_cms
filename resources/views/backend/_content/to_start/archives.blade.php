@extends('backend._template.main')
@section('style')
	@include('backend._includes.table-data-style')
@stop
@section('content')
<div class="page-header page-header-default">
	<div class="page-header-content">
		<div class="page-title">
			<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Home</span> - To Start list</h4>
		</div>
	</div>

	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href="{{url('')}}"><i class="icon-home2 position-left"></i> Home</a></li>
			<li class="active">To Start list</li>
		</ul>
	</div>
</div>
<!-- /page header -->


<!-- Content area -->
<div class="content">

	@include('backend._includes.notif')
	<!-- Main charts -->
	<div class="panel panel-flat">
		<div class="panel-heading">
		<h5 class="panel-title">To Start list</h5>
			<div class="heading-elements">
				<ul class="icons-list">
					<li><a data-action="collapse"></a></li>
					<!-- <li><a data-action="reload"></a></li>
					<li><a data-action="close"></a></li> -->
				</ul>
			</div>
		</div>

		<div class="panel-body">
		</div>

		<table class="table datatable-button-html5-columns">
			<thead>
				<tr>
					<th>To Start Name</th>
					<th>Type</th>
					<th>Deleted at</th>
					<th><center>Actions</center></th>
				</tr>
			</thead>
			<tbody>
				@foreach($to_start as $index=>$info)
				<tr>
					<td>{{$info->food_name}}</td>
					<td>{{ucfirst($info->type)}}</td>
					<td>{{Helper::date_format($info->deleted_at)}}</td>
					<td><center>
						<a data-toggle="modal" title="View and edit" data-target="#modal_{{$info->id}}" class="btn border-info text-info btn-flat btn-icon btn-rounded"><i class="icon-eye"></i></a></a>&nbsp; &nbsp;
						<a href="{{url('administrator/plank/to_start/restore/'.$info->id)}}" onclick="return confirm('Are you sure you want to restore this?');" data-toggle="tooltip" data-placement="top" title="Restore" class="btn border-success text-success btn-flat btn-icon btn-rounded"><i class="icon-sync fa-spin"></i></a></center>
					</td>
				</tr>
				<div id="modal_{{$info->id}}" class="modal fade">
					<div class="modal-dialog">
						<form class="form-horizontal form-validate-jquery" action="update/{{$info->id}}" method="POST" enctype="multipart/form-data">
						<input type="hidden" name="_token" value="{{csrf_token()}}">
						<div class="modal-content">
							<div class="modal-header bg-info">
								<button type="button" class="close" data-dismiss="modal">&times;</button>
								<h6 class="modal-title">Start</h6>
							</div>

							<div class="modal-body">
								<div class="form-group">
									<label><strong>Food Name</strong></label>
									<input type="text" name="food_name" value="{{$info->food_name}}" placeholder="Food Name" required="required" class="form-control">
								</div>
								<div class="form-group">
									<label><strong>Type</strong></label>
									<select name="type" value="{{$info->type}}"  required="required" class="form-control">
										<option value="{{$info->type}}">{{ucfirst($info->type)}}</option>
										@if($info->type=="salads")
										<option value="start">Start</option>
										@elseif($info->type=="start")
										<option value="salads">Salads</option>
										@endif
									</select>
								</div>
							</div>
							<hr>
							<div class="modal-footer">
								<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
							</div>
						</div>
						</form>
					</div>
				</div>
				@endforeach
			</tbody>
		</table>
	</div>
	<!-- /main charts -->


	<!-- Dashboard content -->
	
	<!-- /dashboard content -->


	<!-- Footer -->
	@include('backend._includes.footer')
	<!-- /footer -->

</div>
@stop
