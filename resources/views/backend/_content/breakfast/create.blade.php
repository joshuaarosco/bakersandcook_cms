@extends('backend._template.main')
@section('style')
	@include('backend._includes.styles')
@stop
@section('content')
<div class="page-header page-header-default">
	<div class="page-header-content">
		<div class="page-title">
			<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Home</span> - Breakfast</h4>
		</div>
	</div>

	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href="{{url('')}}"><i class="icon-home2 position-left"></i> Home</a></li>
			<li class="active">Breakfast</li>
		</ul>
	</div>
</div>
<!-- /page header -->


<!-- Content area -->
<div class="content">

	@include('backend._includes.notif')
	<!-- Main charts -->
	<div class="panel panel-flat">
		<div class="panel-heading">
		<h5 class="panel-title">Breakfast</h5>
			<div class="heading-elements">
				<ul class="icons-list">
					<li><a data-action="collapse"></a></li>
					<li><a data-action="reload"></a></li>
					<li><a data-action="close"></a></li>
				</ul>
			</div>
		</div>

		<div class="panel-body">
			<p class="content-group-lg"></p>
			<form class="form-horizontal form-validate-jquery" action="add" method="POST" enctype="multipart/form-data">
				<input type="hidden" name="_token" value="{{csrf_token()}}">
				<fieldset class="content-group">
					<legend class="text-bold">Breakfast</legend>
					<div class="form-group">
						<label class="control-label col-lg-3">Type <span class="text-danger">*</span></label>
						<div class="col-lg-9">
							<select name="type" class="form-control" required="required">
								<option value="">Type</option>
								<option value="breakfast">Breakfast</option>
								<option value="salads">Salads</option>
							</select>
						</div>
					</div>
					{{-- <div class="form-group">
						<label class="control-label col-lg-3">Breakfast or Salad Name <span class="text-danger">*</span></label>
						<div class="col-lg-9">
							<input type="text" name="food_name" class="form-control" placeholder="Bread or Pastry Name" required="required">
						</div>
					</div> --}}

					<div class="form-group">
						<label class="control-label col-lg-3">Breakfast or Salad List <span class="text-danger">*</span></label>
						<div class="col-lg-9">
							<textarea class="summernote" name="food_name" id="food_name" placeholder="Food list" >{{old('food_name')}}</textarea>
						</div>
					</div> 
					<!-- /basic text input -->

				</fieldset>
				<div class="text-right">
					<button type="reset" class="btn btn-default" id="reset">Reset <i class="icon-reload-alt position-right"></i></button>
					<button type="submit" class="btn btn-primary">Add <i class="icon-arrow-right14 position-right"></i></button>
				</div>

			</form>
		</div>
	</div>
	<!-- /main charts -->


	<!-- Dashboard content -->
	
	<!-- /dashboard content -->


	<!-- Footer -->
	@include('backend._includes.footer')
	<!-- /footer -->

</div>
@stop