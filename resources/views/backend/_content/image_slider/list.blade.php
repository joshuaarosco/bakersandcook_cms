@extends('backend._template.main')
@section('style')
	@include('backend._includes.table-data-style')
@stop
@section('content')
<div class="page-header page-header-default">
	<div class="page-header-content">
		<div class="page-title">
			<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Home</span> - Menu Image List</h4>
		</div>
	</div>

	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href="{{url('')}}"><i class="icon-home2 position-left"></i> Home</a></li>
			<li class="active">Menu Image List</li>
		</ul>
	</div>
</div>
<!-- /page header -->


<!-- Content area -->
<div class="content">

	@include('backend._includes.notif')
	<!-- Main charts -->
	<div class="panel panel-flat">
		<div class="panel-heading">
		<h5 class="panel-title">Menu Image List</h5>
			<div class="heading-elements">
				<ul class="icons-list">
					<li><a data-action="collapse"></a></li>
					<!-- <li><a data-action="reload"></a></li>
					<li><a data-action="close"></a></li> -->
				</ul>
			</div>
		</div>

		<div class="panel-body">
		</div>

		<table class="table datatable-button-html5-columns">
			<thead>
				<tr>
					<th>Type</th>
					<th>Created at</th>
					<th>Updated at</th>
					<th><center>Actions</center></th>
				</tr>
			</thead>
			<tbody>
				@foreach($image_slider as $index=>$info)
				<tr>
					<td>{{ucfirst($info->type)}}</td>
					<td>{{Helper::date_format($info->created_at)}}</td>
					<td>{{Helper::date_format($info->updated_at)}}</td>
					<td><center>
						<a href="{{url('administrator/image/view/'.$info->id)}}" data-toggle="tooltip" data-placement="top" title="View" class="btn border-info text-info btn-flat btn-icon btn-rounded"><i class="icon-eye"></i></a>&nbsp; &nbsp;
						<a href="{{url('administrator/image/delete/'.$info->id)}}" onclick="return confirm('Are you sure you want to delete this?');" data-toggle="tooltip" data-placement="top" title="Delete" class="btn border-danger text-danger btn-flat btn-icon btn-rounded"><i class="icon-trash"></i></a></center>
					</td>
				</tr>
				@endforeach
			</tbody>
		</table>
	</div>
	<!-- /main charts -->


	<!-- Dashboard content -->
	
	<!-- /dashboard content -->


	<!-- Footer -->
	@include('backend._includes.footer')
	<!-- /footer -->

</div>
@stop