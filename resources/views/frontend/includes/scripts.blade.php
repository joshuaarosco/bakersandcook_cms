<script src="{{ asset('js/jquery-2.2.3.min.js')}}"></script>
<!-- fullPage slider -->
<script type="text/javascript" src="{{ asset('js/jquery.fullPage.min.js')}}"></script>
<!-- Bootstrap JavaScript -->
<script src="{{ asset('js/bootstrap.min.js')}}"></script>
<!-- isotop -->
<script type="text/javascript" src="{{ asset('js/isotope.pkgd.min.js')}}"></script>
<!-- swiper -->
<script type="text/javascript" src="{{ asset('js/idangerous.swiper.min.js')}}"></script>
<!-- popup -->
<script src="{{ asset('js/index.js')}}"></script>