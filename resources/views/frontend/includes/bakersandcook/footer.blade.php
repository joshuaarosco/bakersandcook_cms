 <footer>
                    <div class="footer">
                        <div class="footer-wrapper">
                            <div class="footer-main">
                                <div class="container">
                                    <a href="#" class="footer-logo"><img src="{{asset('frontend/bakersandcook/images/logo/b&c-logo.png')}}" alt="" class="img-responsive" /></a>
                                    <div class="footer-main-wrapper">
                                        <div class="text-center padding-bottom-15" style="margin-bottom: 40px;">
                                            <span style="margin: auto 20px;">
                                            <a href="https://www.facebook.com/BakerandCookPH/" target="_blank"><i class="fa fa-facebook fa-lg" style="padding: 10px 12px; color: #000;border: 1px solid #fff; border-radius: 100%; background: #ebdaa6;"></i> </a>
                                            <a href="https://www.instagram.com/bakerandcookph/" target="_blank"><i class="fa fa-instagram fa-lg" style="padding: 10px; color: #000;border: 1px solid #fff; border-radius: 100%; background: #ebdaa6;"></i></a> @BakerAndCookPh</span>
                                        </div>
                                        <div class="row">
                                           <!--  <div class="col-md-4" style="margin-bottom: 40px;">
                                                <div class="plank-info widget">
                                                    <div class="title text-center"><a href="http://bistro.com.ph/" target="_blank" style="margin: 0 auto;"><img class="img-responsive" src="{{asset('frontend/bakersandcook/images/more-image/bff-card-1.png')}}" alt=""></a></div>
                                                    <div class="content-widget text-center">
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-4" style="margin-bottom: 40px;">
                                                <div class="plank-info widget">
                                                    <div class="title text-center"><a href="http://bistro.com.ph/" target="_blank" style="margin: 0 auto;"><img class="img-responsive" src="{{asset('frontend/bakersandcook/images/more-image/bff-card-2.png')}}" alt=""></a></div>
                                                    <div class="content-widget text-center">
                                                    </div>
                                                </div>
                                            </div> -->

                                            <div class="col-md-12 text-center" style="margin-bottom: 40px;">
                                                <div class="plank-info widget">
                                                    <div class="title text-center"><a href="https://www.bistro.com.ph/newsletter/register.php" target="_blank" style="margin: 0 auto;"><img class="" src="{{asset('frontend/bakersandcook/images/more-image/mailing-list.png')}}" alt="" style="width: 12%;"></a></div>
                                                    <div class="content-widget text-center">
                                                       <h4><a href="http://bistro.com.ph/" target="_blank" class="bistro">Learn more about The Bistro Group</a></h4>
                                                    </div>
                                                     <div class="title text-center"><a href="https://www.bistro.com.ph/newsletter/register.php" target="_blank" style="margin: 0 auto;"><img class="" src="{{asset('frontend/bakersandcook/images/more-image/bistro.png')}}" alt="" style="width: 12%;"></a></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="hyperlink">
                                <div class="container">
                                    <div class="name-company pull-left">&copy; Bakers And Cook 2019 | ALL RIGHTS RESERVED</div>
                                    <div class="social-footer pull-right">
                                        <ul class="list-inline list-unstyled">
                                            <li><a href="https://www.facebook.com/BakerandCookPH/" class="link facebook"><i class="fa fa-facebook"></i></a></li>
                                            <li><a href="https://www.instagram.com/bakerandcookph/" class="link pinterest"><i class="fa fa-instagram"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </footer>
                <!-- BUTTON BACK TO TOP-->
                <div id="back-top"><a href="#top" class="link"><i class="fa fa-angle-double-up"></i></a></div>