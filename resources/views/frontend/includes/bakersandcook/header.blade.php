<div class="wrapper-menu-mobile-background"></div>
            <!-- MENU MOBILE-->
            <div class="wrapper-menu-mobile">
                <div class="mb-social-wrapper">
                    <div class="mb-socials">
                        <ul class="list-unstyled list-inline">
                            <li><a href="https://www.facebook.com/BakerandCookPH/" class="link facebook"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="https://www.instagram.com/bakerandcookph/" class="link instagram"><i class="fa fa-instagram"></i></a></li>
                        </ul>
                    </div>
                    <div class="mb-button-close"><i class="icons fa fa-times"></i></div>
                    <div class="clearfix"></div>
                </div>
                <ul class="nav navbar-nav mb-nav">
                    <li><a href="{{url('')}}/#home" class="main-menu mb-button-close" style="float: none !important;"><span class="text">Home</span></a></li>
                    <li><a href="{{url('')}}/#about" class="main-menu mb-button-close" style="float: none !important;"><span class="text">About Us</span></a></li>
                    <li><a href="{{url('')}}/#menu" class="main-menu mb-button-close" style="float: none !important;"><span class="text">Menu</span></a></li>
                    <li><a href="{{url('plank')}}" target="_blank"  class="main-menu"><span class="text">Plank</span></a></li>
                    <li><a href="{{url('')}}#contact" class="main-menu mb-button-close" style="float: none  !important;><span class="text">Contact</span></a></li>
                </ul>
            </div>
            <!-- SEARCH MOBILE-->
            <div class="wrapper-search-mobile">
                <div class="mb-social-wrapper">
                    <div class="mb-button-close"><i class="icons fa fa-times"></i></div>
                    <div class="clearfix"></div>
                </div>
                <div class="mb-search">
                    <form><input type="text" placeholder="Search" onfocus="this.placeholder = ''" class="searchbox" />
                        <div class="line-boder"></div><button type="submit" class="searchbutton fa fa-search"></button></form>
                </div>
            </div>