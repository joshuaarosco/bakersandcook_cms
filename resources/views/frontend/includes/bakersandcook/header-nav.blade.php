                <header>
                    <div class="bg-transparent">
                        <div class="header-main">
                            <div class="container">
                                <div class="header-main-wrapper">
                                    <div class="hamburger-menu-mobile"><i class="icons fa fa-bars"></i></div>
                                    <div class="logo-wrapper">
                                        <span style="position: absolute; top:-10px;" class="hidden-xs"><img src="{{asset('frontend/bakersandcook/images/logo/ribbon.png')}}?v=1.1" alt=""></span>
                                        <a href="{{url('')}}" class="logo"><img src="{{asset('frontend/bakersandcook/images/logo/b&c-logo.png')}}?v=1.1" alt="" /></a>
                                    </div>
                                    <nav class="navigation">
                                        <ul class="nav-links nav navbar-nav">
                                            <li><a href="{{url('')}}#home" class="main-menu"><span class="text">Home</span></a></li>
                                            <li><a href="{{url('')}}#about" class="main-menu"><span class="text">About Us</span></a></li>
                                            <li><a href="{{url('')}}#menu" class="main-menu"><span class="text">Menu</span></a></li>
                                            <!-- <li><a href="{{url('plank')}}" target="_blank" class="main-menu"><span class="text">Plank</span></a></li> -->
                                            <li><a href="{{url('')}}#contact" class="main-menu"><span class="text">Contact</span></a></li>
                                            
                                        </ul>
                                        <div class="button-search">
                                             <div class="logo-wrapper" style="float: right; padding-left: 0px !important">
                                                 <span style="position: absolute; top:-10px;" class="hidden-xs hidden-sm"></span>
                                                 <a href="https://www.foodpanda.ph/restaurant/q3my/bakerandcook" class="logo" target="_blank">
                                                    <img src="{{asset('frontend/bakersandcook/images/food-panda-order-2.png')}}"></a>
                                            </div>
                                        </div>
                                        <div class="nav-search hide">
                                            <form><input type="text" placeholder="Search" class="searchbox" /><button type="submit" class="searchbutton fa fa-search"></button></form>
                                        </div>
                                    </nav>
                                    <div class="button-search-mobile"><i class="icons fa fa-search"></i></div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>

                            <div class="container hidden-md hidden-lg" style="padding: 0px !important; ">
                                <a href="https://www.foodpanda.ph/restaurant/q3my/bakerandcook" target="_blank"><img src="{{asset('frontend/bakersandcook/images/food-panda.jpg')}}" style="width: 100%;"></a>
                            </div>
                        </div>
                    </div>
                </header>