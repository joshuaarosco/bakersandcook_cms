        <meta http-equiv="content-type" content="text/html; charset=utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="apple-touch-icon" sizes="57x57" href="{{asset('frontend/bakersandcook/images/favicon/apple-icon-57x57.png')}}">
        <link rel="apple-touch-icon" sizes="60x60" href="{{asset('frontend/bakersandcook/images/favicon/apple-icon-60x60.png')}}">
        <link rel="apple-touch-icon" sizes="72x72" href="{{asset('frontend/bakersandcook/images/favicon/apple-icon-72x72.png')}}">
        <link rel="apple-touch-icon" sizes="76x76" href="{{asset('frontend/bakersandcook/images/favicon/apple-icon-76x76.png')}}">
        <link rel="apple-touch-icon" sizes="114x114" href="{{asset('frontend/bakersandcook/images/favicon/apple-icon-114x114.png')}}">
        <link rel="apple-touch-icon" sizes="120x120" href="{{asset('frontend/bakersandcook/images/favicon/apple-icon-120x120.png')}}">
        <link rel="apple-touch-icon" sizes="144x144" href="{{asset('frontend/bakersandcook/images/favicon/apple-icon-144x144.png')}}">
        <link rel="apple-touch-icon" sizes="152x152" href="{{asset('frontend/bakersandcook/images/favicon/apple-icon-152x152.png')}}">
        <link rel="apple-touch-icon" sizes="180x180" href="{{asset('frontend/bakersandcook/images/favicon/apple-icon-180x180.png')}}">
        <link rel="icon" type="image/png" sizes="192x192"  href="{{asset('frontend/bakersandcook/images/favicon/android-icon-192x192.p')}}ng">
        <link rel="icon" type="image/png" sizes="32x32" href="{{asset('frontend/bakersandcook/images/favicon/favicon-32x32.png')}}">
        <link rel="icon" type="image/png" sizes="96x96" href="{{asset('frontend/bakersandcook/images/favicon/favicon-96x96.png')}}">
        <link rel="icon" type="image/png" sizes="16x16" href="{{asset('frontend/bakersandcook/images/favicon/favicon-16x16.png')}}">
        <link rel="manifest" href="{{asset('frontend/bakersandcook/images/favicon/manifest.json')}}">
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
        <meta name="theme-color" content="#ffffff">
        <!-- FONT CSS-->
        <link type="text/css" rel="stylesheet" href="https://fonts.googleapis.com/css?family=pacifico">
        <link type="text/css" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Cuprum:400,700">
        <link type="text/css" rel="stylesheet" href="{{asset('frontend/bakersandcook/font/font-icon/font-awesome/css/font-awesome.css')}}">
        <link type="text/css" rel="stylesheet" href="{{asset('frontend/bakersandcook/font/font-icon/font-oganic/flaticon.css')}}">
        <!-- LIBRARY CSS-->
        <link type="text/css" rel="stylesheet" href="{{asset('frontend/bakersandcook/libs/bootstrap/css/bootstrap.min.css')}}">
        <link type="text/css" rel="stylesheet" href="{{asset('frontend/bakersandcook/libs/bootstrap-timepicker/jquery.timepicker.css')}}">
        <link type="text/css" rel="stylesheet" href="{{asset('frontend/bakersandcook/libs/bootstrap-datepicker/css/bootstrap-datepicker.min.css')}}">
        <link type="text/css" rel="stylesheet" href="{{asset('frontend/bakersandcook/libs/animate/animate.css')}}">
        <link type="text/css" rel="stylesheet" href="{{asset('frontend/bakersandcook/libs/slick-slider/slick.css')}}">
        <link type="text/css" rel="stylesheet" href="{{asset('frontend/bakersandcook/libs/slick-slider/slick-theme.css')}}">
        <link type="text/css" rel="stylesheet" href="{{asset('frontend/bakersandcook/libs/fancybox/css/jquery.fancybox.css')}}">
        <link type="text/css" rel="stylesheet" href="{{asset('frontend/bakersandcook/libs/fancybox/css/jquery.fancybox-buttons.css')}}">
        <link type="text/css" rel="stylesheet" href="{{asset('frontend/bakersandcook/libs/fancybox/css/jquery.fancybox-thumbs.css')}}">
        <!-- STYLE CSS-->
        <link type="text/css" rel="stylesheet" href="{{asset('frontend/bakersandcook/css/layout.css?v=3')}}">
        <link type="text/css" rel="stylesheet" href="{{asset('frontend/bakersandcook/css/components.css')}}">
        <link type="text/css" rel="stylesheet" href="{{asset('frontend/bakersandcook/css/responsive.css')}}">
        <link type="text/css" rel="stylesheet" href="{{asset('frontend/bakersandcook/css/modif.css?v=7')}}">
        <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
        <style>
                ul.list-info {
                       text-transform: capitalize;
                   }
                   .homepage-banner-warpper {
                       background: #000;
                       background: -webkit-radial-gradient(#000, transparent);
                       background: -o-radial-gradient(#000, transparent);
                       background: -moz-radial-gradient(#000, transparent);
                       background: radial-gradient(circle,rgba(0, 0, 0, 0.8) 15%, transparent,transparent);
                       background-position: bottom;
                   }
                   .panel {
                        margin-bottom: 20px;
                        border: 1px solid transparent;
                        border-radius: 4px;
                        -webkit-box-shadow: 0 1px 1px rgba(0,0,0,.05);
                        box-shadow: 0 1px 1px rgba(0,0,0,.05);
                        padding: 20px 20px;
                        background:none;
                    }
                    .nav-tabs>li.active>a, .nav-tabs>li.active>a:focus, .nav-tabs>li.active>a:hover {
                        color: #555;
                        cursor: default;
                        background-color: #ebdaa6;
                        border: none;
                        border-bottom-color: transparent;
                    }
                    .nav-tabs>li>a {
                        margin-right: 0;
                        line-height: 1.42857143;
                        border: none;
                        border-radius: 0;
                        font-family: 'Roboto', sans-serif;
                        color: #4c4c4c;
                    }
                    .nav-tabs {
                        border-bottom:none;
                    }
                    .panel-heading {
                        /*padding: 0 15px;*/
                        border-bottom: 1px solid transparent;
                        border-top-right-radius: 0;
                        display: table;
                        margin: 0 auto;
                    }
                    .tab-content {
                        padding: 20px 20px;
                        background: rgba(45, 45, 45,0.5) !important;
                    }
        </style>