<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-67024928-10', 'auto');
  ga('send', 'pageview');

</script>

	<script src="{{asset('frontend/bakersandcook/libs/jquery/jquery-2.2.4.min.j')}}s"></script>
	<script src="{{asset('frontend/bakersandcook/libs/bootstrap/js/bootstrap.min.js')}}"></script>
	<script src="{{asset('frontend/bakersandcook/libs/bootstrap-timepicker/jquery.timepicker.min.js')}}"></script>
	<script src="{{asset('frontend/bakersandcook/libs/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}"></script>
	<script src="{{asset('frontend/bakersandcook/libs/smooth-scroll/jquery-smoothscroll.min.js')}}"></script>
	<script src="{{asset('frontend/bakersandcook/libs/wow-js/wow.min.js')}}"></script>
	<script src="{{asset('frontend/bakersandcook/libs/slick-slider/slick.min.js')}}"></script>
	<script src="{{asset('frontend/bakersandcook/libs/isotope/isotope.pkgd.min.js')}}"></script>
	<script src="{{asset('frontend/bakersandcook/libs/fancybox/js/jquery.fancybox.min.js')}}"></script>
	<script src="{{asset('frontend/bakersandcook/libs/fancybox/js/jquery.fancybox-buttons.min.js')}}"></script>
	<script src="{{asset('frontend/bakersandcook/libs/fancybox/js/jquery.fancybox-thumbs.min.js')}}"></script>
  
	<!-- MAIN JS-->
	<script src="{{asset('frontend/bakersandcook/js/main.min.js?v=2')}}">
	</script>
	<!-- LOADING SCRIPTS FOR PAGE-->
	<script src="http://maps.googleapis.com/maps/api/js?libraries=places&amp;key=AIzaSyAtgrx4xpbvy1gitTYgksS6k-wWPnwL-1w"></script>
	<script src="{{asset('frontend/bakersandcook/js/pages/contact.min.js')}}"></script>
	<script src="{{asset('frontend/bakersandcook/libs/mail/contact_me.min.js')}}"></script>
	<script src="{{asset('frontend/bakersandcook/libs/mail/jqBootstrapValidation.min.js')}}"></script>
	<script type="text/javascript">
		$(function() {
		  $('a[href*="#"]:not([href="#"])').click(function() {
		    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
		      var target = $(this.hash);
		      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
		      if (target.length) {
			$('html, body').animate({
			  scrollTop: target.offset().top
			}, 1000);
			return false;
		      }
		    }
		  });
		});

$(document).ready( function() {
    $('#myCarousel').carousel({
		interval:   3000
	});
	$('#myCarousel2').carousel({
		interval:   3000
	});
	
	// var clickEvent = false;
	// $('#myCarousel').on('click', '.nav a', function() {
	// 		clickEvent = true;
	// 		$('.nav li').removeClass('active');
	// 		$(this).parent().addClass('active');		
	// }).on('slid.bs.carousel', function(e) {
	// 	if(!clickEvent) {
	// 		var count = $('.nav').children().length -1;
	// 		var current = $('.nav li.active');
	// 		current.removeClass('active').next().addClass('active');
	// 		var id = parseInt(current.data('slide-to'));
	// 		if(count == id) {
	// 			$('.nav li').first().addClass('active');	
	// 		}
	// 	}
	// 	clickEvent = false;
	// });
});
	</script>