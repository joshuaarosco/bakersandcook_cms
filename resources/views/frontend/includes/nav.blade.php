<nav>
   <ul>
    <li><a href="{{url('')}}" class="active " >home</a></li>
    <li><a href="{{url('')}}#slide2" class="nav-toggle" data-block='2' >About</a></li>
    <li><a href="{{url('')}}#slide4" class=" ">Biography</a></li>
    <li><a href="{{url('')}}#slide5" class="">Business</a></li>
    <li><a href="{{url('blog/personal')}}" class=" ">Personal Blogs</a></li>
    <li><a href="{{url('blog')}}" class=" ">News</a></li>
    <li><a href="{{url('')}}#slide7" class="">Advocacy</a></li>
    <li><a href="{{url('contact')}}" class="">Contact</a></li>
</ul>
<ul class="soc-network">
    <li><a href="https://facebook.com/josephcalata/" class="icon-facebook"></a></li>
    <li><a href="https://twitter.com/JosephHCalata/" class="icon-twitter"></a></li>
</ul>
</nav>