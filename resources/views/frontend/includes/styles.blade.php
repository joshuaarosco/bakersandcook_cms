<title>Joseph Calata</title>
<link rel="stylesheet" type="text/css" href="{{ asset('css/css_reset.css')}}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap.css')}}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/magnific-popup.css')}}">
<link rel="shortcut icon" href="{{ asset('img/favicon.png')}}" type="image/x-icon">
<!-- full page slider -->
<link rel="stylesheet" type="text/css" href="{{ asset('css/jquery.fullPage.css')}}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/index.css')}}">
<!-- Modal -->
<link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap2.css')}}">
<link href="https://fonts.googleapis.com/css?family=Great+Vibes" rel="stylesheet">
<style type="text/css">
	.alert {
		padding: 8px 35px 8px 14px;
		margin-bottom: 18px;
		color: #fff;
		background-color: #373636;
		border: 1px solid #626262;
		-webkit-border-radius: 4px;
		-moz-border-radius: 4px;
		border-radius: 4px;
	}
</style>
<script>
	$(document).ready(function(){
		$("#myBtn").click(function(){
			$("#myModal").modal();
		});
	});
</script>