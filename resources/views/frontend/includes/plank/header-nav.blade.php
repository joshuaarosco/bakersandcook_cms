                <header>
                    <div class="bg-transparent">
                        <div class="header-main">
                            <div class="container">
                                <div class="header-main-wrapper">
                                    <div class="hamburger-menu-mobile"><i class="icons fa fa-bars"></i></div>
                                    <div class="logo-wrapper">
                                        <a href="{{url('plank')}}" class="logo"><img src="{{asset('frontend/plank/images/logo/plank.png')}}" alt="" /></a>
                                    </div>
                                    <nav class="navigation">
                                        <ul class="nav-links nav navbar-nav">
                                            <li><a href="{{url('plank')}}" class="main-menu"><span class="text">Home</span></a></li>
                                            <li><a href="{{url('plank')}}#about" class="main-menu"><span class="text">about us</span></a></li>
                                            <li><a href="{{url('plank')}}#menus" class="main-menu"><span class="text">menu</span></a></li>
                                            <li><a href="{{url('plank')}}#contact" class="main-menu"><span class="text">contact</span></a></li>
                                            <li class="social-nav"><a href="https://www.facebook.com/BakerandCookPH/"><span class="fa fa-facebook"></span></a></li>
                                            <li class="social-nav"><a href="https://www.instagram.com/bakerandcookph/"><span class="fa fa-instagram"></span></a></li>
                                        </ul>
                                        <div class="button-search">
                                            <p class="main-menu"><i class="fa fa-search"></i></p>
                                        </div>
                                        <div class="nav-search hide">
                                            <form><input type="text" placeholder="Search" class="searchbox" /><button type="submit" class="searchbutton fa fa-search"></button></form>
                                        </div>
                                    </nav>
                                    <div class="button-search-mobile"><i class="icons fa fa-search"></i></div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </header>