        <meta http-equiv="content-type" content="text/html; charset=utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- FONT CSS-->
        <link type="text/css" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Pacifico">
        <link type="text/css" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Cuprum:400,700">
        <link type="text/css" rel="stylesheet" href="{{asset('frontend/plank/font/font-icon/font-awesome/css/font-awesome.css')}}">
        <link type="text/css" rel="stylesheet" href="{{asset('frontend/plank/font/font-icon/font-oganic/flaticon.css')}}">
        <!-- LIBRARY CSS-->
        <link type="text/css" rel="stylesheet" href="{{asset('frontend/plank/libs/bootstrap/css/bootstrap.min.css')}}">
        <link type="text/css" rel="stylesheet" href="{{asset('frontend/plank/libs/bootstrap-timepicker/jquery.timepicker.css')}}">
        <link type="text/css" rel="stylesheet" href="{{asset('frontend/plank/libs/bootstrap-datepicker/css/bootstrap-datepicker.min.css')}}">
        <link type="text/css" rel="stylesheet" href="{{asset('frontend/plank/libs/animate/animate.css')}}">
        <link type="text/css" rel="stylesheet" href="{{asset('frontend/plank/libs/slick-slider/slick.css')}}">
        <link type="text/css" rel="stylesheet" href="{{asset('frontend/plank/libs/slick-slider/slick-theme.css')}}">
        <link type="text/css" rel="stylesheet" href="{{asset('frontend/plank/libs/fancybox/css/jquery.fancybox.css')}}">
        <link type="text/css" rel="stylesheet" href="{{asset('frontend/plank/libs/fancybox/css/jquery.fancybox-buttons.css')}}">
        <link type="text/css" rel="stylesheet" href="{{asset('frontend/plank/libs/fancybox/css/jquery.fancybox-thumbs.css')}}">
        <!-- STYLE CSS-->
        <link type="text/css" rel="stylesheet" href="{{asset('frontend/plank/css/layout.css')}}">
        <link type="text/css" rel="stylesheet" href="{{asset('frontend/plank/css/components.css')}}">
        <link type="text/css" rel="stylesheet" href="{{asset('frontend/plank/css/responsive.css')}}">
        <link type="text/css" rel="stylesheet" href="{{asset('frontend/plank/css/responsive.css')}}">
        <link type="text/css" rel="stylesheet" href="{{asset('frontend/plank/css/modif.css?v=6')}}">

        <link href="https://fonts.googleapis.com/css?family=Merienda+One|Roboto" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,900" rel="stylesheet">        

        <style>
                   .homepage-banner-warpper {
                       background: #000;
                       background: -webkit-radial-gradient(#000, transparent);
                       background: -o-radial-gradient(#000, transparent);
                       background: -moz-radial-gradient(#000, transparent);
                       background: radial-gradient(circle,rgba(0, 0, 0, 0.8) 15%, transparent,transparent);
                       background-position: bottom;
                   }
                   .panel {
                        /*background-color: rgba(0, 0, 0, 0.14);*/
                        background-color: transparent;
                    }
                    .panel-default>.panel-heading {
                        color: #fff;
                        background-color: transparent;
                        border-color: transparent;
                        margin: 0 auto;
                    }
                    .nav>li>a {
                      position: relative;
                      display: block;
                      padding: 10px 15px;
                      color: #333030;
                      font-weight: 500;
                      text-transform: uppercase;
                      font-size: 18px;
                      font-family: 'Roboto'
                      /*font-family: 'Merienda One', cursive;*/
                    }
                    .nav-tabs>li.active>a, .nav-tabs>li.active>a:focus, .nav-tabs>li.active>a:hover {
                        color: #008d6b;
                        cursor: default;
                        background-color: transparent;
                        border: none;
                        border-bottom-color: transparent;
                    }
                    .nav-tabs>li {
                        float: left;
                        margin-bottom: -1px;
                        width: 25%;
                        text-align: center;
                    }
                    .nav-tabs {
                        border-bottom: 1px solid #008d6b;
                    }
                    .panel-default {
                        border-color: transparent;
                    }
                    .nav-tabs>li>a:hover {
                        border-color: transparent;
                    }
                    @media screen and (min-width: 769px) {
                        .carousel {
                            position: relative;
                            padding: 0 75px;
                            background: transparent!important;
                        }
                    }

                    #fade-quote-carousel.carousel {
                      padding-bottom: 60px;
                  }
                  #fade-quote-carousel.carousel .carousel-inner .item {
                      opacity: 0;
                      -webkit-transition-property: opacity;
                      -ms-transition-property: opacity;
                      transition-property: opacity;
                  }
                  #fade-quote-carousel.carousel .carousel-inner .active {
                      opacity: 1;
                      -webkit-transition-property: opacity;
                      -ms-transition-property: opacity;
                      transition-property: opacity;
                  }
                  #fade-quote-carousel.carousel .carousel-indicators {
                      bottom: 10px;
                      visibility: hidden;
                  }
                  #fade-quote-carousel.carousel .carousel-indicators > li {
                      background-color: #e84a64;
                      border: none;
                  }
                  #fade-quote-carousel blockquote {
                    text-align: center;
                    border: none;
                }
                #fade-quote-carousel .profile-circle {
                    width: 100px;
                    height: 100px;
                    margin: 0 auto;
                    border-radius: 100px;
                }
        </style>