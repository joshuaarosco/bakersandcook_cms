<!DOCTYPE html>
<html lang="en">

<head>
		<title>Baker and Cook Dean Brettschneider</title>
        <meta charset="utf-8">
        <meta name="description" content="Baker & Cook - Singapore's only true artisan bakery & foodstore chain is the brainchild of Global Baker Dean Brettschneider.">
        <meta name="author" content="Jae-Mar Arenque 2018">
        <meta name="keywords" content="Baker and Cook, Pizza at MOA, Plank">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <link rel="canonical" href="http://bakerandcook.com.ph/">
        <!-- Mobile Specific Meta  -->
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <meta name="robots" content = "index, follow">

		@include('frontend.includes.bakersandcook.styles')
		<style type="text/css">
               #about h1.title{     text-shadow: 0px 1px 2px #fff; }
               #menu h1.title{     text-shadow: 0px 1px 2px #555; }
               #menu .carousel-inner h2.title,#menu .carousel-inner ul.list-info{    text-shadow: 0px 1px 2px #555;}
    	</style>
	</head>

	<body>
		<!--BEGIN PAGE LOADER-->
		<div class="body-wrapper">
			@include('frontend.includes.bakersandcook.header')
			<!-- WRAPPER CONTENT-->
			<div class="wrapper-content">
				<!-- HEADER-->
				@include('frontend.includes.bakersandcook.header-nav')
				<!-- MAIN CONTENT-->
				<div class="main-contents">
					<!-- Slider-->
					<section id="home" class="background-slide">
					@foreach($header as $index=>$info)
						<div class="slide-item"><img src="{{asset($info->directory.'/'.$info->filename)}}" alt="" data-parallax="{&quot;y&quot;: 200}" class="img-responsive img-background">
							<div class="container">
								<div class="homepage-banner-warpper">
									<div class="homepage-banner-content">
											<h1 class="title mobile">
											@if($info->image_filename!="")
											<img src="{{asset($info->directory.'/'.$info->image_filename)}}" style="margin: 0 auto; width: 40%" alt="">
											@endif
											</h1>
											<h1 class="title">{!!html_entity_decode($info->header,ENT_QUOTES, 'UTF-8')!!}</h1>
                                            <h2 class="subtitle">{!!html_entity_decode($info->sub_header,ENT_QUOTES, 'UTF-8')!!}</h2>
										@if($info->button_title!=""&&$info->button_link!="")
										<div class="group-btn"><a href="{{$info->button_link}}" target="_blank" 
												class="btn btn-right btn-transparent fadeInRight">{{$info->button_title}}</a></div>
										@endif
									</div>
								</div>
							</div>
						</div>
					@endforeach
						<!-- <div class="slide-item"><img src="{{asset('frontend/bakersandcook/images/background-full/homepage-2.jpg')}}" alt="" data-parallax="{&quot;y&quot;: 200}" class="img-responsive img-background">
							<div class="container">
								<div class="homepage-banner-warpper">
									<div class="homepage-banner-content">
										 <h1 class="title mobile"><img src="{{asset('frontend/bakersandcook/images/background-full/plank-logo.png')}}" style="margin: 0 auto;" alt=""></h1>
										<div class="group-btn"><a href="{{url('plank')}}" target="_blank" 
												class="btn btn-right btn-transparent fadeInRight"> Go to Plank </a></div>
									</div>
								</div>
							</div>
						</div> -->
					</section>
					<!-- About us-->
					<section id="about" class="banner customer-review padding-top-20 padding-bottom-20">
						<div class="container">
							<div class="main-titles">
								<h1 class="title" style="margin-top: 30px;">welcome</h1>
							</div>
							<div class="main-content">
									<div data-wow-delay="0.3s" class="about-center wow zoomIn"><img src="{{asset('frontend/bakersandcook/images/more-image/plate.png?v=2')}}"  style="margin: 0 auto; width: 20%" alt="" class="img-responsive"></div>
									<div class="clearfix"></div>
								<div class="col-ms-12" style="background: rgba(235, 218, 166, 0.91); border-radius: 50px; padding: 45px; margin-top: 40px">
									
									<div class="main-titles">
										<h1 class="title">About Us</h1>
									</div>
									@foreach($abouts as $index=>$info)
										<h4 class="text-justify" style="color: #000; padding: 10px; font-family: 'Roboto'; font-weight: 400;">{!!html_entity_decode($info->content,ENT_QUOTES, 'UTF-8')!!}</h4>
									@endforeach
								</div>
							</div>
						</div>
					</section>
					<!-- Products-->
					<!-- Banner 2-->
					<section class="banner banner-2 padding-top-20 padding-bottom-20" id="menu">
                    <div class="container">
                        <div class="main-titles">
                            <h1 class="title" style="color: #fff;">our specialties</h1>
                        </div>
                        <div class="panel with-nav-tabs panel-default">
                            <div class="panel-heading">
                                <ul class="nav nav-tabs">
                                    <li class="active"><a href="#tab1default" data-toggle="tab">Bread & Pastries</a></li>
                                    <li><a href="#tab2default" data-toggle="tab">Cakes & Slices</a></li>
                                    <li><a href="#tab3default" data-toggle="tab">Tarts & Pies</a></li>
                                    <!-- <li><a href="#tab4default" data-toggle="tab">All day Breakfast</a></li> -->
                                    <li><a href="#tab5default" data-toggle="tab">To-Go items</a></li>
                                </ul>
                            </div>
                            <div class="panel-body">
                                <div class="tab-content">
                                    <div class="tab-pane fade in active" id="tab1default">
                                        <div class="row wrapper-carousel">
                                            <div class="col-sm-5">
	                                            <div id="myCarousel2" class="carousel  slide">
	                                                <div class="carousel-inner">
                                                    <?php $x = 1;?>
                                                    @foreach($image_slider as $index => $info)
                                                        @if($info->type == "bread-pastries")
		                                                <div class="{{$x==1? "active": NULL}} item">  
                                                            <img src="{{asset($info->directory.'/'.$info->filename)}}" class="img-responsive">
                                                        </div>
                                                        <?php $x=0;?>
                                                        @endif
                                                    @endforeach
	                                                </div>
	                                            </div>
                                            </div>

                                            <div class="col-sm-7">
                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        <h2 class="title">Bread</h2>
                                                        
                                                        @foreach($bread_pastries as $index => $info)
                                                            @if($info->type == "bread")
                                                            {!!$info->food_name!!}
                                                            @endif
                                                        @endforeach
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <h2 class="title">Pastries</h2>
                                                        {{-- <ul class="list-info">
                                                            <li>Croissant</li>
                                                            <li>Pain au Chocolat</li>
                                                            <li>Almond Croissant</li>
                                                            <li>Multigrain Croissant</li>
                                                            <li>Almond Chocolate Croissant</li>
                                                            <li>cheese twist</li>
                                                            <li>Danish blueberry</li>
                                                            <li>Danish Crown Mixed Berries</li>
                                                            <li>Danish Peach</li>
                                                            <li>Cinnamon Swirl</li>
                                                            <li>Vanilla Bambolini</li>
                                                            <li>Chocolate Bambolini</li>
                                                            <li>Caramel Bambolini</li>
                                                        </ul> --}}
                                                        @foreach($bread_pastries as $index => $info)
                                                            @if($info->type == "pastry")
                                                            {!!$info->food_name!!}
                                                            @endif
                                                        @endforeach
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="tab2default">
                                        <div class="row">
                                            <div class="col-sm-5">
                                                <div id="myCarousel2" class="carousel  slide">
                                                    <div class="carousel-inner">
                                                        <?php $x = 1;?>
                                                        @foreach($image_slider as $index => $info)
                                                        @if($info->type == "cakes-slices")
                                                        <div class="{{$x==1? "active": NULL}} item">  
                                                            <img src="{{asset($info->directory.'/'.$info->filename)}}" class="img-responsive">
                                                        </div>
                                                        <?php $x=0;?>
                                                        @endif
                                                        @endforeach
                                                        {{-- <div class="item"> 
                                                            <img src="{{asset('frontend/bakersandcook/images/products/15-pastries-2643-0591.jpg')}}" class="img-responsive">
                                                        </div> --}}
                                                    </div>
                                                </div>
                                            </div>
                                                    <div class="col-sm-7">
                                                        <h2 class="title">Cakes & Slices</h2>
                                                        {{-- <ul class="list-info">
                                                            <li>Chocolate Indulgent Cake</li>
                                                            <li>Carrot Cake</li>
                                                            <li>Blueberry CheeseCake</li>
                                                            <li>Passionfruit Cheesecake</li>
                                                            <li>Gluten Free Orange Almond Cake</li>
                                                            <li>Red Velvet Cake</li>
                                                            <li>assorted fruit tart</li>
                                                            <li>cherry tart</li>
                                                            <li>chocolate cheesecake</li>
                                                            <li>blueberry tart</li>
                                                            <li>Chocolate Indulgent Cake slice</li>
                                                            <li>Caramel Chocolate Brownie</li>
                                                            <li>Chocolate Lamington</li>
                                                            <li>Strawberry Lamington</li>
                                                            <li>Lemon Drizzle</li>
                                                            <li>Canele</li>
                                                            <li>Mille Feullie</li>
                                                            <li>Chocolate Eclairs</li>
                                                            <li>�clair with Craquelin</li>
                                                            <li>Pavlova w/ berries and cream</li>
                                                        </ul> --}}
                                                        @foreach($cakes_slices as $index => $info)
                                                            {!!$info->food_name!!}
                                                        @endforeach
                                                    </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="tab3default">
                                        <div class="row">
                                            <div class="col-sm-5">
                                                <div id="myCarousel2" class="carousel  slide">
                                                    <div class="carousel-inner">
                                                    <?php $x = 1;?>
                                                    @foreach($image_slider as $index => $info)
                                                        @if($info->type == "tarts-pies")
                                                        <div class="{{$x==1? "active": NULL}} item">  
                                                            <img src="{{asset($info->directory.'/'.$info->filename)}}" class="img-responsive">
                                                        </div>
                                                        <?php $x=0;?>
                                                        @endif
                                                    @endforeach
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-7">
                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        <h2 class="title">Tarts</h2>
                                                        {{-- <ul class="list-info">
                                                            <li>Pecan Tart</li>
                                                            <li>assorted fruit tart</li>
                                                            <li>peach tart</li>
                                                            <li>profite rolls</li>
                                                        </ul> --}}
                                                        @foreach($tarts_pies as $index => $info)
                                                            @if($info->type == "tarts")
                                                            {!!$info->food_name!!}
                                                            @endif
                                                        @endforeach
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <h2 class="title">Pies</h2>
                                                        {{-- <ul class="list-info">
                                                            <li>Salmon Quiche</li>
                                                            <li>Salmon Quiche</li>
                                                            <li>Veggie Quiche</li>
                                                            <li>Chili Con Carne</li>
                                                            <li>Chicken Roll</li>
                                                            <li>Steak Pie</li>
                                                            <li>Minced Beef pie</li>
                                                            <li>Wild Mushroom &amp; Bacon Tart</li>
                                                            <li>Spinach &amp; Potato Feta Tart</li>
                                                        </ul> --}}
                                                        @foreach($tarts_pies as $index => $info)
                                                            @if($info->type == "pies")
                                                            {!!$info->food_name!!}
                                                            @endif
                                                        @endforeach
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="tab4default">
                                        <div class="row">
                                            <div class="col-sm-5">
                                                <div id="myCarousel1" class="carousel  slide">
                                                    <div class="carousel-inner">
                                                    <?php $x = 1;?>
                                                    @foreach($image_slider as $index => $info)
                                                        @if($info->type == "breakfast")
                                                        <div class="{{$x==1? "active": NULL}} item">  
                                                            <img src="{{asset($info->directory.'/'.$info->filename)}}" class="img-responsive">
                                                        </div>
                                                        <?php $x=0;?>
                                                        @endif
                                                    @endforeach
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-7">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <h2 class="title">All Day Breakfast</h2>
                                                        <div class="row">
                                                            <ul class="list-info col-sm-6 ml-40">
                                                                <li>Eggs Benedict with Bacon</li>
                                                                <li>Eggs Benedict with Salmon</li>
                                                                <li>Simply Scrambled Eggs w/ Bacon</li>
                                                                <li>Banana Pecan Pancake</li>
                                                                <li>Mango Pancake</li>
                                                            </ul>
                                                            {{--
                                                            @foreach($breakfasts as $index => $info)
                                                                {!!$info->food_name!!}
                                                            @endforeach
                                                            --}}
                                                        </div>

                                                        <h2 class="title">Sandwiches</h2>
                                                        <div class="row">
                                                            <ul class="list-info col-sm-6 ml-40">
                                                                <li>Chicken Salad Sandwich</li>
                                                                <li>Beef Banh Mi</li>
                                                                <li>Ham & Cheese Sandwich</li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="tab5default">
                                        <div class="row">
                                            <div class="col-sm-5">
                                                <div id="myCarousel2" class="carousel  slide">
                                                    <div class="carousel-inner">
                                                    <?php $x = 1;?>
                                                    @foreach($image_slider as $index => $info)
                                                        @if($info->type == "items")
                                                        <div class="{{$x==1? "active": NULL}} item">  
                                                            <img src="{{asset($info->directory.'/'.$info->filename)}}" class="img-responsive">
                                                        </div>
                                                        <?php $x=0;?>
                                                        @endif
                                                    @endforeach
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-7">
                                                {{-- <ul class="list-info">
                                                    <li>Plain Shortbread</li>
                                                    <li>Cranberry Shortbread</li>
                                                    <li>Chocolate Chip Cookies</li>
                                                    <li>Fruit &amp; Nut Bar</li>
                                                    <li>Granola</li>
                                                    <li>Orange Jam</li>
                                                    <li>Strawberry Jam</li>
                                                    <li>Strawberry Jam small</li>
                                                    <li>smoky pepper</li>
                                                    <li>lemony hummus</li>
                                                    <li>tzatziki</li>
                                                    <li>truffle butter</li>
                                                </ul> --}}
                                                @foreach($items as $index => $info)
                                                {!!$info->item_name!!}
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- <div style="background: rgba(235, 218, 166, 0.91); border-radius: 50px; color: 000; padding: 20px;">
                            <p><strong>Promo: baker & Cook Bread offered in longger time and big Discount! Starting on tuesday Oct 2</strong></p>
                            <table class="table table-bordered">
                                <tr>
                                    <th>Offer</th>
                                    <th>Time</th>
                                </tr>
                                <tr>
                                    <td>
                                        40% OFF for Bistro Card holders <br>
                                        30% OFF for non-card holders
                                    </td>
                                    <td style="vertical-align: center">
                                        8pm onwards
                                    </td>
                                </tr>
                            </table>
                        </div> -->
                    </div>
                </section>
					<section id="contact" class="contact-bg padding-top-20 padding-bottom-20">
						<div class="container">
							<div class="main-titles">
								<h1 class="title" style="color: #fff; margin-top: 30px;">Contact Us</h1></div>
							<div class="main-content">
								<div class="row">
									
									
									<div class="col-md-6">
										<div class="desc-int" style="padding: 50px 0!important;">
                                        <div class="main-titles text-center">
                                            <div class="main-titles">
                                                 <h1 class="title" style="margin-bottom: 3%!important;line-height: 45px; color: #ebdaa6">Time<br>Open</h1></div>
                                                <h2 style="">Everyday</h2>
                                                <div style="color: white">
                                                    <h3>
                                                        <span class="fa fa-map-marker" style="margin-right: 5px;"></span> 
                                                        G/F S Maison, Conrad Manila
                                                    </h3>
                                                    <h3>
                                                        <span class="fa fa-check" style="margin-right: 5px;"></span> 
                                                        10:00AM - 11:00PM
                                                    </h3>
                                                    <h3>
                                                        <a style="color: #fff;" href="tel:836-7141"><span class="fa fa-phone" style="margin-right: 5px;"></span> 809-2340</a>
                                                    </h3>
                                                </div>
                                                <hr style="width: 100px;border-color: #5f5b55;">

                                                <div style="color: white;margin-top: 20px;">
                                                    <h3>
	                                                    <a style="color: #fff;" href="https://www.facebook.com/BakerandCookPH/" target="_blank">
	                                                    <span class="fa fa-facebook-square" style="margin-right: 5px;"></span></a> 
	                                                    
	                                                    <a style="color: #fff;" href="https://www.instagram.com/bakerandcookph/" target="_blank">
	                                                    <span class="fa fa-instagram" style="margin-right: 5px;"></span>
	                                                    </a>  
	                                                    @BakerandCookPh
                                                    </h3>
                                                </div>
                                        </div>
                                    </div>
									</div>
									<div class="col-md-6" style="color: white">
										@if(Session::get('notification-status')=="success") 
		                                <div class="alert bg-success" style="color: black;">Your message successfully sent!</div>
		                                @elseif(Session::get('notification-status')=="failed")
		                                <div class="alert bg-success" style="color: black;">Your message failed to sent!</div>
		                                @endif
										<p><span class="fa fa-send" style="margin-right: 10px;"></span> CONTACT US</p>
										<form action="{{url('inquiry/sent')}}" method="POST" class="contact-form">
										<input type="hidden" value="{{csrf_token()}}" name="_token">
										<input type="hidden" name="type" value="bakerandcook">
										<div class="row">
											<div class="col-sm-6 text-center">
												<div class="form-group"><label class="form-label">NAME <span class="highlight">*</span></label><input id="name" type="text" required="required" name="name"data-validation-required-message="Please enter your name." class="form-control">
													<p
														class="help-block text-danger"></p>
												</div>
											</div>
											<div class="col-sm-6 text-center">
												<div class="form-group"><label class="form-label">PHONE</label><input id="phone" type="text" required="required" name="phone" data-validation-required-message="Please enter your phone number." class="form-control">
													<p class="help-block text-danger"></p>
												</div>
											</div>
											<div class="col-sm-12 text-center">
												<div class="form-group"><label class="form-label">EMAIL <span class="highlight">*</span></label><input id="email" name="email" type="email" required="required" data-validation-required-message="Please enter your email address." class="form-control">
													<p
														class="help-block text-danger"></p>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-12">
												<div class="form-group"><label class="form-label">HOW CAN WE HELP?</label><textarea id="message" required="required" name="message" data-validation-required-message="Please enter a message." class="form-control form-textarea"></textarea>
													<p class="help-block text-danger"></p>
												</div>
											</div>
										</div>
										<button type="submit" class="btn btn-maincolor">Send Message </button>
										<!-- <div id="success" class="margin-top-50">
											
										</div> -->
										</form>
									</div>
								</div>


							</div>
						</div>
					</section>
						
				</div>
				<!-- FOOTER-->
				@include('frontend.includes.bakersandcook.footer')
			</div>
		</div>
		
				@include('frontend.includes.bakersandcook.scripts')
	</body>


</html>