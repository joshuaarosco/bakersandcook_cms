<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Baker & Cook Sourdough Pizza by Plank</title>
    <meta charset="utf-8">
    <meta name="description" content="PLANK SOURDOUGH PIZZA is masterminded by Global Baker Dean Brett scheneider and the same team that 
developed and operates the successful Baker and Cook chain of artisan bakeries and food stores. ">
    <meta name="author" content="Jae-Mar Arenque 2018">
    <meta name="keywords" content="Baker and Cook, Pizza at MOA, Plank">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="canonical" href="http://bakerandcook.com.ph/">
    <!-- Mobile Specific Meta  -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="robots" content = "index, follow">

	@include('frontend.includes.plank.styles')

</head>
<body>
	<div class="body-wrapper">
		@include('frontend.includes.plank.header')



		<div class="wrapper-content">
                <!-- HEADER-->
                @include('frontend.includes.plank.header-nav')
                <!-- MAIN CONTENT-->
                <div class="main-contents">
                    <!-- Slider-->
                    <section id="home" class="background-slide">
                        @foreach($header as $index=>$info)
                        <div class="slide-item"><img src="{{asset($info->directory.'/'.$info->filename)}}" alt="" class="img-responsive img-background" style="width: auto !important;">
                            <div class="container">
                                <div class="homepage-banner-warpper">
                                    <div class="homepage-banner-content">
                                        <div class="group-title fadeInDown mobile">
                                            <h1 class="title mobile">
                                            @if($info->image_filename!="")
                                            <img src="{{asset($info->directory.'/'.$info->image_filename)}}" style="margin: 0 auto;" alt="">
                                            @endif
                                            </h1>
                                            <br>
                                            <!-- <hr class="sep"> -->
                                            <h1 class="title" style="font-size: 3.375rem;">{!!html_entity_decode($info->header,ENT_QUOTES, 'UTF-8')!!}</h1>
                                            <h2 class="subtitle">{!!html_entity_decode($info->sub_header,ENT_QUOTES, 'UTF-8')!!}</h2></div>
                                        @if($info->button_title!=""&&$info->button_link!="")
                                        <div class="group-btn"><a href="{{$info->button_link}}"
                                                class="btn btn-right btn-transparent fadeInRight">{{$info->button_title}}</a></div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                       <!--  <div class="slide-item"><img src="{{asset('frontend/plank/images/background-full/homepage-2.jpg')}}" alt="" class="img-responsive img-background">
                            <div class="container">
                                <div class="homepage-banner-warpper">
                                    <div class="homepage-banner-content">
                                        <div class="group-title animated fadeInDown hidden">
                                            <h1 class="title">Plank Sourdough Pizza</h1>
                                            <h2 class="subtitle">I'd rather have left over feelings than left over pizza</h2></div>
                                        <div class="group-btn"><a href="#"
                                                class="btn btn-right btn-transparent fadeInLeft"> ORDER NOW</a></div>
                                    </div>
                                </div>
                            </div>
                        </div> -->
                    </section>
                    <!-- About us-->
                    <section id="about" class="about-us padding-top-15 padding-bottom-15">
                        <div class="container">

                            <div class="main-titles">
                                <h1 class="title" style="margin-bottom: 3%!important;"><img src="{{asset('frontend/plank/images/background-full/Discover.png?v=1.2')}}" alt=""> <br>
                                <img src="{{asset('frontend/plank/images/background-full/88.png')}}" alt="" style="margin: 0 auto; width: 50px"></h1></div>
                                <div class="text-center" style="margin-bottom: 3%;"><h4>
                                @foreach($abouts as $index=>$info)
                                @if($info->type=="discover")
                                <strong>{!!html_entity_decode($info->content,ENT_QUOTES, 'UTF-8')!!}</strong>
                                @endif
                                @endforeach</h4></div>
                            
                            <div class="main-content col-md-10 col-md-offset-1">
                               	<div class="row">
                               		<div class="col-md-offset-2 col-sm-8">
                               			<div class="text-center">
                               				<span class="text-center"><img src="{{asset('frontend/plank/images/background-full/coffee.png')}}" alt=""  style="margin: 0 auto; vertical-align: center;"></span>
                               			<h3>OUR <strong>FOOD</strong></h3>
                               			<hr class="sep" style="border-color: #008d6b; margin-bottom: 10px;">
                               			@foreach($abouts as $index=>$info)
                                        @if($info->type=="our food")
                               			<p>{!!html_entity_decode($info->content,ENT_QUOTES, 'UTF-8')!!}</p>
                                        @endif
                                        @endforeach
                               			</div>
                               		</div>
                               	</div>
                            </div>
                        </div>
                    </section>
                    <!-- Customer-->
                    <section id="menus" class="banner customer-review padding-top-15 padding-bottom-15">
                        <div class="container">
                            <div class="main-titles">
                                 <h1 class="title" style="margin-bottom: 3%!important;"><img src="{{asset('frontend/plank/images/background-full/Discover.png')}}" alt="" style="margin: auto !important; " class="img-responsive"> </h1>
                            </div>
                            <div class="top">
                                <img src="{{asset('frontend/plank/images/background-full/border-top.png')}}" style="padding-bottom: 10px;" alt="">
                            </div>
                            <div class="panel with-nav-tabs panel-default">
                            <div class="panel-heading">
                                    <ul class="nav nav-tabs">
                                        <li class="active"><a href="#tab1default" data-toggle="tab">Appetizers</a></li>
                                        <li><a href="#tab2default" data-toggle="tab">Pizza</a></li>
                                        <li><a href="#tab3default" data-toggle="tab">Entrees</a></li>
                                        <li><a href="#tab4default" data-toggle="tab">Pasta</a></li>
                                    </ul>
                            </div>
                            <div class="panel-body">
                                <div class="tab-content">
                                    <div class="tab-pane fade in active" id="tab1default">
                                        <div class="row wrapper-carousel">
                                            <div class="col-sm-6">
                                                <div class="carousel slide" id="fade-quote-carousel" data-ride="carousel" data-interval="3000">
                                                  <!-- Carousel indicators -->
                                                  <ol class="carousel-indicators">
                                                    <li data-target="#fade-quote-carousel" data-slide-to="0"></li>
                                                    <li data-target="#fade-quote-carousel" data-slide-to="1" class="active"></li>
                                                    <li data-target="#fade-quote-carousel" data-slide-to="2"></li>
                                                    <li data-target="#fade-quote-carousel" data-slide-to="3"></li>
                                                </ol>
                                                <!-- Carousel items -->
                                                <div class="carousel-inner">
                                                    <div class="item">
                                                        <img src="{{asset('frontend/plank/images/appetizers/app-1.jpg')}}">
                                                    </div>
                                                    <div class="item">
                                                        <img src="{{asset('frontend/plank/images/appetizers/salad-1.jpg')}}">
                                                    </div>
                                                    <div class="active item">
                                                        <img src="{{asset('frontend/plank/images/appetizers/app-2.jpg')}}">
                                                    </div>  
                                                    <div class="item">
                                                        <img src="{{asset('frontend/plank/images/appetizers/salad-2.jpg')}}">
                                                    </div>                      
                                                </div>
                                            </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <div class="row">
                                                            <div class="col-sm-6">
                                                                <h2 class="title">Appetizers</h2>
                                                                {{-- <ul class="list-info">
                                                                    <li>Trio B&amp;C Dips with Crostinis</li>
                                                                    <li>Plank Crispy Garlic Bread</li>
                                                                    <li>Cheese Plank Pizza</li>
                                                                </ul> --}}
                                                                @foreach($start as $index => $info)
                                                                @if($info->type == "start")
                                                                    {!!$info->food_name!!}
                                                                @endif
                                                                @endforeach
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <h2 class="title">Salads</h2>
                                                                {{-- <ul class="list-info">
                                                                    <li>Green Salad</li>
                                                                    <li>Caprese Salad</li>
                                                                </ul> --}}
                                                                @foreach($start as $index => $info)
                                                                @if($info->type == "salads")
                                                                    {!!$info->food_name!!}
                                                                @endif
                                                                @endforeach
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="tab2default">
                                        <div class="row">
                                        <div class="col-sm-6">
                                                <div class="carousel slide" id="fade-quote-carousel" data-ride="carousel" data-interval="3000">
                                                  <!-- Carousel indicators -->
                                                  <ol class="carousel-indicators">
                                                    <li data-target="#fade-quote-carousel" data-slide-to="0"></li>
                                                    <li data-target="#fade-quote-carousel" data-slide-to="1" class="active"></li>
                                                    <li data-target="#fade-quote-carousel" data-slide-to="2"></li>
                                                    <li data-target="#fade-quote-carousel" data-slide-to="3"></li>
                                                </ol>
                                                <!-- Carousel items -->
                                                <div class="carousel-inner">
                                                    <div class="item ">
                                                        <img src="{{asset('frontend/plank/images/pizzas/pizza1.jpg')}}">
                                                    </div>
                                                    <div class="item active">
                                                        <img src="{{asset('frontend/plank/images/pizzas/pizza2.jpg')}}">
                                                    </div>
                                                    <div class="item">
                                                        <img src="{{asset('frontend/plank/images/pizzas/pizza3.jpg')}}">
                                                    </div>  
                                                    <div class="item">
                                                        <img src="{{asset('frontend/plank/images/pizzas/pizza4.jpg')}}">
                                                    </div>                      
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <h2 class="title" style="color:#008d6b">Margherita Pizza</h2>
                                                    <p class="description">Tomato, mozzarella, sharp cheddar, caramelized garlic in balsamic reduction, fresh basil, extra virgin olive oil.</p> 
                                                    <hr> 
                                                     <h2 class="title" style="color:#008d6b">Okonomiyaki Pizza</h2>
                                                    <p class="description">Japanese bbq sauce, mozzarella, sharp cheddar, shredded cabbage, onions, crabsticks, bacon, japanese mayo, katsuobushi.</p>
                                                    <hr>
                                                    <h2 class="title" style="color:#008d6b">Seafood Pizza</h2>
                                                    <p class="description">Creamy tomato sauce, mozzarella, sharp chedddar, shiitake-mushroom, shrimps, salmon, capers, arugula, extra virgin olive oil.</p>
                                                    <hr>
                                                    <h2 class="title" style="color:#008d6b">Prosciutto Pizza</h2>
                                                    <p class="description">Creamy tomato sauce, mozzarella, sharp cheddar, prosciutto, arugula, extra virgin olive oil.</p>
                                                    <hr> 
                                                    <h2 class="title" style="color:#008d6b">Truffle Mushroom Pizza</h2>
                                                    <p class="description">Creamy white sauce, mozzarella, sharp cheddar, sauteed mushrooms, shaved parmesan, truffle oil.</p>
                                                    <hr>
                                                </div>
                                                 <div class="col-sm-6"> 
                                                    <h2 class="title" style="color:#008d6b">Four Cheese Pizza</h2>
                                                    <p class="description">Creamy white sauce, mozzarella, sharp cheddar, parmesan, gorgonzola, arugula, extra virgin olive oil.</p>
                                                    <hr>
                                                    <h2 class="title" style="color:#008d6b">Hawaiian Pizza</h2>
                                                    <p class="description">Pineapple creme fraiche, mozzarella, sharp cheddar, pineapple, pepperoni, arugula, extra virgin olive oil. </p>
                                                    <hr>
                                                    <h2 class="title" style="color:#008d6b">Mexican Pizza</h2>
                                                    <p class="description">Mixed cheese, chorizo, onions, salsa, cilantro.</p>
                                                    <hr>
                                                    <h2 class="title" style="color:#008d6b">Ratatouille Pizza</h2>
                                                    <p class="description">Tomato sauce, garlic, sauteed mushrooms, zucchini, bell peppers, eggplant & black olives.</p>
                                                    <hr>
                                                </div> 
                                            </div>
                                        </div>
                                       </div>
                                    </div>
                                    <div class="tab-pane fade" id="tab3default">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="carousel slide" id="fade-quote-carousel" data-ride="carousel" data-interval="3000">
                                                  <!-- Carousel indicators -->
                                                  <ol class="carousel-indicators">
                                                    <li data-target="#fade-quote-carousel" data-slide-to="0"></li>
                                                    <li data-target="#fade-quote-carousel" data-slide-to="1" class="active"></li>
                                                    <li data-target="#fade-quote-carousel" data-slide-to="2"></li>
                                                    <li data-target="#fade-quote-carousel" data-slide-to="3"></li>
                                                </ol>
                                                <!-- Carousel items -->
                                                <div class="carousel-inner">
                                                    <div class="item ">
                                                        <img src="{{asset('frontend/plank/images/entry/ent-1.jpg')}}">
                                                    </div>
                                                    <div class="item active">
                                                        <img src="{{asset('frontend/plank/images/entry/ent-2.jpg')}}">
                                                    </div>
                                                    <div class="item">
                                                        <img src="{{asset('frontend/plank/images/entry/ent-3.jpg')}}">
                                                    </div>  
                                                    <div class="item">
                                                        <img src="{{asset('frontend/plank/images/entry/ent-4.jpg')}}">
                                                    </div>                      
                                                </div>
                                            </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="col-sm-6">
                                                    <h2 class="title">Entrees List:</h2>
                                                    {{-- <ul class="list-info">
                                                        <li>Plank Roast Chicken</li>
                                                        <li>Plank Ribs</li>
                                                    </ul> --}}
                                                    @foreach($entrees_sweets as $index => $info)
                                                    @if($info->type == "entrees")
                                                    {!!$info->food_name!!}
                                                    @endif
                                                    @endforeach
                                                </div>
                                                <div class="col-sm-6">
                                                    <h2 class="title">Sweets:</h2>
                                                    {{-- <ul class="list-info">
                                                        <li>Plank Affogato</li>
                                                    </ul> --}}
                                                    @foreach($entrees_sweets as $index => $info)
                                                    @if($info->type == "sweets")
                                                    {!!$info->food_name!!}
                                                    @endif
                                                    @endforeach
                                                    <h2 class="title">Or pop next door to Baker & Cook for your favourite sweet treat, tea & coffee</h2>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="tab4default">
                                        <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="carousel slide" id="fade-quote-carousel" data-ride="carousel" data-interval="3000">
                                                      <!-- Carousel indicators -->
                                                      <ol class="carousel-indicators">
                                                        <li data-target="#fade-quote-carousel" data-slide-to="0"></li>
                                                        <li data-target="#fade-quote-carousel" data-slide-to="1" class="active"></li>
                                                    </ol>
                                                    <!-- Carousel items -->
                                                    <div class="carousel-inner">
                                                        <div class="item ">
                                                            <img src="{{asset('frontend/plank/images/pasta/pasta-1.jpg')}}">
                                                        </div>
                                                        <div class="item active">
                                                            <img src="{{asset('frontend/plank/images/pasta/pasta-2.jpg')}}">
                                                        </div>                     
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="col-md-6">
                                                   <h2 class="title" style="color:#008d6b">Shrimp Ala Vodka</h2>
                                                   <p class="description">Spaghetti pasta, creamy tomato sauce, shrimp, vodka</p>
                                                   <hr>
                                                   <h2 class="title" style="color:#008d6b">Clams & Spaghetti</h2>
                                                   <p class="description">Spaghetti Pasta, clams, garlic & white wine</p>
                                                   <hr>
                                                   <h2 class="title" style="color:#008d6b">Carbonara</h2>
                                                   <p class="description">Spaghetti, bacon, white sauce, parmesan, poached egg, fresh basil</p>
                                                   <hr>
                                                   <h2 class="title" style="color:#008d6b">Pancetta Pesto Cream</h2>
                                                   <p class="description">Penne, pancetta, parmesan, creamy pesto sauce</p>
                                                   <hr> 
                                               </div>

                                               <div class="col-sm-6">
                                                <h2 class="title" style="color:#008d6b">Bolognese</h2>
                                                <p class="description">Spaghetti, tomatoes, meat sauce, chicken paté</p>
                                                <hr>
                                                <h2 class="title" style="color:#008d6b">Beef Ragu</h2>
                                                <p class="description">Fusilli pasta, slow cooked beef cheeks, oregano, garlic, parmesan cheese, fresh basil, extra virgin oil </p>
                                                <hr>
                                                <h2 class="title" style="color:#008d6b">Truffle Chicken Pasta</h2>
                                                <p class="description">Spaghetti pasta, truffle cream sauce, grilled chicken </p>
                                                <hr>
                                              </div> 
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                            <div class="bottom margin-top-50">
                        		<img src="{{asset('frontend/plank/images/background-full/border-bottom.png')}}" alt="">
                        	</div>
                        </div>
                    </section>
                    <!-- Blogs-->
                    <section id="contact" class="contact-bg padding-top-15 padding-bottom-15">
                        <div class="container">
                            <div class="main-content">
                            	<div class="row">
                                    <div class="col-md-5 time">
                                        <div class="desc-int" style="padding: 50px 0!important;">
                                        <div class="main-titles text-center">
                                            <div class="main-titles">
                                                 <h1 class="title" style="margin-bottom: 3%!important;"><img src="{{asset('frontend/plank/images/background-full/Time.png')}}" alt=""> <br>
                                                <img src="{{asset('frontend/plank/images/background-full/88-brown.png')}}" alt="" style="margin: 0 auto; width: 50px"></h1></div>
                                                <h2>Everyday</h2>
                                                <div style="color: white">
                                                    <h3><span class="fa fa-check" style="margin-right: 5px;"></span> 11:00AM - 10:00PM</h3>
                                                    
                                                </div>
                                                <div class="num">
                                                    809-2340
                                                </div>
                                                <div style="color: white">
                                                    <h3><span class="fa fa-map-marker" style="margin-right: 5px;"></span> G/F S Maison, Conrad Manila</h3>
                                                    <h3><span class="fa fa-envelope" style="margin-right: 5px;"></span> <a href="mailto:bc_plank.conrad@bistro.com.ph" style="color: #fff">bc_plank.conrad@bistro.com.ph</a></h3>
                                                    <h3><a style="color: #fff;" href="https://www.facebook.com/Baker-Cook-206813232840592/"><span class="fa fa-facebook-square" style="margin-right: 5px;"></span> <span class="fa fa-instagram" style="margin-right: 5px;"></span> @BakerandCookPh</a></h3>
                                                </div>
                                        </div>
                                    </div>
                                    </div>
                                    <div class="col-md-5">
                                        <div class="main-titles text-center" style="padding:0 20px">
                                            <div class="main-titles">
                                                 <h1 class="title" style="margin-bottom: 3%!important; margin-top: 30px;"><img src="{{asset('frontend/plank/images/background-full/Contact-Us.png')}}" alt=""> <br>
                                                <img src="{{asset('frontend/plank/images/background-full/88.png')}}" alt="" style="margin: 0 auto; width: 50px"></h1></div>
                                        <form action="{{url('inquiry/sent')}}" method="POST"  class="contact-form">
                                        <input type="hidden" value="{{csrf_token()}}" name="_token">
                                        <input type="hidden" name="type" value="plank">
                                        <div class="row">
                                        @if(Session::get('notification-status')=="success") 
                                        <div class="alert bg-success" style="color: black;">Your message successfully sent!</div>
                                        @elseif(Session::get('notification-status')=="failed")
                                        <div class="alert bg-success" style="color: black;">Your message failed to sent!</div>
                                        @endif
                                            <div class="col-sm-6 text-center">
                                                <div class="form-group"><label class="form-label">NAME <span class="highlight">*</span></label><input id="name" name="name" type="text" required="required" data-validation-required-message="Please enter your name." class="form-control">
                                                    <p
                                                        class="help-block text-danger"></p>
                                                </div>
                                            </div>
                                            <div class="col-sm-6 text-center">
                                                <div class="form-group"><label class="form-label">PHONE</label><input id="phone" type="text" required="required" data-validation-required-message="Please enter your phone number." name="phone" class="form-control">
                                                    <p class="help-block text-danger"></p>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 text-center">
                                                <div class="form-group"><label class="form-label">EMAIL <span class="highlight">*</span></label><input id="email" type="email" required="required" data-validation-required-message="Please enter your email address." name="email" class="form-control">
                                                    <p
                                                        class="help-block text-danger"></p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="form-group"><label class="form-label">HOW CAN WE HELP?</label><textarea id="message" required="required" data-validation-required-message="Please enter a message." class="form-control form-textarea" name="message"></textarea>
                                                    <p class="help-block text-danger"></p>
                                                </div>
                                            </div>
                                        </div>
                                        <button type="submit" class="btn btn-maincolor">Send Message </button>
                                        <!-- <div id="success" class="margin-top-50"></div> -->
                                        </form>
                                    </div>

                            </div>
                        </div>
                    </section>
                </div>
               	@include('frontend.includes.plank.footer')
               
            </div>
	</div>
	@include('frontend.includes.plank.scripts')
</body>
</html>